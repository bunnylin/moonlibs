{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function GetDat(const datname : UTF8string) : dword;
// Returns the index of the lowest existing dat in availableDats[] with a matching project name, or 0 if not found.
// The input project name should be in lowercase.
begin
	for result := 1 to availableDatCount - 1 do
		if availableDats[result].projectName = datname then exit;
	result := 0;
end;

procedure SortDats;
var i, j : dword;
	tempdat : TDatFile;
	tempname : UTF8string;
begin
	if availableDatCount < 3 then exit; // don't touch index 0, must have at least 2 others to sort
	// Insertion sort.
	i := 2;
	//if onlylast then i := availableDatCount - 1;
	while i < availableDatCount do begin
		tempdat := availableDats[i];
		tempname := lowercase(tempdat.displayName);
		j := i - 1;
		while (j <> 0) and (lowercase(availableDats[j].displayName) > tempname) do begin
			availableDats[j + 1] := availableDats[j];
			dec(j);
		end;
		availableDats[j + 1] := tempdat;
		inc(i);
	end;
end;

function LoadGraphicFileMetaData(var filu : file; blocksize : dword) : TGraphicFile;
// Loads the image's metadata into tempgra. Return NIL if anything went wrong.
// The file must be open and seeked to the beginning of this graphic's metadata.
type metaheader = packed record
		resx, resy, sizex, sizey : word;
		ofsx, ofsy : longint;
		framecount, seqlen : dword;
	end;
var pmetaheader : ^metaheader;
	i, blockend : dword;
	metabuffy : array[0..255 + sizeof(metaheader)] of byte;
begin
	if blocksize = 0 then begin
		asman_logger('[!] LoadGraphicFileMetaData: empty block');
		result := NIL;
		exit;
	end;
	metabuffy[0] := 0; // silence a compiler warning
	blockend := filepos(filu) + blocksize; // end of this block

	// Read the image meta header into memory.
	blockread(filu, metabuffy[0], 1); // image name byte length
	blockread(filu, metabuffy[1], metabuffy[0] + sizeof(metaheader));
	pmetaheader := @metabuffy[metabuffy[0] + 1];
	if metabuffy[0] > 31 then metabuffy[0] := 31; // name must be 31 chars or less

	result := TGraphicFile.Create(upcase(ShortString((@metabuffy[0])^)));
	with result do begin
		origResXP := LEtoN(pmetaheader^.resx);
		origResYP := LEtoN(pmetaheader^.resy);
		origSizeXP := LEtoN(pmetaheader^.sizex);
		origSizeYP := LEtoN(pmetaheader^.sizey);
		origOfsXP := LEtoN(pmetaheader^.ofsx);
		origOfsYP := LEtoN(pmetaheader^.ofsy);
		frameCount := LEtoN(pmetaheader^.framecount);
		seqLen := LEtoN(pmetaheader^.seqlen);

		setlength(sequence, seqLen);
		if seqLen <> 0 then blockread(filu, sequence[0], seqLen * 4);
		i := IOresult;
		if i <> 0 then begin
			asman_logger('[!] LoadGraphicFileMetaData: ' + errortxt(i));
			result := NIL;
			exit;
		end;

		// Remember where to find the image data.
		srcFileReadOfs := filepos(filu);
		srcFileReadSize := blockend - srcFileReadOfs;

		if frameCount = 0 then frameCount := 1;
		origFrameHeightP := origSizeYP div frameCount;
	end;
end;

procedure TDatFile.LoadDat;
// Attempts to load the previously enumerated .dat resource pack. Throws an exception in case of errors.
// This loads all small assets fully into memory, but only loads metadata for large assets like graphics.
// Automatically loads all parents of the dat, and tries to load a tsv if one is next to the dat.
var blockp : pointer;
	newgfxlist : array of TGraphicFile = NIL;
	blockID, blocksize, newgfxcount, i : dword;
	tempgra : TGraphicFile;

	procedure _GrabMidis;
	// Unpack the midi files from blockp^.
	var readp, endp : pointer;
		size : dword;
		name : ShortString = '';
	begin
		readp := blockp;
		endp := blockp + blocksize;
		while readp < endp do begin
			if readp + byte(readp^) + 4 + 1 >= endp then raise Exception.Create('midi head oob');
			move(readp^, name[0], byte(readp^) + 1);
			inc(readp, byte(readp^) + 1);
			size := LEtoN(dword(readp^)); inc(readp, 4);
			if readp + size > endp then raise Exception.Create('midi data oob');
			ImportMidiFile(readp, size, upcase(name));
			inc(readp, size);
		end;
		// Drop spare space, unlikely to see music files added after this in normal use.
		setlength(musicObjects, musicObjectCount);
	end;

	procedure _LoadTSV;
	// Automatically load the accompanying TSV, if any.
	var basepath, tsvpath : UTF8string;
		tsvlist : TStringBunch;
		f : TFileLoader;
	begin
		basepath := copy(datFilePath, 1, datFilePath.Length - 4);
		tsvlist := FindFiles_caseless(basepath + '*.tsv');
		tsvlist.Sort;
		for tsvpath in tsvlist do
			if tsvpath[basepath.Length + 1] in ['.','-'] then begin
				asman_logger('Autoload accompanying ' + tsvpath);
				f := TFileLoader.Open(tsvpath);
				try
					ImportStringTable(f.readp, f.fullFileSize, FALSE);
				finally
					f.Destroy();
				end;
				break;
			end;
	end;

begin
	blocksize := 0; blockID := 0;

	if parentName <> '' then begin
		i := GetDat(parentName);
		if i = 0 then
			raise Exception.Create(strcat('LoadDat: % is missing parent %', [projectName, parentName]));
		with availableDats[i] do if NOT loaded then LoadDat;
	end;

	asman_logger('Loading ' + datFilePath);
	setlength(newgfxlist, 400);
	newgfxcount := 0;

	if NOT loaded then begin
		filemode := 0; reset(fileObject, 1); // read-only
		i := IOresult;
		if i <> 0 then raise Exception.Create('LoadDat: ' + errortxt(i) + ' opening ' + datFilePath);
		loaded := TRUE;
	end;

	seek(fileObject, dataStartOfs);
	while filepos(fileObject) + 8 < filesize(fileObject) do begin

		blockread(fileObject, blockID, 4);
		blockID := LEtoN(blockID);
		blockread(fileObject, blocksize, 4);
		blocksize := LEtoN(blocksize);
		//asman_logger(strcat('load block & size=% ofs=%', [blockID, blocksize, filepos(fileObject)]));
		i := IOresult;
		if i <> 0 then raise Exception.Create('LoadDat: ' + errortxt(i) + ' reading ' + datFilePath);
		if blocksize = 0 then continue;
		if filepos(fileObject) + blocksize > filesize(fileObject) then break;

		{$note todo: use generic compression for any block ID $C0*}
		// F0: Don't read pre-emptively; C0: Read and decompress; B0: Read directly into blockp^.
		case blockID shr 8 of
			// Scripts.
			$501E0B:
			begin
				getmem(blockp, blocksize);
				try
					blockread(fileObject, blockp^, blocksize);
					ImportScripts(blockp, blocksize);
				finally
					freemem(blockp); blockp := NIL;
				end;
			end;

			// String table.
			$511E0B:
			begin
				getmem(blockp, blocksize + 1);
				try
					blockread(fileObject, blockp^, blocksize);
					byte((blockp + blocksize)^) := $A; // ensure the file ends with a newline
					ImportStringTable(blockp, blocksize + 1, TRUE);
				finally
					freemem(blockp); blockp := NIL;
				end;
			end;

			// Midi files.
			$521E0B:
			begin
				getmem(blockp, blocksize);
				try
					blockread(fileObject, blockp^, blocksize);
					_GrabMidis;
				finally
					freemem(blockp); blockp := NIL;
				end;
			end;

			// PNG or PNGX.
			$531E0B, $53EE0B:
			begin
				i := filepos(fileObject);
				tempgra := LoadGraphicFileMetaData(fileObject, blocksize);
				seek(fileObject, i + blocksize);
				if tempgra <> NIL then begin
					tempgra.srcDat := self;
					// Check if a graphic by this name is already listed.
					i := GetGraphicFile(tempgra.graphicName);
					if i <> 0 then begin
						// Already listed: overwrite it.
						graphicFiles[i].Destroy;
						graphicFiles[i] := tempgra;
					end
					else begin
						// Unlisted: add to newgfxlist.
						if newgfxcount >= dword(length(newgfxlist)) then
							setlength(newgfxlist, length(newgfxlist) + 100);
						newgfxlist[newgfxcount] := tempgra;
						inc(newgfxcount);
					end;
				end;
			end;

			else begin
				asman_logger(strcat('Unknown block $& at $&', [longint(blockID), filepos(fileObject)]));
				seek(fileObject, filepos(fileObject) + blocksize);
			end;
		end;

		i := IOresult;
		if i <> 0 then raise Exception.Create('LoadDat: ' + errortxt(i) + ' reading ' + datFilePath);
	end;

	// Ignore and flush remaining IO errors, we'll just go with whatever we got.
	while IOresult <> 0 do ;

	// Add new graphic metadata into graphicFiles[].
	// (Note that we don't check if the new PNGs exist in multiple copies in this same DAT file; if multiple copies
	// exist, then they all get loaded side by side into graphicFiles, and exactly which PNG is returned by
	// GetGraphicFile is undefined behavior.)
	if newgfxcount <> 0 then begin
		i := graphicFileCount + newgfxcount;
		if i >= dword(length(graphicFiles)) then setlength(graphicFiles, i);
		move(newgfxlist[0], graphicFiles[graphicFileCount], sizeof(TGraphicFile) * newgfxcount);
		graphicFileCount := i;
		newgfxlist := NIL;
		SortGraphicFiles(FALSE);
	end;

	i := length(loadedDatList);
	setlength(loadedDatList, i + 1);
	loadedDatList[i] := self;

	if mainOverride <> '' then mainLabelName := mainOverride;

	// Load accompanying TSV if found. Must be the same basename, but can have an extra hyphen and suffix.
	_LoadTSV;
end;

function TDatFile.LoadBanner(const saveasname : string31) : boolean;
// Loads the banner image for this dat project. The image in memory is renamed to saveasname, which can't be empty.
// If the dat doesn't contain a banner, tries to load one from its parent, if any.
// If no banners are found, no image is loaded and the previous "saveasname" graphic remains in place.
// Returns TRUE if the banner was successfully loaded.
var tempgra : TGraphicFile;
	datfilesize : ptruint;
	i, blocksize : dword;
	holdingfile : boolean;
begin
	result := FALSE;
	holdingfile := FALSE;
	asman_logger('banner from ' + projectName);
	try try
		if saveasname = '' then raise Exception.Create('empty saveasname');

		if NOT loaded then begin
			filemode := 0; reset(fileObject, 1); // read-only
			i := IOresult;
			if i <> 0 then raise Exception.Create(errortxt(i) + ' opening ' + datFilePath);
			holdingfile := TRUE;
		end;

		datfilesize := FileSize(fileObject);
		seek(fileObject, dataStartOfs);
		blockread(fileObject, i, 4);
		i := LEtoN(i);
		if i shr 8 <> $53EE0B then raise Exception.Create('first resource in dat not banner');

		blockread(fileObject, i, 4);
		blocksize := LEtoN(i);
		if dataStartOfs + 8 + blocksize > datfilesize then raise Exception.Create('first resource size out of bounds');

		tempgra := LoadGraphicFileMetaData(fileObject, blocksize);
		if tempgra = NIL then raise Exception.Create('failed to load banner metadata');

		tempgra.srcDat := self;
		tempgra.graphicName := upcase(saveasname);
		AddOrReplaceGraphicFile(tempgra);

		result := TRUE;

	except
		on e : Exception do begin
			asman_logger('[!] LoadBanner: ' + e.Message);
			// If failed to load a banner, try the parent dat.
			if parentName <> '' then begin
				i := GetDat(parentName);
				if i <> 0 then result := availableDats[i].LoadBanner(saveasname);
			end;
		end;
	end;
	finally
		if holdingfile then close(fileObject);
		i := IOresult;
		if i <> 0 then asman_logger('[!] LoadBanner: ' + errortxt(i) + ' closing ' + datFilePath);
	end;
end;

// Output DAT-file specs (words stored x86-style LSB first):
//
// Header:
//   signature : DWORD = $CACABAAB;
//   file format version : BYTE;
//   parent name : UTF-8 ministring;
//   dat description : UTF-8 ministring;
//   version [:mainOverride]  : UTF-8 ministring, colon-separated content;
//   baseResX, baseResY : WORD; can be 0 to use parent's or default base resolution
//
// Followed by a series of data blocks... each has a DWORD signature, DWORD data length, then that many bytes of DATA.
// If the signature's first byte is $C0, the block is compressed with zstd and there's an extra DWORD between data
// length and DATA, which is the uncompressed size of the DATA. Uncompress, and proceed as if signature top was $B0.
// If the first byte is $F0, it's a big file read from disk on-demand, otherwise same as $B0.
// If the dat has a banner image, it must be the first data block in the file.
//
// Script bytecode
//   signature : DWORD = $501E0BB0;
//   data length : DWORD;
//   language list count : byte;
//   language list : array of UTF-8 ministring;
//   array of record
//     label name : UTF-8 ministring up to 63 bytes;
//     next label : UTF-8 ministring up to 63 bytes;
//     label language : byte;
//     codesize : dword; (uncompressed byte size)
//     code : array[codesize] of bytecode;
//   end;
//
// String table
//   signature : DWORD = $511E0BB0;
//   data length : DWORD;
//   Followed by the TSV file.
//
// Midi music
//   signature : DWORD = $521E0BB0;
//   data length : DWORD;
//   array of record
//     song name : UTF-8 ministring up to 31 bytes;
//     datasize : dword;
//     midi data : array[datasize] of byte;
//   end;
//
// PNG or PNGX image
//   signature : DWORD = $531E0BB0; // or $53EE0BB0 if it's the banner image
//   data length : DWORD;
//   image name : UTF-8 ministring up to 31 bytes;
//   origresx, origresy : WORD;
//   origsizexp, origsizeyp : WORD;
//   origofsxp, origofsyp : LONGINT;
//   framecount : DWORD;
//   seqlen bytes : DWORD;
//   sequence : array[0..seqlen/4 - 1] of DWORD;
//   Followed by the PNG or PNGX file without the first 2 DWORDS (redundant sig)
//
// OGG sound
//   signature : DWORD = $541E0BB0;
//   data length : DWORD;
//   name : UTF-8 ministring up to 31 bytes;
//   followed by ... the OGG file?
//
// FLAC sound
//   signature : DWORD = $551E0BB0;
//   data length : DWORD;
//   name : UTF-8 ministring up to 31 bytes;
//   followed by ... the FLAC file?

procedure TDatFile.WriteDat(const filepath : UTF8string);
// Attempts to write all known assets into the given dat file. Throws an exception in case of failures.
// Use LoadFileTree etc to build lists of known files before calling this.
var f : TBufferedFileWriter;
	i, bannerindex : dword;

	procedure _WriteGraphic(index : dword; isbanner : boolean);
	var gf : file;
		p : pointer = NIL;
		tempimage : mcg_bitmap = NIL;
		j : dword;
		mustclose : boolean = FALSE;
	begin
		with graphicFiles[index] do begin
			if (srcDat <> NIL) and (srcDat.loaded) then
				gf := srcDat.fileObject
			else begin
				if srcDat <> NIL then srcFilePath := srcDat.datFilePath;
				if srcFilePath = '' then
					raise Exception.Create('WriteDat: WriteGraphic: ' + graphicName + ' file not found');
				assign(gf, srcFilePath);
				filemode := 0; reset(gf, 1); // readonly
				j := IOresult;
				if j <> 0 then
					raise Exception.Create('WriteDat: WriteGraphic: ' + errortxt(j) + ' opening ' + srcFilePath);
				mustclose := TRUE;
			end;
			Assert(IOresult = 0);

			try
				Assert(srcFileReadSize <> 0);
				getmem(p, srcFileReadSize);
				seek(gf, srcFileReadOfs);
				blockread(gf, p^, srcFileReadSize);
				j := IOresult;
				if j <> 0 then
					raise Exception.Create('WriteDat: WriteGraphic: ' + errortxt(j) + ' reading ' + srcFilePath);

				if isbanner then
					f.WriteDword(NtoLE($53EE0BF0))
				else
					f.WriteDword(NtoLE($531E0BF0)); // sig

				// Ensure multiframe images have been rearranged in a single vertical column before saving.
				// Metadata origSizes were adjusted by mcsassm.LoadPNG if rearranging is required.
				if frameCount > 1 then begin
					tempimage := mcg_bitmap.FromPNG(p, srcFileReadSize, [MCG_FLAG_HEADERONLY]);
					if tempimage.sizeXP <> origSizeXP then begin
						tempimage.Destroy; tempimage := NIL;
						tempimage := mcg_bitmap.FromPNG(p, srcFileReadSize, []);
						freemem(p); p := NIL;
						RearrangeFrames(tempimage, graphicFiles[index]);
						tempimage.MakePNG(p, srcFileReadSize);
						tempimage.Destroy; tempimage := NIL;
					end;
				end;

				j := dword(length(graphicName) + 1) + (4 * 2) + (4 * 4) + (seqLen * 4) + srcFileReadSize;
				f.WriteDword(NtoLE(j)); // blocksize

				f.WriteBytes(@graphicName[0], length(graphicName) + 1);
				if origResXP = 0 then
					f.WriteWord(NtoLE(baseResX))
				else
					f.WriteWord(NtoLE(origResXP));
				if origResYP = 0 then
					f.WriteWord(NtoLE(baseResY))
				else
					f.WriteWord(NtoLE(origResYP));
				f.WriteWord(NtoLE(origSizeXP));
				f.WriteWord(NtoLE(origSizeYP));
				f.WriteDword(NtoLE(dword(origOfsXP)));
				f.WriteDword(NtoLE(dword(origOfsYP)));
				f.WriteDword(NtoLE(frameCount));
				f.WriteDword(NtoLE(seqLen));
				if seqLen <> 0 then for j := 0 to seqLen - 1 do f.WriteDword(NtoLE(sequence[j]));

				f.WriteBytes(p, srcFileReadSize);

			finally
				if mustclose then close(gf);
				if p <> NIL then freemem(p);
				if tempimage <> NIL then tempimage.Destroy;
			end;
		end;
	end;

	procedure _WriteConcatScripts;
	var stringtable : pointer = NIL;
		j, totalsize : dword;
	begin
		asman_logger('Writing scripts...');
		{$note todo: compress+write scripts in one pass} // they're currently uncompressed

		// Two ministring lengths + language byte + code size dword = 7 bytes. Plus 1 for language count.
		totalsize := (scriptObjectCount - 1) * 7 + 1;
		for j := 1 to scriptObjectCount - 1 do with scriptObjects[j] do
			inc(totalsize,
				dword(length(labelName) + dword(length(nextLabel)) + dword(length(code))));
		for j := high(languageList) downto 0 do inc(totalsize, languageList[j].Length + 1);

		f.WriteDword(NtoLE($501E0BB0)); // sig
		f.WriteDword(NtoLE(totalsize));
		f.WriteByte(byte(languageList.Length));
		for j := 0 to high(languageList) do begin
			f.WriteByte(byte(languageList[j].Length));
			f.WriteBytes(@languageList[j][1], byte(languageList[j].Length));
		end;
		for j := 1 to scriptObjectCount - 1 do with scriptObjects[j] do begin
			f.WriteBytes(@labelName[0], length(labelName) + 1);
			f.WriteBytes(@nextLabel[0], length(nextLabel) + 1);
			f.WriteByte(labelLanguage);
			f.WriteDword(NtoLE(length(code)));
			if length(code) <> 0 then f.WriteBytes(@code[0], length(code));
		end;

		asman_logger('Writing strings...');
		totalsize := ExportStringTable(stringtable);
		Assert((stringtable <> NIL) and (totalsize <> 0));
		try
			f.WriteDword(NtoLE($511E0BB0)); // sig
			f.WriteDword(NtoLE(totalsize));
			f.WriteBytes(stringtable, totalsize);
		finally
			freemem(stringtable);
		end;
	end;

	procedure _WriteConcatMusic;
	var j, totalsize : dword;
	begin
		asman_logger('Writing music...');
		totalsize := 0;
		for j := 1 to musicObjectCount - 1 do with musicObjects[j] do
			inc(totalsize, dword(length(data) + length(songName) + 4 + 1));

		f.WriteDword(NtoLE($521E0BB0)); // sig
		f.WriteDword(NtoLE(totalsize));
		for j := 1 to musicObjectCount - 1 do with musicObjects[j] do begin
			f.WriteBytes(@songName[0], length(songName) + 1);
			f.WriteDword(NtoLE(length(data)));
			f.WriteBytes(@data[0], length(data));
		end;
	end;

begin
	if (parentName = '') and (GetScript(mainLabelName) = 0) then
		raise Exception.Create(mainLabelName + ' not found, please add it to one of the scripts.');
	Assert((NOT loaded) or (lowercase(filepath) <> lowercase(datFilePath)), 'dat is already loaded from target');
	if languageList[0] = '' then begin
		languageList[0] := 'English';
		asman_logger('[!] Game language undefined! Expected "language <name>" in inf. Defaulting to English.');
	end;

	f := TBufferedFileWriter.Create(filepath, NIL, 8 * 1024 * 1024);
	asman_logger('Saving as ' + filepath);
	try
		f.WriteDword(NtoLE(asman_signature));
		f.WriteByte(6); // dat format version

		f.WriteByte(byte(parentName.Length));
		if parentName.Length <> 0 then f.WriteBytes(@parentName[1], parentName.Length);

		f.WriteByte(byte(displayName.Length));
		if displayName.Length <> 0 then f.WriteBytes(@displayName[1], displayName.Length);

		// Version and main label override are stuffed in the same string, colon-separated. (Backward compatibility.)
		i := gameVersion.Length;
		if mainOverride <> '' then inc(i, mainOverride.Length + 1);
		if i > 255 then raise Exception.Create('Version + main override string must be < 255 bytes.');
		f.WriteByte(byte(i));
		if gameVersion <> '' then f.WriteString(gameVersion);
		if mainOverride <> '' then begin
			f.WriteByte(ord(':'));
			f.WriteString(mainOverride);
		end;

		f.WriteWord(NtoLE(word(baseResX)));
		f.WriteWord(NtoLE(word(baseResY)));

		if bannerName = '' then
			asman_logger('[!] Banner image undefined! Expected "banner <image>" in inf.')
		else begin
			asman_logger('Writing banner...');
			bannerindex := GetGraphicFile(bannerName);
			if bannerindex = 0 then raise Exception.Create('Banner file not found: ' + bannerName);
			_WriteGraphic(bannerindex, TRUE);
		end;

		if graphicFileCount > 1 then begin
			asman_logger('Writing graphics...');
			for i := 1 to graphicFileCount - 1 do
				if graphicFiles[i].graphicName <> bannerName then _WriteGraphic(i, FALSE);
		end;

		if scriptObjectCount > 1 then _WriteConcatScripts;
		if musicObjectCount > 1 then _WriteConcatMusic;

		i := IOresult;
		if i <> 0 then raise Exception.Create(errortxt(i) + ' writing ' + filepath);
		asman_logger(filepath + ' saved.');

	finally
		f.Destroy;
		Assert(IOresult = 0, 'IO fail leak');
	end;
end;

function TDatFile.ReadDATHeader : boolean;
// Attempts to read the header of a .dat resource pack, filling in all metadata for the TDatFile.
// Returns TRUE for success.
var i : dword;
	sb : TStringBunch = NIL;
begin
	result := FALSE;
	filemode := 0; reset(fileObject, 1); // read-only
	i := IOresult;
	if i <> 0 then begin
		asman_logger('[!] ReadDatHeader: opening ' + errortxt(i));
		exit;
	end;

	try
		blockread(fileObject, i, 4);
		if LEtoN(i) <> asman_signature then begin
			asman_logger('[!] ReadDatHeader: ' + datFilePath + ' is missing mcsassm dat signature');
			exit;
		end;

		i := 0;
		blockread(fileObject, i, 1);
		if i <> 6 then begin
			asman_logger('[!] ReadDatHeader: dat format is v' + strdec(i) + ', must be v6; please rebuild ' + datFilePath);
			exit;
		end;

		blockread(fileObject, i, 1);
		parentName := '';
		setlength(parentName, i);
		if i <> 0 then begin
			blockread(fileObject, parentName[1], i);
			parentName := lowercase(parentName);
			// If the project and parent are the same, no parent dependency.
			if parentName = projectName then parentName := '';
		end;

		blockread(fileObject, i, 1);
		if i = 0 then
			displayName := projectName
		else begin
			displayName := '';
			setlength(displayName, i);
			blockread(fileObject, displayName[1], i);
		end;

		gameVersion := '';
		mainOverride := '';
		blockread(fileObject, i, 1);
		if i <> 0 then begin
			setlength(gameVersion, i);
			blockread(fileObject, gameVersion[1], i);
			sb := gameVersion.Split(':', FALSE);
			if sb.Length <> 0 then gameVersion := sb[0];
			if sb.Length > 1 then mainOverride := upcase(sb[1]);
		end;

		i := IOresult;
		if i <> 0 then begin
			asman_logger('[!] ReadDatHeader: reading ' + errortxt(i));
			exit;
		end;

		blockread(fileObject, i, 4);
		i := LEtoN(i);
		baseResX := i and $FFFF;
		baseResY := i shr 16;

		dataStartOfs := filepos(fileObject);
		result := TRUE;
	finally
		close(fileObject);
		Assert(IOresult = 0, 'IO fail leak');
	end;
end;

procedure EnumerateDats(const scanpath : UTF8string);
// Finds *.dat in scanpath and puts them in availableDats[].
// The caller should call SortDats after enumerating all locations to present a friendly game selection list.
// Search is not recursive. Adding a new dat with the same name as an existing dat is not possible.
var filelist : TStringBunch;
	newdat : TDatFile;
	datbasename : UTF8string;
	i : dword;
begin
	asman_logger('Enumerating dats in ' + scanpath);
	filelist := FindFiles_caseless(PathCombine([scanpath, '*.dat']));
	if filelist.Length = 0 then begin
		asman_logger('none found');
		exit;
	end;
	for i := 0 to high(filelist) do begin // system may already have sorted the list, don't reverse
		datbasename := ExtractFileName(filelist[i]);
		setlength(datbasename, datbasename.Length - 4); // delete .dat suffix
		datbasename := lowercase(datbasename);

		if GetDat(datbasename) <> 0 then
			asman_logger('Already added ' + filelist[i])
		else begin
			newdat := TDatFile.Create;
			with newdat do begin
				projectName := datbasename;
				datFilePath := filelist[i];
				assign(fileObject, filelist[i]);
				loaded := FALSE;
			end;
			if newdat.ReadDatHeader then begin
				if availableDatCount >= dword(length(availableDats)) then
					setlength(availableDats, availableDatCount + 8);
				availableDats[availableDatCount] := newdat;
				inc(availableDatCount);
				asman_logger('Added ' + filelist[i]);
				// No need to sort dat list here, GetDat uses a linear search.
			end
			else begin
				asman_logger('Failed to add ' + filelist[i]);
				newdat.Destroy; newdat := NIL;
			end;
		end;
	end;
	setlength(availableDats, availableDatCount);
end;

destructor TDatFile.Destroy;
begin
	if loaded then close(fileObject);
	inherited;
end;

procedure UnloadDats(initall : boolean);
// Call this to dump all loaded assets if any, reset the asset arrays including list of dats loaded to empty.
// If initall is true, extra init actions are done and invalid null items are set up in each array.
// Unloading is called automatically on asman startup, and at asman shutdown.
// You should call this explicitly only when loading a game state, if the new state has different dats (eg. due to
// a mod change); afterward, load your new list of dats.
// The cacher thread is automatically stopped and relaunched if necessary.
var i : dword;
begin
	asman_logger('clearing asset manager');

	// Release the arrays entirely to discourage implicit resizing.
	if scriptObjectCount <> 0 then
		for i := scriptObjectCount - 1 downto 0 do scriptObjects[i].Destroy;
	setlength(scriptObjects, 0);
	scriptObjectCount := 0;

	if length(graphicObjects) <> 0 then
		for i := high(graphicObjects) downto 0 do graphicObjects[i].Destroy;
	setlength(graphicObjects, 0);

	if graphicFileCount <> 0 then
		for i := graphicFileCount - 1 downto 0 do graphicFiles[i].Destroy;
	setlength(graphicFiles, 0);
	graphicFileCount := 0;

	if musicObjectCount <> 0 then
		for i := musicObjectCount - 1 downto 0 do musicObjects[i].Destroy;
	setlength(musicObjects, 0);
	musicObjectCount := 0;

	if availableDatCount <> 0 then
		for i := availableDatCount - 1 downto 0 do availableDats[i].Destroy;
	setlength(availableDats, 0);
	availableDatCount := 0;
	setlength(loadedDatList, 0);

	setlength(languageList, 0);
	if NOT initall then exit;

	asman_logger('initing asset manager');
	// Set up a null item in each array. (This ensures the array is never empty, which saves a safety check during
	// Get* seeks, and allows returning index 0 if there was an error.)
	setlength(scriptObjects, 1);
	setlength(graphicFiles, 1);
	setlength(musicObjects, 1);
	setlength(availableDats, 1);

	scriptObjects[0] := TScriptObject.Create;
	graphicFiles[0] := TGraphicFile.Create('');
	musicObjects[0] := TMusicObject.Create;
	availableDats[0] := TDatFile.Create;
	scriptObjectCount := 1;
	globalStringCount := 0;
	totalStringCount := 0;
	graphicFileCount := 1;
	musicObjectCount := 1;
	availableDatCount := 1;
	mainLabelName := 'MAIN.';
	framesVertically := FALSE;

	ExpandGraphicObjectList;

	asman_gfxMemUsage := 0;
	asman_gObjWriteSlot := 1;

	// Set up the base language.
	setlength(languageList, 1);
	languageList[0] := '';
	defaultScriptLanguage := 0;
end;

