unit minimutex;

{                                                                           }
{ Minimutex - simple mutex and semaphore implementation for Free Pascal     }
{ Copyright 2023 :: Kirinn Bunnylin / MoonCore                              }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

// This unit provides two thread synchronisation mechanisms: a mutex, and a counting semaphore.
// There are no safety checks at all, so misuse will cause deadlocks, but it does run fast.
// These are implemented with interlocked commands only, so this unit should work on any platform where FPC has
// interlocked operations available. Both mechanisms should work correctly with both intra- and inter-process threads.
// The synchronisation is done through variables, so there's no cleanup required; just let them go out of scope.
//
// Threads waiting for access will only spinlock instead of suspending efficiently, so this implementation is
// inappropriate if your use case anticipates threads needing to wait frequently or for a potentially long time.
// You can specify an imprecise sleep amount between each spinlock round to mitigate CPU waste. Set to a negative
// number to spin aggressively without any delays; 0 or more to sleep at least so many milliseconds.
//
// The mutex only allows a single thread through at a time. You have to initialise the mutex variable to 0 before use.
// Remember to make absolutely sure the mutex is released after being locked. Perhaps best use a try-finally.
// There is no queue, so if multiple threads are waiting, they will grab the mutex in a random order.
// If FIFO ordering is needed, use the semaphore instead with initial = 1.
//
// The semaphore can be set to an initial number of signals, allowing so many threads through before further threads
// have to wait. One signal lets one thread through. Threads are released in FIFO order, except if multiple signals
// come at the same time, threads released simultaneously from the front of the queue don't necessarily start executing
// in their arrival order, particularly if the waiting threads have long sleep values.

{$mode objfpc}
{WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{WARN 4080 off} // doing the operation could prevent overflow errors.
{WARN 4081 off}
{WARN 4055 off} // Conversion between ordinals and pointers is not portable (even with ptruint)

interface

type TMiniMutex = longint;
type TMiniSemaphore = record
	turncounter : longint;
	arrivalticket : longint;
end;

// Waits until the specified mutex is free, then locks it. The mutex must have been initialised to 0.
// Set sleepmsec to 0 or higher to sleep so many msecs between lock polls. Set to negative to spinlock hard.
// If multiple threads are waiting on the mutex, they take it in random order.
procedure MiniMutexLock(var mutex : TMiniMutex; sleepmsec : longint = 0);

// Immediately frees the specified mutex. Should be called by the thread that locked the mutex when done using
// the shared object or code. Recommend using try-finally to ensure mutex release.
procedure MiniMutexRelease(var mutex : TMiniMutex); inline;

// Call this before trying to signal or wait on the semaphore.
// You can set the initial signal count to let some number of threads through immediately.
procedure MiniSemaphoreInit(var sema : TMiniSemaphore; initial : longint = 0); inline;

// Signals (increments) the semaphore, allowing the next queuing thread through the semaphore.
procedure MiniSemaphoreSignal(var sema : TMiniSemaphore); inline;

// If the semaphore has a free signal, reserves (decrements) it immediately and returns. If there is
// no free signal, the calling thread enters a FIFO wait queue until the semaphore is signalled.
// Set sleepmsec to 0 or higher to sleep so many msecs between wait polls. Set to negative to spinlock hard.
procedure MiniSemaphoreWait(var sema : TMiniSemaphore; sleepmsec : longint = 0);

// ------------------------------------------------------------------

implementation

uses sysutils; // only to get sleep ;_;

{$push}{$R-} // don't need range checks, let stuff wrap freely

procedure MiniMutexLock(var mutex : TMiniMutex; sleepmsec : longint = 0);
begin
	while TRUE do begin
		if InterlockedExchange(mutex, 1) = 0 then exit;
		if sleepmsec >= 0 then sleep(sleepmsec);
	end;
end;

procedure MiniMutexRelease(var mutex : TMiniMutex); inline;
begin
	mutex := 0;
end;

procedure MiniSemaphoreInit(var sema : TMiniSemaphore; initial : longint = 0); inline;
begin
	sema.arrivalticket := 0;
	sema.turncounter := initial;
end;

procedure MiniSemaphoreSignal(var sema : TMiniSemaphore); inline;
begin
	InterlockedIncrement(sema.turncounter);
end;

procedure MiniSemaphoreWait(var sema : TMiniSemaphore; sleepmsec : longint = 0);
var ticket : longint;
begin
	ticket := InterlockedIncrement(sema.arrivalticket);
	while ticket - sema.turncounter > 0 do
		if sleepmsec >= 0 then sleep(sleepmsec);
end;

{$pop}

begin
end.

