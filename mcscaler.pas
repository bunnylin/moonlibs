unit mcscaler;
{                                                                           }
{ Mooncore Graphics Scaler                                                  }
{ Copyright 2005-2024 :: Kirinn Bunnylin / MoonCore                         }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

{$mode objfpc}
{$modeswitch Typehelpers}
{$WARN 4079 off} // Spurious hints: Converting the operands to "Int64" before
{$WARN 4080 off} // doing the operation could prevent overflow errors.
{$WARN 4081 off}
{$WARN 6058 off} // as of FPC 3.2.0 inlining is ignored in some cases and each attempt generates a warning...

interface

uses mcgloder;

type mcgbmpHelper = type helper for mcg_bitmap
	private
		procedure ResizeBGR(tox, toy : dword);
		procedure ResizeBGRX(tox, toy : dword);
		procedure ResizeBGRA(tox, toy : dword);
	public
		procedure Resize(tox, toy : dword);
		procedure Tile(tox, toy : dword);
end;

implementation

uses sysutils, mccommon;

// Thoughts for new BunnyScale:
//
// Double-pass? First pass should calculate all needed pixel diffs and save them in a big bitmap... and quickly
// generate extra few pixels for the borders, so it's possible to do a second pass with direct references to the
// diffmap, rather than having to add IF-statements to protect against areas too close to the image borders.
// The diffs are I think all calculatable without conditionals, so it doesn't have to be super-slow. Only the second
// pass needs conditionals to check for pixel patterns, to identify where to best interpolate.

procedure mcgbmpHelper.ResizeBGR(tox, toy : dword);
var workbuffy : array of byte = NIL;
	srcp, destp : pointer;
	loopx, loopy : dword;
	start, finish, span : dword;
	a, b, c, d, e, f, g : dword;
begin
	if (bitmap = NIL) or (tox = 0) or (toy = 0) or (sizeXP = 0) or (sizeYP = 0) then exit;
	{$note todo: resizing should use gamma correction...}

	// Adjust image horizontally into workbuffy.
	if tox > sizeXP then begin
		start := 0;
		a := tox * 3;
		b := sizeXP * 3;
		g := sizeYP * a;
		// Horizontal stretch...
		setlength(workbuffy, g);
		destp := @workbuffy[0];
		span := (sizeXP shl 15) div tox;
		for loopx := tox - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * 3];
			if start and $FFFF8000 = finish and $FFFF8000 then begin
				// Start and finish are in the same source pixel column.
				for loopy := sizeYP - 1 downto 0 do begin
					word(destp^) := word(srcp^);
					byte((destp + 2)^) := byte((srcp + 2)^);
					inc(srcp, b);
					inc(destp, a);
				end;
			end
			else begin
				// Start and finish are in two adjacent source pixel columns.
				c := (start and $7FFF) xor $7FFF; // weight of left column
				d := (finish and $7FFF); // weight of right column
				e := c + d; // total weight for dividing
				c := (c shl 15) div e; // 32k weight of left column
				d := (d shl 15) div e; // 32k weight of right column
				for loopy := sizeYP - 1 downto 0 do begin
					byte((destp	)^) := (byte((srcp	)^) * c + byte((srcp + 3)^) * d) shr 15;
					byte((destp + 1)^) := (byte((srcp + 1)^) * c + byte((srcp + 4)^) * d) shr 15;
					byte((destp + 2)^) := (byte((srcp + 2)^) * c + byte((srcp + 5)^) * d) shr 15;
					inc(srcp, b);
					inc(destp, a);
				end;
			end;
			dec(destp, g);
			inc(start, span);
			inc(destp, 3);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end
	else if tox < sizeXP then begin
		// Horizontal shrink...
		setlength(workbuffy, tox * sizeYP * 3);
		destp := @workbuffy[0];
		span := (sizeXP shl 15) div tox;
		for loopy := sizeYP - 1 downto 0 do begin
			start := 0;
			b := (sizeYP - 1 - loopy) * sizeXP;
			for loopx := tox - 1 downto 0 do begin
				finish := start + span - 1;
				srcp := @bitmap[(start shr 15 + b) * 3];
				// left edge
				c := (start and $7FFF) xor $7FFF; // weight of left column
				// (c is also accumulated weight for this pixel)
				d := byte(srcp^) * c;
				e := byte(srcp^) * c;
				f := byte(srcp^) * c;
				// full middle columns
				a := start shr 15 + 1;
				while a < finish shr 15 do begin
					inc(c, $8000); // accumulate weight
					inc(d, byte(srcp^) shl 15); inc(srcp);
					inc(e, byte(srcp^) shl 15); inc(srcp);
					inc(f, byte(srcp^) shl 15); inc(srcp);
					inc(a);
				end;
				// right edge
				a := (finish and $7FFF); // weight of right column
				inc(c, a); // accumulate weight
				inc(d, byte(srcp^) * a); inc(srcp);
				inc(e, byte(srcp^) * a); inc(srcp);
				inc(f, byte(srcp^) * a);
				// save result
				byte(destp^) := d div c; inc(destp);
				byte(destp^) := e div c; inc(destp);
				byte(destp^) := f div c; inc(destp);
				inc(start, span);
			end;
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end;

	// else... Horizontal change is unnecessary.
	sizeXP := tox;
	stride := (tox * bitDepth + 7) shr 3;

	// Adjust image vertically into workbuffy.
	start := 0;
	b := sizeXP * 3;
	if toy > sizeYP then begin
		// Vertical stretch...
		setlength(workbuffy, b * toy);
		destp := @workbuffy[0];
		span := (sizeYP shl 15) div toy;
		for loopy := toy - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * b];
			if start and $FFFF8000 = finish and $FFFF8000 then begin
				// Start and finish are on the same source pixel row.
				move(srcp^, destp^, b);
				inc(destp, b);
			end
			else begin
				// Start and finish are on two adjacent source pixel rows.
				c := (start and $7FFF) xor $7FFF; // weight of upper row
				d := (finish and $7FFF); // weight of lower row
				e := c + d; // total weight for dividing
				c := (c shl 15) div e; // 32k weight of left column
				d := (d shl 15) div e; // 32k weight of right column
				for loopx := b - 1 downto 0 do begin
					byte(destp^) := (byte(srcp^) * c + byte((srcp + b)^) * d) shr 15;
					inc(srcp);
					inc(destp);
				end;
			end;
			inc(start, span);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end
	else if toy < sizeYP then begin
		// Vertical shrink...
		setlength(workbuffy, b * toy);
		destp := @workbuffy[0];
		span := (sizeYP shl 15) div toy;
		for loopy := toy - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * b];
			a := (start and $7FFF) xor $7FFF; // weight of highest row
			c := (finish and $7FFF); // weight of lowest row
			g := (finish shr 15) - (start shr 15);
			if g <> 0 then dec(g); // number of full rows between high and low
			d := a + c + g shl 15; // total weight
			g := g * b;
			for loopx := b - 1 downto 0 do begin
				// Accumulate weighed pixels in e, first add highest and lowest.
				e := byte(srcp^) * a + byte((srcp + g + b)^) * c;
				// Then add middle lines.
				if g <> 0 then begin
					f := g;
					repeat
						inc(srcp, b);
						inc(e, byte(srcp^) shl 15);
						dec(f, b);
					until f = 0;
					dec(srcp, g);
				end;
				// Divide by total weight.
				byte(destp^) := e div d;
				inc(srcp);
				inc(destp);
			end;
			inc(start, span);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end;

	// else ... Vertical change is unnecessary.
	sizeYP := toy;
end;

procedure mcgbmpHelper.ResizeBGRX(tox, toy : dword);
var workbuffy : array of byte = NIL;
	srcp, destp : pointer;
	loopx, loopy : dword;
	start, finish, span : dword;
	a, b, c, d, e, f, g : dword;
begin
	if (bitmap = NIL) or (tox = 0) or (toy = 0) or (sizeXP = 0) or (sizeYP = 0) then exit;

	// Adjust image horizontally into workbuffy.
	if tox > sizeXP then begin
		start := 0;
		a := tox * 4;
		b := sizeXP * 4;
		g := sizeYP * a;
		// Horizontal stretch...
		setlength(workbuffy, g);
		destp := @workbuffy[0];
		span := (sizeXP shl 15) div tox;
		for loopx := tox - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * 4];
			if start and $FFFF8000 = finish and $FFFF8000 then begin
				// Start and finish are in the same source pixel column.
				for loopy := sizeYP - 1 downto 0 do begin
					dword(destp^) := dword(srcp^);
					inc(srcp, b);
					inc(destp, a);
				end;
			end
			else begin
				// Start and finish are in two adjacent source pixel columns.
				c := (start and $7FFF) xor $7FFF; // weight of left column
				d := (finish and $7FFF); // weight of right column
				e := c + d; // total weight for dividing
				c := (c shl 15) div e; // 32k weight of left column
				d := (d shl 15) div e; // 32k weight of right column
				for loopy := sizeYP - 1 downto 0 do begin
					byte((destp	)^) := (byte((srcp	)^) * c + byte((srcp + 4)^) * d) shr 15;
					byte((destp + 1)^) := (byte((srcp + 1)^) * c + byte((srcp + 5)^) * d) shr 15;
					byte((destp + 2)^) := (byte((srcp + 2)^) * c + byte((srcp + 6)^) * d) shr 15;
					//byte((destp + 3)^) := (byte((srcp + 3)^) * c + byte((srcp + 7)^) * d) shr 15;
					inc(srcp, b);
					inc(destp, a);
				end;
			end;
			dec(destp, g);
			inc(start, span);
			inc(destp, 4);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end
	else if tox < sizeXP then begin
		// Horizontal shrink...
		setlength(workbuffy, tox * sizeYP * 4);
		destp := @workbuffy[0];
		span := (sizeXP shl 15) div tox;
		for loopy := sizeYP - 1 downto 0 do begin
			start := 0;
			b := (sizeYP - 1 - loopy) * sizeXP;
			for loopx := tox - 1 downto 0 do begin
				finish := start + span - 1;
				srcp := @bitmap[(start shr 15 + b) * 4];
				// left edge
				c := (start and $7FFF) xor $7FFF; // weight of left column
				// (c is also accumulated weight for this pixel)
				d := byte(srcp^) * c; inc(srcp);
				e := byte(srcp^) * c; inc(srcp);
				f := byte(srcp^) * c; inc(srcp);
				//g := byte(srcp^) * c;
				inc(srcp);
				// full middle columns
				a := start shr 15 + 1;
				while a < finish shr 15 do begin
					inc(c, $8000); // accumulate weight
					inc(d, byte(srcp^) shl 15); inc(srcp);
					inc(e, byte(srcp^) shl 15); inc(srcp);
					inc(f, byte(srcp^) shl 15); inc(srcp);
					//inc(g, byte(srcp^) shl 15);
					inc(srcp);
					inc(a);
				end;
				// right edge
				a := (finish and $7FFF); // weight of right column
				inc(c, a); // accumulate weight
				inc(d, byte(srcp^) * a); inc(srcp);
				inc(e, byte(srcp^) * a); inc(srcp);
				inc(f, byte(srcp^) * a); inc(srcp);
				//inc(g, byte(srcp^) * a);
				// Save the result.
				byte(destp^) := d div c; inc(destp);
				byte(destp^) := e div c; inc(destp);
				byte(destp^) := f div c; inc(destp);
				//byte(destp^) := g div c;
				inc(destp);
				inc(start, span);
			end;
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end;

	// else... Horizontal change is unnecessary.
	sizeXP := tox;
	stride := (tox * bitDepth + 7) shr 3;

	// Adjust image vertically into workbuffy.
	start := 0;
	b := sizeXP * 4;
	if toy > sizeYP then begin
		// Vertical stretch...
		setlength(workbuffy, b * toy);
		destp := @workbuffy[0];
		span := (sizeYP shl 15) div toy;
		for loopy := toy - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * b];
			if start and $FFFF8000 = finish and $FFFF8000 then begin
				// Start and finish are on the same source pixel row.
				move(srcp^, destp^, b);
				inc(destp, b);
			end
			else begin
				// Start and finish are on two adjacent source pixel rows.
				c := (start and $7FFF) xor $7FFF; // weight of upper row
				d := (finish and $7FFF); // weight of lower row
				e := c + d; // total weight for dividing
				c := (c shl 15) div e; // 32k weight of left column
				d := (d shl 15) div e; // 32k weight of right column
				for loopx := b - 1 downto 0 do begin
					byte(destp^) := (byte(srcp^) * c + byte((srcp + b)^) * d) shr 15;
					inc(srcp);
					inc(destp);
				end;
			end;
			inc(start, span);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end
	else if toy < sizeYP then begin
		// Vertical shrink...
		setlength(workbuffy, b * toy);
		destp := @workbuffy[0];
		span := (sizeYP shl 15) div toy;
		for loopy := toy - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * b];
			a := (start and $7FFF) xor $7FFF; // weight of highest row
			c := (finish and $7FFF); // weight of lowest row
			g := (finish shr 15) - (start shr 15);
			if g <> 0 then dec(g); // number of full rows between high and low
			d := a + c + g shl 15; // total weight
			g := g * b;
			for loopx := b - 1 downto 0 do begin
				// Accumulate weighed pixels in e, first add highest and lowest.
				e := byte(srcp^) * a + byte((srcp + g + b)^) * c;
				// Then add middle lines.
				if g <> 0 then begin
					f := g;
					repeat
						inc(srcp, b);
						inc(e, byte(srcp^) shl 15);
						dec(f, b);
					until f = 0;
					dec(srcp, g);
				end;
				// Divide by total weight, save the result.
				byte(destp^) := e div d;
				inc(srcp);
				inc(destp);
			end;
			inc(start, span);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end;

	// else ... Vertical change is unnecessary.
	sizeYP := toy;
end;

procedure mcgbmpHelper.ResizeBGRA(tox, toy : dword); {$note todo: resize from another mcg_bitmap source}
var workbuffy : array of byte = NIL;
	srcp, destp : pointer;
	loopx, loopy : dword;
	start, finish, span : dword;
	a, b, c, d, e, f, g : dword;
begin
	if (bitmap = NIL) or (tox = 0) or (toy = 0) or (sizeXP = 0) or (sizeYP = 0) then exit;

	// Adjust image horizontally into workbuffy.
	if tox > sizeXP then begin
		start := 0;
		a := tox * 4;
		b := sizeXP * 4;
		g := sizeYP * a;
		// Horizontal stretch...
		setlength(workbuffy, g);
		destp := @workbuffy[0];
		span := (sizeXP shl 15) div tox;
		for loopx := tox - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * 4];
			if start and $FFFF8000 = finish and $FFFF8000 then begin
				// Start and finish are in the same source pixel column.
				for loopy := sizeYP - 1 downto 0 do begin
					dword(destp^) := dword(srcp^);
					inc(srcp, b);
					inc(destp, a);
				end;
			end
			else begin
				// Start and finish are in two adjacent source pixel columns.
				c := (start and $7FFF) xor $7FFF; // weight of left column
				d := (finish and $7FFF); // weight of right column
				e := c + d; // total weight for dividing
				c := (c shl 15) div e; // 32k weight of left column
				d := (d shl 15) div e; // 32k weight of right column
				for loopy := sizeYP - 1 downto 0 do begin
					byte((destp	)^) := (byte((srcp	)^) * c + byte((srcp + 4)^) * d) shr 15;
					byte((destp + 1)^) := (byte((srcp + 1)^) * c + byte((srcp + 5)^) * d) shr 15;
					byte((destp + 2)^) := (byte((srcp + 2)^) * c + byte((srcp + 6)^) * d) shr 15;
					byte((destp + 3)^) := (byte((srcp + 3)^) * c + byte((srcp + 7)^) * d) shr 15;
					inc(srcp, b);
					inc(destp, a);
				end;
			end;
			dec(destp, g);
			inc(start, span);
			inc(destp, 4);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end
	else if tox < sizeXP then begin
		// Horizontal shrink...
		setlength(workbuffy, tox * sizeYP * 4);
		destp := @workbuffy[0];
		span := (sizeXP shl 15) div tox;
		for loopy := sizeYP - 1 downto 0 do begin
			start := 0;
			b := (sizeYP - 1 - loopy) * sizeXP;
			for loopx := tox - 1 downto 0 do begin
				finish := start + span - 1;
				srcp := @bitmap[(start shr 15 + b) * 4];
				// left edge
				c := (start and $7FFF) xor $7FFF; // weight of left column
				// (c is also accumulated weight for this pixel)
				d := byte(srcp^) * c; inc(srcp);
				e := byte(srcp^) * c; inc(srcp);
				f := byte(srcp^) * c; inc(srcp);
				g := byte(srcp^) * c; inc(srcp);
				// full middle columns
				a := start shr 15 + 1;
				while a < finish shr 15 do begin
					inc(c, $8000); // accumulate weight
					inc(d, byte(srcp^) shl 15); inc(srcp);
					inc(e, byte(srcp^) shl 15); inc(srcp);
					inc(f, byte(srcp^) shl 15); inc(srcp);
					inc(g, byte(srcp^) shl 15); inc(srcp);
					inc(a);
				end;
				// right edge
				a := (finish and $7FFF); // weight of right column
				inc(c, a); // accumulate weight
				inc(d, byte(srcp^) * a); inc(srcp);
				inc(e, byte(srcp^) * a); inc(srcp);
				inc(f, byte(srcp^) * a); inc(srcp);
				inc(g, byte(srcp^) * a);
				// Save the result.
				byte(destp^) := d div c; inc(destp);
				byte(destp^) := e div c; inc(destp);
				byte(destp^) := f div c; inc(destp);
				byte(destp^) := g div c; inc(destp);
				inc(start, span);
			end;
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end;

	// else... Horizontal change is unnecessary.
	sizeXP := tox;
	stride := (tox * bitDepth + 7) shr 3;

	// Adjust image vertically into workbuffy.
	start := 0;
	b := sizeXP * 4;
	if toy > sizeYP then begin
		// Vertical stretch...
		setlength(workbuffy, b * toy);
		destp := @workbuffy[0];
		span := (sizeYP shl 15) div toy;
		for loopy := toy - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * b];
			if start and $FFFF8000 = finish and $FFFF8000 then begin
				// Start and finish are on the same source pixel row.
				move(srcp^, destp^, b);
				inc(destp, b);
			end
			else begin
				// Start and finish are on two adjacent source pixel rows.
				c := (start and $7FFF) xor $7FFF; // weight of upper row
				d := (finish and $7FFF); // weight of lower row
				e := c + d; // total weight for dividing
				c := (c shl 15) div e; // 32k weight of left column
				d := (d shl 15) div e; // 32k weight of right column
				for loopx := b - 1 downto 0 do begin
					byte(destp^) := (byte(srcp^) * c + byte((srcp + b)^) * d) shr 15;
					inc(srcp);
					inc(destp);
				end;
			end;
			inc(start, span);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end
	else if toy < sizeYP then begin
		// Vertical shrink...
		setlength(workbuffy, b * toy);
		destp := @workbuffy[0];
		span := (sizeYP shl 15) div toy;
		for loopy := toy - 1 downto 0 do begin
			finish := start + span - 1;
			srcp := @bitmap[(start shr 15) * b];
			a := (start and $7FFF) xor $7FFF; // weight of highest row
			c := (finish and $7FFF); // weight of lowest row
			g := (finish shr 15) - (start shr 15);
			if g <> 0 then dec(g); // number of full rows between high and low
			d := a + c + g shl 15; // total weight
			g := g * b;
			for loopx := b - 1 downto 0 do begin
				// Accumulate weighed pixels in e, first add highest and lowest.
				e := byte(srcp^) * a + byte((srcp + g + b)^) * c;
				// Then add middle lines.
				if g <> 0 then begin
					f := g;
					repeat
						inc(srcp, b);
						inc(e, byte(srcp^) shl 15);
						dec(f, b);
					until f = 0;
					dec(srcp, g);
				end;
				// Divide by total weight, save the result.
				byte(destp^) := e div d;
				inc(srcp);
				inc(destp);
			end;
			inc(start, span);
		end;
		bitmap := workbuffy; setlength(workbuffy, 0);
	end;

	// else ... Vertical change is unnecessary.
	sizeYP := toy;
end;

procedure mcgbmpHelper.Resize(tox, toy : dword);
// Resizes the image to tox:toy resolution. Uses a sort of general purpose linear method to do it.
// Scaling downwards looks good, as color values stack properly.
// Scaling upwards by integer multiples looks like a point scaler.
// Scaling upwards by fractions is like a softened point scaler.
begin
	if length(bitmap) = 0 then raise Exception.Create('Image is empty');
	if bitDepth < 8 then raise Exception.Create('Can''t resize graphic with bit depth < 8');
	if (sizeXP = 0) or (sizeYP = 0) then raise Exception.Create(strcat('Invalid source size: %x%', [sizeXP, sizeYP]));
	if (tox = 0) or (toy = 0) then raise Exception.Create(strcat('Invalid resize target size: %x%', [tox, toy]));

	case bitmapFormat of
		MCG_FORMAT_BGR: ResizeBGR(tox, toy);
		MCG_FORMAT_BGRA: ResizeBGRA(tox, toy);
		MCG_FORMAT_BGRX: ResizeBGRX(tox, toy);
		else raise Exception.Create(strcat('Can''t resize with bitmapFormat %', [bitmapFormat]));
	end;
end;

procedure mcgbmpHelper.Tile(tox, toy : dword);
// Expands the bitmap to tox:toy resolution by tiling.
var workbuffy : array of byte = NIL;
	srcp, destp : pointer;
	movesize, srcrowsize, destrowsize, todoheight, y : dword;
begin
	if length(bitmap) = 0 then raise Exception.Create('Image is empty');
	if bitDepth < 8 then raise Exception.Create('Can''t tile graphic with bit depth < 8');
	if (sizeXP = 0) or (sizeYP = 0) then raise Exception.Create(strcat('Invalid source size: %x%', [sizeXP, sizeYP]));
	if (tox = 0) or (toy = 0) then raise Exception.Create(strcat('Invalid tile target size: %x%', [tox, toy]));

	case bitmapFormat of
		MCG_FORMAT_BGR: movesize := 3;
		MCG_FORMAT_BGRX, MCG_FORMAT_BGRA: movesize := 4;
		else movesize := 1;
	end;
	srcrowsize := sizeXP * movesize;
	destrowsize := tox * movesize;
	setlength(workbuffy, destrowsize * toy);
	srcp := @bitmap[0];
	destp := @workbuffy[0];

	todoheight := sizeYP;
	if toy <= sizeYP then todoheight := toy;

	if tox <= sizeXP then begin
		// Target width is less than one tile width.
		for y := todoheight - 1 downto 0 do begin
			move(srcp^, destp^, destrowsize);
			inc(destp, destrowsize);
			inc(srcp, srcrowsize);
		end;
	end

	else begin
		// Target width is more than one tile width.
		movesize := destrowsize - srcrowsize;
		for y := todoheight - 1 downto 0 do begin
			move(srcp^, destp^, srcrowsize);
			MemCopy(destp, destp + srcrowsize, movesize);
			inc(destp, destrowsize);
			inc(srcp, srcrowsize);
		end;
	end;

	// Extend the fully sideways-tiled buffer down to the bottom.
	if toy > sizeYP then MemCopy(@workbuffy[0], destp, destrowsize * (toy - sizeYP));

	bitmap := workbuffy; workbuffy := NIL;
	sizeXP := tox; sizeYP := toy;
	stride := (tox * bitDepth + 7) shr 3;
end;

// ------------------------------------------------------------------

initialization
finalization
end.

