#!/usr/bin/env python3

import sys
import os
import glob
import subprocess

testables = glob.glob("unittest/test_*.pas")
print(str(len(testables)) + " tests found")
sys.argv[0] = ""

for testable in testables:
	print("----------------------------------------------------------------------")
	args = ["fpc", testable] + sys.argv
	print("[" + ' '.join(args) + "]")
	result = subprocess.run(args)
	if result.returncode != 0: exit(1)

iswindows = (os.name == "nt")
for testable in testables:
	print("----------------------------------------------------------------------")
	bin = testable[:-4]
	if iswindows: bin = bin + ".exe"
	print("[" + bin + "]")
	result = subprocess.run(bin)
	if result.returncode != 0: exit(1)

print("All tests OK")
exit(0)
