unit mcscriptwriter;

{                                                                           }
{ Mooncore game script writer. Use to easily create a script memory block.  }
{ Copyright 2024 :: Kirinn Bunnylin / MoonCore                              }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

{$mode objfpc}
{$I-}

interface

uses mccommon, mcfileio;

type TScriptWriter = object
	labelList : array of ptruint;
	lineList : TStringBunch;
	jumpList : array of dword;
	finalBuffy : array of byte;
	nonCode : array of dword;
	localsList : array of dword;
	commentList : array of record
		ofs : dword;
		ctxt : UTF8string;
	end;
	lineIndex, finalBufSize, jumpCount, nonCodeCount, localsCount, commentCount : dword;
	{$note optimise: faster to have a single currentLine, copied to lineList on writebufln?} // avoids linelist[] index lookup on every write
	{$note optimise: currentline a fixed 64k bytearray, copy to new string on writebufln?} // avoids realloc after every writebuf
	// however, can't + strings normally into currentline as user; needs new writebufpre proc
	m_loader : TFileLoader;

	procedure WriteBuf(const line : UTF8string); inline;
	procedure WriteBuf(const line : UTF8string; linelen : dword); inline;
	procedure WriteBufLn(const line : UTF8string);
	procedure SetLineOfs(ofs : dword);
	procedure AddJump(address : dword);
	procedure SortJumpList;
	procedure AddComment(const _txt : UTF8string; _ofs : dword);
	procedure AddNonCodeSection(startofs : dword);
	function PassNonCode(var ofs : dword) : boolean;
	procedure AddLocalVar(index : dword);
	procedure Init(const loader : TFileLoader);
	procedure GenerateFinal(striplabels : boolean);
end;

var ScriptWriter : TScriptWriter;

implementation

procedure TScriptWriter.WriteBuf(const line : UTF8string); inline;
begin
	lineList[lineIndex] := lineList[lineIndex] + line;
end;

procedure TScriptWriter.WriteBuf(const line : UTF8string; linelen : dword); inline;
begin
	lineList[lineIndex] := lineList[lineIndex] + copy(line, 1, linelen);
end;

procedure TScriptWriter.WriteBufLn(const line : UTF8string);
begin
	{$note optimise: if linelist[lineindex] = '' then linelist[lineindex] := line else...}
	if line <> '' then WriteBuf(line);
	inc(lineIndex);
	if lineIndex >= dword(length(labelList)) then begin
		setlength(labelList, length(labelList) shl 1);
		setlength(lineList, length(labelList) shl 1);
	end;
	labelList[lineIndex] := m_loader.ofs;
	lineList[lineIndex] := '';
end;

procedure TScriptWriter.SetLineOfs(ofs : dword);
begin
	labelList[lineIndex] := ofs;
end;

procedure TScriptWriter.AddJump(address : dword);
begin
	if jumpCount >= dword(length(jumpList)) then setlength(jumpList, jumpCount + jumpCount shr 1);
	jumpList[jumpCount] := address;
	inc(jumpCount);
end;

procedure TScriptWriter.SortJumpList; {$note optimise: use insertion sort}
// Sort the jump list by address values - teleporting gnome, with duplicate removal.
var i, j, l : dword;
begin
	i := 0; j := $FFFFFFFF;
	while i < jumpCount do begin
		if (i <> 0) and (jumpList[i] = jumpList[i - 1]) then begin
			jumpList[i] := jumpList[jumpCount - 1];
			dec(jumpCount);
		end
		else if (i = 0) or (jumpList[i] > jumpList[i - 1]) then begin
			if j = $FFFFFFFF then
				inc(i)
			else begin
				i := j;
				j := $FFFFFFFF;
			end;
		end
		else begin
			l := jumpList[i];
			jumpList[i] := jumpList[i - 1];
			jumpList[i - 1] := l;
			j := i; dec(i);
		end;
	end;
end;

procedure TScriptWriter.AddComment(const _txt : UTF8string; _ofs : dword);
// Add a comment that will be printed in the final output when the given offset is reached.
// Don't use this for immediate comments, those should be output directly with WriteLine etc.
// Use this for delayed comments, eg. when reading a list of jumps, stash comments at the jump landing points
// describing where the jump came from. Particularly useful with choice outcomes.
var i : dword;
begin
	if commentCount >= dword(length(commentList)) then setlength(commentList, 16 + commentCount shl 1);
	// Keep the comments sorted at all times. Lowest ofs at start of list.
	i := commentCount;
	while i <> 0 do begin
		dec(i);
		if commentList[i].ofs <= _ofs then begin inc(i); break; end;
		commentList[i + 1] := commentList[i];
	end;
	commentList[i].ofs := _ofs;
	commentList[i].ctxt := _txt;
	inc(commentCount);
end;

procedure TScriptWriter.AddNonCodeSection(startofs : dword);
// Add a non-code section start offset into a sorted list of non-code sections. When your read offset reaches the
// lowest listed non-code offset, call PassNonCode to skip ahead to the next code section.
var i, j : dword;
begin
	if nonCodeCount >= dword(length(nonCode)) then setlength(nonCode, nonCodeCount + nonCodeCount shr 1 + 12);
	// Find the index to insert the new ofs.
	i := 0;
	while (i < nonCodeCount) and (nonCode[i] < startofs) do inc(i);

	if i < nonCodeCount then begin
		if nonCode[i] = startofs then exit; // already listed
		// Shift up all above the target index.
		for j := nonCodeCount - 1 downto i do nonCode[j + 1] := nonCode[j];
	end;

	nonCode[i] := startofs;
	inc(nonCodeCount);
end;

function TScriptWriter.PassNonCode(var ofs : dword) : boolean;
// Call this when the read offset has reached a known non-code script section. The next known code section is
// the next nearest jump label. Read offset is moved ahead there, and the non-code entry is discarded.
// Returns TRUE if reached the end of bytecode (so you can stop parsing).
var i, delcount : dword;
begin
	if jumpCount = 0 then exit(TRUE); // no known jumps at all, all remaining is noncode
	SortJumpList;
	i := jumpCount;
	while (i <> 0) and (jumpList[i - 1] > ofs) do dec(i);
	if i = jumpCount then exit(TRUE); // no known jumps after current location, all remaining is noncode
	i := jumpList[i];
	if i >= m_loader.buffySize then exit(TRUE); // jump out of bounds, no more code in this file
	ofs := i;
	delcount := 0;
	while (delcount < nonCodeCount) and (nonCode[delcount] < i) do inc(delcount);
	if (delcount <> 0) and (delcount < nonCodeCount) then
		for i := delcount to nonCodeCount - 1 do nonCode[i - delcount] := nonCode[i];
	dec(nonCodeCount, delcount);
	result := FALSE;
end;

procedure TScriptWriter.AddLocalVar(index : dword);
// Maintains a list of all local variables seen used in this script. These are of the form $v<index>, index in hex.
var i : dword;
begin
	if localsCount <> 0 then for i := localsCount - 1 downto 0 do
		if localsList[i] = index then exit;
	if localsCount >= dword(length(localsList)) then setlength(localsList, localsCount shl 1 + 8);
	localsList[localsCount] := index;
	inc(localsCount);
end;

procedure TScriptWriter.Init(const loader : TFileLoader); {$note optimise: init params to adjust starting arrays}
begin
	labelList := NIL; lineList := NIL; jumpList := NIL; finalBuffy := NIL;
	setlength(labelList, 640); // derive from source file size, given average bytecode to line output ratio
	setlength(lineList, 640); // maybe always limit these to 160, then when full, resize to 160 * src.remainingbytes / src.ofs + 16
	setlength(jumpList, 64); // limit to 40, then smart resize + 8
	setlength(nonCode, 64); // not used at all in some bytecode, default to 0 unless noncode:true
	localsList := NIL;
	labelList[0] := 0;
	lineList[0] := '';
	finalBufSize := 0;
	lineIndex := 0;
	jumpCount := 0;
	nonCodeCount := 0;
	localsCount := 0;
	commentCount := 0;
	m_loader := loader;
end;

procedure TScriptWriter.GenerateFinal(striplabels : boolean);
var i, l, comm, outofs : dword;
	txt : string[16];
	labelinjumplist : boolean;
begin
	SortJumpList;
	// Set up a buffer that can hold all script lines.
	finalBufSize := localsCount * 16;
	if lineIndex <> 0 then for i := lineIndex - 1 downto 0 do
		inc(finalBufSize, lineList[i].Length + 9);
	if commentCount <> 0 then for i := commentCount - 1 downto 0 do
		inc(finalBufSize, commentList[i].ctxt.Length + 4);
	setlength(finalBuffy, finalBufSize);
	outofs := 0; {$note optimise: use writep}

	// Print reset instructions for local variables.
	if localsCount <> 0 then begin
		for i := localsCount - 1 downto 0 do begin
			txt := '$v' + strhex(localsList[i]) + ':=0'#$A;
			move(txt[1], finalbuffy[outofs], length(txt));
			inc(outofs, length(txt));
		end;
	end;

	// Build the sakurascript lines into a single memory block.
	l := 0;
	comm := 0;
	if lineIndex <> 0 then for i := 0 to lineIndex - 1 do begin

		// If this line's label address is in jumplist, add a linebreak to visually differentiate code blocks.
		labelinjumplist := (l < jumpCount) and (labelList[i] >= jumpList[l]);
		if labelinjumplist then begin
			finalBuffy[outofs] := $A; inc(outofs);
			inc(l);
		end;

		// Print the label if this line's label address is in jumpList, or keeping all debug labels, and this label
		// is different from the previous line's label.
		if ((NOT striplabels) or (labelinjumplist))
		and ((i = 0) or (labelList[i] <> labelList[i - 1]))
		then begin
			txt := '@' + strhex(labelList[i], 4) + ':';
			move(txt[1], finalBuffy[outofs], length(txt));
			inc(outofs, length(txt));
			finalBuffy[outofs] := $20;
			inc(outofs);
		end;

		// Print stashed comments as their offset comes up.
		while (comm < commentCount) and (commentList[comm].ofs <= labelList[i]) do begin
			finalBuffy[outofs] := byte('/'); inc(outofs);
			finalBuffy[outofs] := byte('/'); inc(outofs);
			finalBuffy[outofs] := byte(' '); inc(outofs);
			with commentList[comm] do if ctxt <> '' then begin
				move(ctxt[1], finalBuffy[outofs], ctxt.Length);
				inc(outofs, ctxt.Length);
			end;
			finalBuffy[outofs] := $A; inc(outofs);
			inc(comm);
		end;

		// Print the generated script line.
		if lineList[i].Length <> 0 then begin
			move(lineList[i][1], finalBuffy[outofs], lineList[i].Length);
			inc(outofs, lineList[i].Length);
			lineList[i] := '';
		end;

		// Line break after each line.
		finalBuffy[outofs] := $A;
		inc(outofs);
	end;
	finalBufSize := outofs;
end;
end.

