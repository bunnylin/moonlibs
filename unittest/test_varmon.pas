program test_varmon;

{$mode objfpc}
{$ifdef WINDOWS}{$apptype console}{$endif}
{$codepage UTF8}
{$ASSERTIONS ON}
{$WARN 5024 OFF} // varname parameter not used

uses mcvarmon, mccommon;

var varmon : TVarmon;
	specialnumber : longint;
	specialstring : TStringBunch = NIL;
	stash : TStringBunch = NIL;
	txt : UTF8string;
	poku, poku2 : pointer;
	i, size1, size2 : dword;

function VarmonGetProvider(const varname : ShortString; const dest : TStringBunch) : longint;
var i : longint;
begin
	if dest <> NIL then for i := high(dest) downto 0 do dest[i] := specialstring[i];
	result := specialnumber;
end;

procedure VarmonSetProvider(const varname : ShortString; const src : TStringBunch; num : longint);
var i : longint;
begin
	if src <> NIL then for i := high(src) downto 0 do specialstring[i] := src[i];
	specialnumber := num;
end;

begin
 poku := NIL;
 poku2 := NIL;
 varmon := TVarmon.Create(1, 1);
 setlength(specialstring, 4);
 try
 writeln('max buckets=', VARMON_MAXBUCKETS);
 writeln('max languages=', VARMON_MAXLANGUAGES);

 with varmon do begin
 // Should start with a clean state even without an init.
 writeln(':: basics ::');
 Assert(CountBuckets > 0);
 stash := GetStrVar('non-existing string');
 Assert(stash.Length = 1);
 Assert(stash[0] = '');

 Assert(GetVarType('non-existing variable') = VT_NULL);

 // Strings and numbers can be set and get in variables.
 // (After the linebreak, that's a pound sign and hiragana ne~.)
 txt := 'Test string' + chr(0) + chr($A) + chr($C2) + chr($A3) + chr($E3) + chr($81) + chr($AD) + '~';
 SetStrVar('a', txt);
 SetNumVar('i', 32);
 Assert(GetVarType('a') = VT_STR);
 Assert(GetVarType('i') = VT_INT);
 stash := GetStrVar('a');
 Assert(stash.Length = 1);
 Assert(stash[0] = txt);
 Assert(GetNumVar('i') = 32);
 writeln('_' + txt + '_');

 // An existing variable can be overwritten with the same type.
 txt := 'banana';
 SetStrVar('a', txt);
 Assert(GetVarType('a') = VT_STR);
 stash := GetStrVar('a');
 Assert(stash[0] = txt);

 // An existing variable can be overwritten with a different type.
 SetNumVar('a', 82);
 Assert(GetVarType('a') = VT_INT);
 Assert(GetNumVar('a') = 82);
 txt := 'cherry';
 SetStrVar('a', txt);
 Assert(GetVarType('a') = VT_STR);
 stash := GetStrVar('a');
 Assert(stash[0] = txt);

 // Save current and wipe. Should be duly wiped.
 writeln(':: save1 ::');
 size1 := SaveVarState(poku);
 writeln(':: init1 ::');
 Init(0, 0);
 stash := GetStrVar('a');
 Assert(stash[0] = '');
 Assert(GetNumVar('i') = 0);

 // Set another variable or two and save again.
 SetStrVar('b', txt);
 SetStrVar('c', txt);
 SetNumVar('j', 99);
 SetNumVar('k', -99);
 writeln(':: save2 ::');
 size2 := SaveVarState(poku2);

 // Load state. The new variables should be gone, and the previous present.
 writeln(':: load1 ::');
 LoadVarState(poku, size1);
 Assert(GetVarType('b') = VT_NULL);
 Assert(GetVarType('c') = VT_NULL);
 Assert(GetVarType('j') = VT_NULL);
 Assert(GetVarType('k') = VT_NULL);
 Assert(GetVarType('a') = VT_STR);
 Assert(GetVarType('i') = VT_INT);
 stash := GetStrVar('A');
 writeln('_' + stash[0] + '_');
 Assert(stash[0] = txt);
 Assert(GetNumVar('I') = 32);

 // Load state again. New variables should be present.
 writeln(':: load2 ::');
 LoadVarState(poku2, size2);
 Assert(GetVarType('b') = VT_STR);
 Assert(GetVarType('c') = VT_STR);
 Assert(GetVarType('j') = VT_INT);
 Assert(GetVarType('k') = VT_INT);
 Assert(GetVarType('a') = VT_NULL);
 Assert(GetVarType('i') = VT_NULL);
 Assert(GetNumVar('k') = -99);

 // Creating new callbacked variables forwards their get/set to callbacks.
 varmon.callbackGetter := @VarmonGetProvider;
 varmon.callbackSetter := @VarmonSetProvider;

 specialnumber := 0;
 SetCallback('specnum', TRUE, VT_INT);
 SetNumVar('specnum', 77);
 Assert(specialnumber = 77);
 Assert(GetNumVar('specnum') = 77);

 specialstring[0] := '';
 txt := 'coconut';
 SetCallback('specstr', TRUE, VT_STR);
 SetStrVar('specstr', txt);
 Assert(specialstring[0] = txt);
 stash := GetStrVar('specstr');
 Assert(stash[0] = txt);

 // Changing the received string must not change the variable value by reference.
 stash[0] := 'broccoli';
 stash := GetStrVar('specstr');
 Assert(stash[0] = txt);

 // Converting existing normal variables to callbacked also works.
 SetNumVar('i', 33);
 SetCallback('i', TRUE, VT_INT);
 SetNumVar('i', 55);
 Assert(specialnumber = 55);
 Assert(GetNumVar('i') = 55);

 specialstring[0] := '';
 SetStrVar('a', 'fig');
 txt := 'kiwi';
 SetCallback('a', TRUE, VT_STR);
 SetStrVar('a', txt);
 Assert(specialstring[0] = txt);
 stash := GetStrVar('a');
 Assert(stash[0] = txt);

 // Callbacked variables can't change type, and their value is unchanged if attempted.
 i := 0;

 SetCallback('i', TRUE, VT_INT);
 SetNumVar('i', 11);
 try SetStrVar('i', txt);
 except inc(i);
 end;
 Assert(i = 1);
 Assert(GetNumVar('i') = 11);

 SetCallback('a', TRUE, VT_STR);
 SetStrVar('a', txt);
 try SetNumVar('a', 50);
 except inc(i);
 end;
 Assert(i = 2);
 stash := GetStrVar('a');
 Assert(stash[0] = txt);

 // Callbacking can be removed, after which variables can change type.
 SetCallback('i', FALSE, VT_INT);
 txt := 'guava';
 SetStrVar('i', txt);
 Assert(specialstring[0] <> txt);
 stash := GetStrVar('i');
 Assert(stash[0] = txt);

 SetCallback('a', FALSE, VT_STR);
 SetNumVar('a', 42);
 Assert(specialnumber <> 42);
 Assert(GetNumVar('a') = 42);

 // Deleting a non-existent variable should do nothing.
 writeln(':: delete ::');
 Init(1, 1);
 DeleteVar('a');
 Assert(GetVarType('a') = VT_NULL);

 // Deleting a number variable should not delete other variables.
 SetNumVar('i', 55);
 SetNumVar('j', 44);
 Assert(GetVarType('i') = VT_INT);
 Assert(GetVarType('j') = VT_INT);
 DeleteVar('i');
 Assert(GetVarType('i') = VT_NULL);
 Assert(GetNumVar('j') = 44);
 DeleteVar('j');
 Assert(GetNumVar('j') = 0);

 // Deleting a callbacked variable only clears its value.
 Init(1, 1);
 SetCallback('a', TRUE, VT_INT);
 SetNumVar('a', 32);
 i := 0;
 DeleteVar('a');
 Assert(GetNumVar('a') = 0);
 SetCallback('a', FALSE, VT_INT);
 DeleteVar('a');
 Assert(GetVarType('a') = VT_NULL);

 // Callbacked variables are saved and loaded through get/set callbacks.
 writeln(':: callback saveload ::');
 Init(0, 0);
 freemem(poku); poku := NIL;
 freemem(poku2); poku2 := NIL;

 SetCallback('a', TRUE, VT_STR);
 SetStrVar('a', txt);
 size1 := SaveVarState(poku);
 SetCallback('a', FALSE, VT_STR);
 DeleteVar('a');

 SetCallback('i', TRUE, VT_INT);
 SetNumVar('i', 69);
 size2 := SaveVarState(poku2);
 SetCallback('i', FALSE, VT_INT);
 DeleteVar('i');

 specialstring[0] := '';
 LoadVarState(poku, size1);
 Assert(specialstring[0] = txt);

 specialnumber := 0;
 LoadVarState(poku2, size2);
 Assert(specialnumber = 69);

 // Ignored variables work correctly during save and load.
 writeln(':: ignored saveload ::');
 Init(0, 0);
 freemem(poku); poku := NIL;

 SetCallback('a', TRUE, VT_STR);
 SetStrVar('a', 'melon');
 SetCallback('b', FALSE, VT_STR);
 SetStrVar('b', 'peach');
 SetCallback('i', TRUE, VT_INT);
 SetNumVar('i', 69);
 SetCallback('j', FALSE, VT_INT);
 SetNumVar('j', 88);

 IgnoreVar('a', TRUE);
 IgnoreVar('b', TRUE);
 IgnoreVar('i', TRUE);
 IgnoreVar('j', TRUE);
 size1 := SaveVarState(poku);

 SetStrVar('a', 'orange');
 SetStrVar('b', 'papaya');
 SetNumVar('i', 32);
 SetNumVar('j', 44);

 specialstring[0] := '';
 specialnumber := 0;
 LoadVarState(poku, size1);

 // Loading ignored (callbacked or not) variables doesn't set their values.
 Assert(specialstring[0] = '');
 Assert(specialnumber = 0);

 // Ignored callbacked variables fetch their value normally through callbacks.
 specialstring[0] := 'orange';
 stash := GetStrVar('a');
 Assert(stash[0] = 'orange');
 specialnumber := 32;
 Assert(GetNumVar('i') = 32);

 // Ignored non-callbacked variables are reset on load.
 stash[0] := '';
 stash := GetStrVar('b');
 Assert(stash[0] = '');
 Assert(GetNumVar('j') = 0);

 // Adding new variables should cause the bucket count to grow.
 writeln(':: autogrow ::');
 Init(1, 4);
 Assert(CountBuckets = 4);
 for i := 1 to 90 * 4 do SetNumVar(strdec(i), i);
 Assert(CountBuckets > 4);

 // Bucket count shouldn't autogrow if numbuckets set manually.
 writeln(':: noautogrow ::');
 Init(1, 1);
 SetNumBuckets(4);
 Assert(CountBuckets = 4);
 for i := 1 to 90 * 4 do SetNumVar(strdec(i), i);
 Assert(CountBuckets = 4);

 // Multiple languages also save correctly.
 writeln(':: multilang ::');
 Init(3, 1);
 stash := NIL; setlength(stash, 3);
 for i := 0 to high(stash) do stash[i] := strdec(i);
 SetStrVar('a', stash);
 freemem(poku); poku := NIL;
 size1 := SaveVarState(poku);
 Init(3, 1);
 LoadVarState(poku, size1);
 stash := GetStrVar('a');
 Assert(stash.Length = 3);
 for i := 0 to 2 do Assert(stash[i] = strdec(i));

 // Loading with a different current language count should work.
 Init(2, 1);
 LoadVarState(poku, size1);
 stash := GetStrVar('a');
 Assert(stash.Length = 2);
 for i := 0 to 1 do Assert(stash[i] = strdec(i));

 Init(4, 1);
 LoadVarState(poku, size1);
 stash := GetStrVar('a');
 Assert(stash.Length = 4);
 for i := 0 to 2 do Assert(stash[i] = strdec(i));
 Assert(stash[3] = stash[0]);

 // Fetching only one language string should work.
 Init(2, 1);
 for i := 0 to 1 do stash[i] := strdec(i);
 SetStrVar('a', stash);
 for i := 0 to 1 do stash[i] := strdec(i + 2);
 Assert(GetStrVar('a', 1) = '1');

 // The stash is not affected by the single get.
 Assert(stash[0] = '2');
 Assert(stash[1] = '3');

 // Getting a specific language non-existent variable returns an empty string.
 Assert(GetStrVar('tentacles', 0) = '');

 // Getting a string with language out of range returns language 0.
 Assert(GetStrVar('a', 2) = '0');

 // Variable access is case-insensitive.
 writeln(':: case sensitivity ::');
 Init(1, 1);
 SetNumVar('abCD', 66);
 Assert(GetNumVar('ABcd') = 66);
 DeleteVar('AbCd');
 Assert(GetVarType('abcd') = VT_NULL);
 Assert(GetVarType('ABCD') = VT_NULL);
 Assert(GetVarType('abCD') = VT_NULL);
 end;

 finally
  if poku <> NIL then begin freemem(poku); poku := NIL; end;
  if poku2 <> NIL then begin freemem(poku2); poku2 := NIL; end;
  if varmon <> NIL then begin varmon.Destroy; varmon := NIL; end;
 end;

 writeln('Tests passed.');
end.
