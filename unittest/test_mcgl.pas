// This is a set of functionality and performance tests for mcgloder.
{$mode objfpc}

uses {$ifdef UNIX}cthreads,{$endif}
 mcgloder, minicrt, mccommon;

var baseimage, testimage : mcg_bitmap;
    maxiterations : dword;

const basesizex = 640; basesizey = 400;

procedure MakeImage(sx, sy : dword; format : EBitmapFormat = MCG_FORMAT_BGRA; bpp : byte = 32);
var destp : pointer;
    counter : dword;
begin
 baseimage := mcg_bitmap.Init(sx, sy, format, bpp);
 with baseimage do begin
  palette := NIL;
  // Fill in the image with a messy deterministic pattern.
  destp := @bitmap[0];
  for counter := high(bitmap) downto 0 do begin
   byte(destp^) := byte(counter xor (counter shr 8));
   inc(destp);
  end;
 end;
end;

procedure MakePalette(var img : mcg_bitmap; count : dword);
begin
 img.palette := NIL;
 setlength(img.palette, count);
 while count <> 0 do begin
  dec(count);
  dword(img.palette[count]) := RorDword(word(count) * word(count), 1) xor count;
 end;
end;

procedure CopyImage;
begin
 testimage := mcg_bitmap.Init(baseimage.sizeXP, baseimage.sizeYP, baseimage.bitmapFormat, baseimage.bitDepth);
 if length(baseimage.palette) <> 0 then setlength(testimage.palette, length(baseimage.palette));
 move(baseimage.bitmap[0], testimage.bitmap[0], length(testimage.bitmap));
end;

// ---------------------------------------------
procedure TestScaler;

 procedure _Run(const testname : string; testx, testy : dword; form : EBitmapFormat);
 var totalresult, iteration : dword;
     tickcount : qword;
 begin
  totalresult := 0;
  for iteration := maxiterations - 1 downto 0 do begin
   // Make a fresh copy of the standard base image.
   CopyImage;
   // Override the image copy's format.
   with testimage do begin
    bitmapFormat := form;
    if form = MCG_FORMAT_BGR then bitDepth := 24 else bitDepth := 32;
    stride := (bitDepth * sizeXP + 7) shr 3;
    // Start counting.
    tickcount := GetMsecTime;
    // Resize the image!
    Resize(testx, testy);
    // Add up the time spent.
    inc(totalresult, GetMsecTime - tickcount);
   end;
   // Chill to hopefully get a full time slice for the next run.
   Delay(256);
   testimage.Destroy;
  end;
  // Report the result, except if this was a warmup run.
  if testname <> '' then
   writeln(testname, ': ', totalresult div maxiterations, ' ms');
 end;

begin
 writeln('scaler');
 // Build the base image.
 MakeImage(basesizex, basesizey);
 // Cache warmup.
 maxiterations := 1;
 _Run('', 32, 32, MCG_FORMAT_BGR);
 _Run('', 32, 32, MCG_FORMAT_BGRA);

 maxiterations := 12;
 // Test downscaling.
 _Run('Downscale24', basesizex * 7 div 8, basesizey * 7 div 8, MCG_FORMAT_BGR);
 _Run('Downscale32', basesizex * 7 div 8, basesizey * 7 div 8, MCG_FORMAT_BGRA);
 // Test integer upscaling.
 _Run('Intupscale24', basesizex * 2, basesizey * 2, MCG_FORMAT_BGR);
 _Run('Intupscale32', basesizex * 2, basesizey * 2, MCG_FORMAT_BGRA);
 // Test fractional upscaling.
 _Run('Fracupscale24', basesizex * 17 div 7, basesizey * 17 div 7, MCG_FORMAT_BGR);
 _Run('Fracupscale32', basesizex * 17 div 7, basesizey * 17 div 7, MCG_FORMAT_BGRA);
 // Clean up.
 baseimage.Destroy;
end;

// ---------------------------------------------
procedure TestCRC;

  procedure _Run(const testname : string);
  var totalresult, iteration, crc : dword;
      tickcount : qword;
  begin
   totalresult := 0;
   for iteration := maxiterations - 1 downto 0 do begin
    tickcount := GetMsecTime;
    crc := mcg_CalculateCRC(@baseimage.bitmap[0], @baseimage.bitmap[0] + length(baseimage.bitmap));
    inc(totalresult, GetMsecTime - tickcount);
    write(strhex(crc),#9);
    // Chill to hopefully get a full time slice for the next run.
    Delay(256);
   end;
   // Report the result, except if this was a warmup run.
   if testname <> '' then
    writeln(testname, ': ', totalresult div maxiterations, ' ms');
  end;

begin
 writeln('CRC');
 MakeImage(basesizex * 2, basesizey * 2);
 // Cache warmup.
 maxiterations := 1;
 _Run('');

 maxiterations := 12;
 _Run('medium CRC');
 baseimage.Resize(basesizex * 3, basesizey * 3);
 _Run('large CRC');
 baseimage.Destroy;
end;

procedure TestCrop;

  procedure _Run(const testname : string; format : EBitmapFormat; bpp : byte);
  var x, px1, px2, oldw, oldh : dword;
  begin
   writeln(testname);
   with baseimage do begin
    bitmapFormat := format;
    bitDepth := bpp;
    stride := (sizeXP * bpp + 7) shr 3;
    oldw := sizeXP;
    oldh := sizeYP;
    x := (12 * bpp + 7) shr 3;
    px1 := dword((@bitmap[x + stride])^);
    px2 := dword((@bitmap[stride * (sizeYP - 1) - x - 4])^);

    Crop(12, 12, 1, 1);

    Assert(sizeXP = oldw - 24);
    Assert(sizeYP = oldh - 2);
    Assert(stride = (sizeXP * bpp + 7) shr 3);
    Assert(dword((@bitmap[0])^) = px1);
    Assert(dword((@bitmap[sizeYP * stride - 4])^) = px2);
   end;
  end;

begin
 writeln('cropping');
 MakeImage(160, 16);
 _Run('rgba', MCG_FORMAT_BGRA, 32);
 _Run('rgbx', MCG_FORMAT_BGRX, 32);
 _Run('rgb', MCG_FORMAT_BGR, 24);
 _Run('indexed8', MCG_FORMAT_INDEXED, 8);
 _Run('indexed4', MCG_FORMAT_INDEXEDALPHA, 4);
 baseimage.Destroy;
end;

procedure TestExpander;

  procedure _Run(const testname : string; bpp : byte);
  var totalresult, iteration : dword;
      tickcount : qword;
  begin
   totalresult := 0;
   for iteration := maxiterations - 1 downto 0 do begin
    // Make a fresh copy of the standard base image.
    CopyImage;
    // Override the image copy's format.
    testimage.bitDepth := bpp;
    testimage.stride := (testimage.sizeXP * testimage.bitDepth + 7) shr 3;
    setlength(testimage.palette, 1 shl bpp);
    tickcount := GetMsecTime;
    testimage.ExpandIndexed(FALSE);
    inc(totalresult, GetMsecTime - tickcount);
    // Chill to hopefully get a full time slice for the next run.
    Delay(256);
    testimage.Destroy;
   end;
   // Report the result, except if this was a warmup run.
   if testname <> '' then
    writeln(testname, ': ', totalresult div maxiterations, ' ms');
  end;

begin
 writeln('expander');
 MakeImage(basesizex * 10, basesizey * 8, MCG_FORMAT_INDEXED, 8);
 // Cache warmup.
 maxiterations := 1;
 _Run('', 4);

 maxiterations := 10;
 _Run('medium 8bpp', 8);
 _Run('medium 4bpp', 4);
 _Run('medium 2bpp', 2);
 _Run('medium 1bpp', 1);
 baseimage.Destroy;

 MakeImage(basesizex * 16, basesizey * 12, MCG_FORMAT_INDEXED, 8);
 _Run('large 8bpp', 8);
 _Run('large 4bpp', 4);
 _Run('large 2bpp', 2);
 _Run('large 1bpp', 1);
 baseimage.Destroy;
 writeln('expander ok');
end;

procedure TestExpander16;

  procedure _Run(const testname : string; tobgrx : boolean);
  var totalresult, iteration : dword;
      tickcount : qword;
  begin
   totalresult := 0;
   for iteration := maxiterations - 1 downto 0 do begin
    // Make a fresh copy of the standard base image.
    CopyImage;
    tickcount := GetMsecTime;
    testimage.Expand16bpp(tobgrx);
    inc(totalresult, GetMsecTime - tickcount);
    // Chill to hopefully get a full time slice for the next run.
    Delay(256);
    testimage.Destroy;
   end;
   // Report the result, except if this was a warmup run.
   if testname <> '' then
    writeln(testname, ': ', totalresult div maxiterations, ' ms');
  end;

begin
 writeln('expander16');
 MakeImage(basesizex * 10, basesizey * 8, MCG_FORMAT_BGR16, 16);
 // Cache warmup.
 maxiterations := 1;
 _Run('', FALSE);

 maxiterations := 10;
 _Run('tobgr', FALSE);
 _Run('tobgrx', TRUE);
 baseimage.Destroy;
end;

procedure TestExpander32;

  procedure _Run(const testname : string);
  var totalresult, iteration : dword;
      tickcount : qword;
  begin
   totalresult := 0;
   for iteration := maxiterations - 1 downto 0 do begin
    // Make a fresh copy of the standard base image.
    CopyImage;
    tickcount := GetMsecTime;
    testimage.Force32bpp;
    inc(totalresult, GetMsecTime - tickcount);
    // Chill to hopefully get a full time slice for the next run.
    Delay(256);
    testimage.Destroy;
   end;
   // Report the result, except if this was a warmup run.
   if testname <> '' then
    writeln(testname, ': ', totalresult div maxiterations, ' ms');
  end;

begin
 writeln('expander32');
 MakeImage(basesizex * 10, basesizey * 8, MCG_FORMAT_BGR, 24);
 // Cache warmup.
 maxiterations := 1;
 _Run('');

 maxiterations := 10;
 _Run('from24');

 baseimage.bitmapFormat := MCG_FORMAT_BGR16;
 baseimage.bitDepth := 16;
 baseimage.stride := baseimage.sizeXP * 2;
 _Run('from16');
 baseimage.Destroy;
end;

procedure TestFlipper;

  procedure _Run(const testname : string; format : EBitmapFormat);
  var totalresult, iteration : dword;
      tickcount : qword;
  begin
   totalresult := 0;
   for iteration := maxiterations - 1 downto 0 do begin
    // Make a fresh copy of the standard base image.
    CopyImage;
    with testimage do begin
     bitmapFormat := format;
     if format = MCG_FORMAT_BGR then bitDepth := 24 else bitDepth := 32;
     stride := (bitDepth * sizeXP + 7) shr 3;
     tickcount := GetMsecTime;
     FlipRGB;
     inc(totalresult, GetMsecTime - tickcount);
    end;
    // Chill to hopefully get a full time slice for the next run.
    Delay(256);
    testimage.Destroy;
   end;
   // Report the result, except if this was a warmup run.
   if testname <> '' then
    writeln(testname, ': ', totalresult div maxiterations, ' ms');
  end;

begin
 writeln('flipper');
 MakeImage(basesizex * 10, basesizey * 8);
 // Cache warmup.
 maxiterations := 1;
 _Run('', MCG_FORMAT_BGRA);

 maxiterations := 10;
 _Run('flip bgra', MCG_FORMAT_BGRA);
 _Run('flip bgr', MCG_FORMAT_BGR);
 baseimage.Destroy;
end;

procedure TestFormats;

  procedure _Run(format : EBitmapFormat; bpp : byte);
  var pngbuffy : pointer;
      pngsize, i, j, amask : dword;
  begin
   writeln(bpp, 'bpp ', format, ', pal ', length(baseimage.palette));
   baseimage.bitmapFormat := format;
   baseimage.bitDepth := bpp;
   baseimage.stride := (baseimage.sizeXP * bpp + 7) shr 3;
   writeln('saving');
   baseimage.MakePNG(pngbuffy, pngsize);
   writeln('loading');
   if baseimage.bitmapFormat = MCG_FORMAT_BGRX then
    testimage := mcg_bitmap.FromPNG(pngbuffy, pngsize, [MCG_FLAG_FORCE32BPP])
   else
    testimage := mcg_bitmap.FromPNG(pngbuffy, pngsize, []);
   freemem(pngbuffy); pngbuffy := NIL;

   pngsize := baseimage.sizeYP * baseimage.stride;
   Assert(length(baseimage.bitmap) >= pngsize);
   Assert(length(testimage.bitmap) >= pngsize);
   Assert(testimage.stride = baseimage.stride);
   Assert(testimage.bitmapFormat = baseimage.bitmapFormat);
   Assert(testimage.bitDepth = baseimage.bitDepth);

   j := length(testimage.palette);
   Assert((j = length(baseimage.palette)) or (j = 1 shl testimage.bitDepth));
   if baseimage.palette <> NIL then begin
    amask := 0;
    if baseimage.bitmapFormat = MCG_FORMAT_INDEXED then amask := BEtoN(dword($FF));
    j := length(baseimage.palette);
    if length(testimage.palette) < j then j := length(testimage.palette);
    for i := j - 1 downto 0 do
     Assert(dword(testimage.palette[i]) = dword(baseimage.palette[i]) or amask);
   end;

   amask := 0;
   if baseimage.bitmapFormat = MCG_FORMAT_BGRX then amask := LEtoN(dword($FFFFFF));
   Assert(dword((@testimage.bitmap[0])^) and amask = dword((@baseimage.bitmap[0])^) and amask);
   dec(pngsize, 4);
   Assert(dword((@testimage.bitmap[pngsize])^) and amask = dword((@baseimage.bitmap[pngsize])^) and amask);

   testimage.Destroy; testimage := NIL;
  end;

begin
 writeln('formats');
 MakeImage(basesizex - 1, basesizey - 1); // excercise byte alignment too
 _Run(MCG_FORMAT_BGRA, 32);
 _Run(MCG_FORMAT_BGRX, 32);
 _Run(MCG_FORMAT_BGR, 24);

 MakePalette(baseimage, 256);
 _Run(MCG_FORMAT_INDEXED, 8);
 _Run(MCG_FORMAT_INDEXEDALPHA, 8);
 MakePalette(baseimage, 999);
 _Run(MCG_FORMAT_INDEXED, 8);
 _Run(MCG_FORMAT_INDEXEDALPHA, 8);
 setlength(baseimage.palette, 16);
 _Run(MCG_FORMAT_INDEXED, 8);
 _Run(MCG_FORMAT_INDEXEDALPHA, 8);

 _Run(MCG_FORMAT_INDEXED, 4);
 _Run(MCG_FORMAT_INDEXEDALPHA, 4);
 setlength(baseimage.palette, 4);
 _Run(MCG_FORMAT_INDEXED, 2);
 _Run(MCG_FORMAT_INDEXEDALPHA, 2);
 setlength(baseimage.palette, 2);
 _Run(MCG_FORMAT_INDEXED, 1);
 _Run(MCG_FORMAT_INDEXEDALPHA, 1);
 baseimage.Destroy;
end;

procedure TestPacker;

  procedure _Run(const testname : string; bpp : byte);
  var totalresult, iteration : dword;
      tickcount : qword;
  begin
   totalresult := 0;
   for iteration := maxiterations - 1 downto 0 do begin
    // Make a fresh copy of the standard base image.
    CopyImage;
    setlength(testimage.palette, 1 shl bpp);
    tickcount := GetMsecTime;
    testimage.PackBitDepth(1);
    inc(totalresult, GetMsecTime - tickcount);
    // Chill to hopefully get a full time slice for the next run.
    Delay(256);
    testimage.Destroy;
   end;
   // Report the result, except if this was a warmup run.
   if testname <> '' then
    writeln(testname, ': ', totalresult div maxiterations, ' ms');
  end;

begin
 writeln('packer');
 MakeImage(basesizex * 8, basesizey * 8, MCG_FORMAT_INDEXED, 8);
 // Cache warmup.
 maxiterations := 1;
 _Run('', 4);

 maxiterations := 10;
 _Run('4bpp', 4);
 _Run('2bpp', 2);
 _Run('1bpp', 1);
 baseimage.Destroy;
end;

procedure TestPremul;

  procedure _Run(const testname : string);
  var totalresult, iteration : dword;
      tickcount : qword;
  begin
   totalresult := 0;
   for iteration := maxiterations - 1 downto 0 do begin
    // Make a fresh copy of the standard base image.
    CopyImage;
    tickcount := GetMsecTime;
    with testimage do mcg_PremulRGBA32(@bitmap[0], sizeXP * sizeYP);
    inc(totalresult, GetMsecTime - tickcount);
    // Chill to hopefully get a full time slice for the next run.
    Delay(256);
    testimage.Destroy;
   end;
   // Report the result, except if this was a warmup run.
   if testname <> '' then
    writeln(testname, ': ', totalresult div maxiterations, ' ms');
  end;

begin
 writeln('premul');
 MakeImage(basesizex * 8, basesizey * 8);
 // Cache warmup.
 maxiterations := 1;
 _Run('');

 maxiterations := 10;
 _Run('alpha premul');
 baseimage.Destroy;
end;

procedure TestQuads;
var q : RGBAquad;
    l : linearquad;
begin
 writeln('testquads');
 q.FromRGBA($12345678);
 Assert(q.r = $12);
 Assert(q.g = $34);
 Assert(q.b = $56);
 Assert(q.a = $78);

 q.FromRGBA4($CAFE);
 Assert(q.r = $CC);
 Assert(q.g = $AA);
 Assert(q.b = $FF);
 Assert(q.a = $EE);

 Assert(q.ToRGBA4 = $CAFE);

 q.FromRGBA(0); Assert(dword(q) = 0);
 q.FromRGBA($FFFFFFFF); Assert(dword(q) = $FFFFFFFF);
 q.FromRGBA4(0); Assert(dword(q) = 0);
 q.FromRGBA4($FFFF); Assert(dword(q) = $FFFFFFFF);

 dword(q) := 0;
 l := mcg_SRGBtoLinear(q);
 Assert(l.b = 0);
 Assert(l.g = 0);
 Assert(l.r = 0);
 Assert(l.a = 0);
 q := mcg_LineartoSRGB(l);
 Assert(dword(q) = 0);

 dword(q) := $FFFFFFFF;
 l := mcg_SRGBtoLinear(q);
 Assert(l.b = $FFFF);
 Assert(l.g = $FFFF);
 Assert(l.r = $FFFF);
 Assert(l.a = $FF);
 q := mcg_LineartoSRGB(l);
 Assert(dword(q) = $FFFFFFFF);

 q.FromRGBA($80818288);
 l := mcg_SRGBtoLinear(q);
 Assert((l.b > 0) and (l.b < $8000));
 Assert((l.g > 0) and (l.g < $8000));
 Assert((l.r > 0) and (l.r < $8000));
 Assert(l.r < l.g);
 Assert(l.g < l.b);
 Assert(l.a = $88);
 q := mcg_LineartoSRGB(l);
 Assert(q.r = $80);
 Assert(q.g = $81);
 Assert(q.b = $82);
 Assert(q.a = $88);

 dword(q) := mcg_PremulRGBA32(0);
 Assert(dword(q) = 0);
 dword(q) := mcg_PremulRGBA32($FFFFFFFF);
 Assert(dword(q) = $FFFFFFFF);
 q.FromRGBA($04206080);
 dword(q) := mcg_PremulRGBA32(dword(q));
 Assert(q.r = $02);
 Assert(q.g = $10);
 Assert(q.b = $30);
 Assert(q.a = $80);
end;

// -------------------------------------------
begin
 if paramcount = 0 then begin
  writeln('Usage: mcgltest <testname>');
  writeln('  a - all');
  writeln('  c - CRC algorithm');
  writeln('  e - expand indexed');
  writeln('  f - flip RGB');
  writeln('  m - alpha premultiplication');
  writeln('  o - save/load formats');
  writeln('  p - pack indexed');
  writeln('  r - crop');
  writeln('  q - RGBAquads');
  writeln('  s - fractional scaler');
  writeln('  t - expand to truecolor');
  writeln('  x - expand 16bpp');
  exit;
 end;
 case paramstr(1) of
  'a': begin
        TestCRC; TestCrop; TestExpander; TestExpander16; TestExpander32; TestFlipper;
        TestFormats; TestPacker; TestPremul; TestQuads; TestScaler;
       end;
  'c': TestCRC;
  'e': TestExpander;
  'f': TestFlipper;
  'm': TestPremul;
  'o': TestFormats;
  'p': TestPacker;
  'r': TestCrop;
  'q': TestQuads;
  's': TestScaler;
  't': TestExpander32;
  'x': TestExpander16;
  else writeln('unknown option');
 end;
end.
