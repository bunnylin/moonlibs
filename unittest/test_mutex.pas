program test_mutex;

{$mode objfpc}
{$ifdef WINDOWS}{$apptype console}{$endif}
{$ASSERTIONS ON}

uses {$ifdef unix}cthreads,{$endif} sysutils, minimutex;

var handles : array of TThreadID;
    magicvalue : longint;
    sleepmsec : longint;
    mutex : TMiniMutex;
    sema : TMiniSemaphore;
    order : array of dword;
    throughcount : longint;

function MutexProc(parameter : pointer) : ptrint;
var i, j : longint;
begin
 sleep(random(20));
 for j := 1 to 4 do begin
  MiniMutexLock(mutex, sleepmsec);
  i := magicvalue;
  inc(magicvalue, ptrint(parameter));
  sleep(random(40));
  magicvalue := i + 1;
  MiniMutexRelease(mutex);
  ThreadSwitch;
 end;
 result := 0;
end;

function SemaOrderProc(parameter : pointer) : ptrint;
begin
 MiniSemaphoreWait(sema, sleepmsec);
 order[magicvalue] := ptruint(parameter); // remember entry order
 inc(magicvalue);
 sleep(50);
 MiniSemaphoreSignal(sema);

 MiniSemaphoreWait(sema, sleepmsec);
 // Check that second go around is in the same order.
 if order[(magicvalue mod 1000) - length(handles)] <> ptruint(parameter) then
  inc(magicvalue, 1000);
 inc(magicvalue);
 MiniSemaphoreSignal(sema);
 result := 0;
end;

function SemaMultiProc(parameter : pointer) : ptrint;
begin
 MiniSemaphoreWait(sema, sleepmsec);
 InterlockedIncrement(throughcount);
 // Remember the maximum threads simultaneously through the semaphore.
 if throughcount > magicvalue then InterlockedExchange(magicvalue, throughcount);
 sleep(random(40));
 InterlockedDecrement(throughcount);
 MiniSemaphoreSignal(sema);
 result := ptrint(parameter);
end;

procedure Run(numthreads : dword; const job : TThreadFunc);
begin
 if numthreads = 0 then exit;
 handles := NIL;
 setlength(handles, numthreads);
 for numthreads := high(handles) downto 0 do
  handles[numthreads] := BeginThread(job, pointer(ptrint(numthreads)));

 for numthreads := high(handles) downto 0 do begin
  WaitForThreadTerminate(handles[numthreads], 5000);
  CloseThread(handles[numthreads]);
 end;
end;

procedure TestMutex(numthreads : dword; _sleep : longint);
begin
 writeln('test mutex: ', numthreads, ' / sleep ', _sleep);
 magicvalue := 0;
 sleepmsec := _sleep;
 mutex := 0;
 Run(numthreads, @MutexProc);
 writeln('expecting magic value ', numthreads * 4, ', got ', magicvalue);
 Assert(magicvalue = numthreads * 4);
end;

procedure TestSemaOrder(numthreads : dword; _sleep : longint);
begin
 writeln('test semaphore order: ', numthreads, ' / sleep ', _sleep);
 magicvalue := 0;
 sleepmsec := _sleep;
 MiniSemaphoreInit(sema, 1);
 order := NIL;
 setlength(order, numthreads);
 Run(numthreads, @SemaOrderProc);
 writeln('expecting magic value ', numthreads * 2, ', got ', magicvalue);
 Assert(magicvalue = numthreads * 2);
end;

procedure TestSemaMulti(numthreads : dword; _sleep : longint);
begin
 writeln('test semaphore multiple: ', numthreads, ' / sleep ', _sleep);
 magicvalue := 0;
 throughcount := 0;
 sleepmsec := _sleep;
 MiniSemaphoreInit(sema, 3);
 Run(numthreads, @SemaMultiProc);
 writeln('expecting magic value 3, got ', magicvalue);
 Assert(magicvalue = 3);
 Assert(throughcount = 0);
end;

begin
 TestMutex(4, -1);
 TestMutex(4, 0);
 TestMutex(4, 25);
 TestMutex(16, -1);
 TestMutex(16, 0);
 TestMutex(16, 25);
 TestSemaOrder(4, -1);
 TestSemaOrder(4, 0);
 TestSemaOrder(4, 25);
 TestSemaOrder(16, -1);
 TestSemaOrder(16, 0);
 TestSemaOrder(16, 25);
 TestSemaMulti(4, -1);
 TestSemaMulti(4, 0);
 TestSemaMulti(4, 25);
 TestSemaMulti(16, -1);
 TestSemaMulti(16, 0);
 TestSemaMulti(16, 25);
 writeln('Tests passed.');
end.
