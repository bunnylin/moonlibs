program test_common;

{$mode objfpc}
{$ifdef WINDOWS}{$apptype console}{$endif}
{$codepage UTF8}
{$ASSERTIONS ON}
{$unitpath ..}
{$define !perftest}

uses
{$ifdef perftest}
cthreads, sysutils,
{$endif}
mccommon;

var	bunch : TStringBunch;
	s1, s2 : UTF8string;
	i : dword;
	p : pointer;
{$ifdef perftest}
	j, l : dword;
	q, total : qword;
{$endif}
	enumx : (Darius, Galaga, Raptor, Tyrian);

begin
 writeln(':: String length ::');
 bunch := NIL;
 s1 := '';
 assert(bunch.Length = 0);
 assert(s1.Length = 0);

 bunch.SetAll('cormorant');
 assert(bunch.Length = 0);

 s1 := 'oxygene';
 assert(s1.Length = 7);

 setlength(bunch, 3);
 assert(bunch.Length = 3);
 bunch.SetAll('equinoxe');
 assert(bunch.Length = 3);
 for i := 2 downto 0 do assert(bunch[i] = 'equinoxe');

 writeln(':: String split ::');
 // Empty input string -> empty result array.
 s1 := '';
 bunch := s1.Split('.', FALSE);
 assert(bunch.Length = 0);
 // No split characters found -> single result item.
 s1 := 'banana';
 bunch := s1.Split('.', FALSE);
 assert(bunch.Length = 1);
 assert(bunch[0] = 'banana');
 // Multiple split chars -> multiple result items.
 bunch := s1.Split('a', FALSE);
 assert(bunch.Length = 4);
 assert(bunch[0] = 'b');
 assert(bunch[1] = 'n');
 assert(bunch[2] = 'n');
 assert(bunch[3] = '');
 // Escapes are preserved and can ignore split char.
 s1 := 'ba.\na\.na';
 bunch := s1.Split('.', FALSE);
 assert(bunch.Length = 2);
 assert(bunch[0] = 'ba');
 assert(bunch[1] = '\na.na');
 // Trailing esc is kept.
 s1 := 'banana\';
 bunch := s1.Split('.', FALSE);
 assert(bunch.Length = 1);
 assert(bunch[0] = 'banana\');
 // All parts can be forced into lowercase.
 s1 := 'Ba.Na..NA';
 bunch := s1.Split('.', TRUE);
 assert(bunch.Length = 4);
 assert(bunch[0] = 'ba');
 assert(bunch[1] = 'na');
 assert(bunch[2] = '');
 assert(bunch[3] = 'na');

 writeln(':: String join ::');
 bunch := NIL;
 assert(bunch.Join('') = '');
 assert(bunch.Join('banana') = '');

 setlength(bunch, 1);
 bunch[0] := 'bonk';
 assert(bunch.Join('') = 'bonk');
 assert(bunch.Join('x') = 'bonk');

 setlength(bunch, 2);
 bunch[1] := 'donk';
 assert(bunch.Join('') = 'bonkdonk');
 assert(bunch.Join('x') = 'bonkxdonk');
 bunch[0] := '';
 bunch[1] := '';
 assert(bunch.Join('x') = 'x');

 writeln(':: String replace ::');
 s1 := '';
 assert(s1.Replace('', '') = '');
 assert(s1.Replace('', 'x') = '');
 assert(s1.Replace('x', '') = '');

 s1 := 'x';
 assert(s1.Replace('', '') = 'x');
 assert(s1.Replace('x', '') = '');
 assert(s1.Replace('x', 'y') = 'y');
 assert(s1.Replace('x', 'zzz') = 'zzz');

 s1 := 'xoxx';
 assert(s1.Replace('x', '') = 'o');
 assert(s1.Replace('xx', 'g') = 'xog');
 assert(s1.Replace('oxxx', 'g') = 'xoxx');

 bunch := NIL;
 assert(bunch.Replace('', '') = NIL);
 setlength(bunch, 1);
 bunch[0] := 'banana';
 bunch := bunch.Replace('a', 'x');
 assert(bunch.Length = 1);
 assert(bunch[0] = 'bxnxnx');

 setlength(bunch, 2);
 bunch[1] := 'ananas';
 bunch := bunch.Replace('n', 'g');
 assert(bunch.Length = 2);
 assert(bunch[0] = 'bxgxgx');
 assert(bunch[1] = 'agagas');

 writeln(':: String startswith ::');
 s1 := '';
 assert(s1.StartsWith('') = FALSE);
 assert(s1.StartsWith('a') = FALSE);
 s1 := 'guava';
 assert(s1.StartsWith('') = FALSE);
 assert(s1.StartsWith('g') = TRUE);
 assert(s1.StartsWith('guava') = TRUE);
 assert(s1.StartsWith('guanw') = FALSE);
 assert(s1.StartsWith('guavax') = FALSE);
 assert(s1.StartsWith('gurana') = FALSE);

 writeln(':: String endswith ::');
 s1 := '';
 assert(s1.EndsWith('') = FALSE);
 assert(s1.EndsWith('a') = FALSE);
 s1 := 'guava';
 assert(s1.EndsWith('') = FALSE);
 assert(s1.EndsWith('a') = TRUE);
 assert(s1.EndsWith('guava') = TRUE);
 assert(s1.EndsWith('guavax') = FALSE);
 assert(s1.EndsWith('aguava') = FALSE);

 writeln(':: String conversions ::');
 assert(strhex(255) = 'FF');
 assert(strhex(0) = '00');
 assert(strhex($7FFFFFFF) = '7FFFFFFF');
 assert(strhex($FFFFFFFF) = 'FFFFFFFF');
 assert(strhex($7FFFFFFFFFFFFFFF) = '7FFFFFFFFFFFFFFF');
 assert(strhex(QWord($FFFFFFFFFFFFFFFF)) = 'FFFFFFFFFFFFFFFF');
 assert(strhex(0, 1) = '0');
 assert(strhex(0, -1) = '0');
 assert(strhex(1, 3) = '001');
 assert(strhex($1234, 3) = '1234');
 assert(strhex(QWord($FFFFFFFFFFFFFFFF), 17) = '0FFFFFFFFFFFFFFFF');

 assert(strdec(0) = '0');
 assert(strdec(-0) = '0');
 assert(strdec(255) = '255');
 assert(strdec(-1) = '-1');
 assert(strdec($7FFFFFFF) = '2147483647');
 assert(strdec($FFFFFFFF) = '4294967295');
 assert(strdec(-2147483648) = '-2147483648');
 assert(strdec($7FFFFFFFFFFFFFFF) = '9223372036854775807');
 assert(strdec(QWord($FFFFFFFFFFFFFFFF)) = '18446744073709551615');
 assert(strdec(Int64(-9223372036854775808)) = '-9223372036854775808');

 writeln(':: Value conversions ::');
 assert(valx('0') = 0);
 assert(valx('') = 0);
 assert(valx('2147483639') = $7FFFFFF7);
 assert(valx('2147483647') = $7FFFFFF7);
 assert(valx('-2147483639') = -2147483639);
 assert(valx('-9999999999') = -2147483639);
 assert(valx('tentacles') = 0);
 assert(valx('123cat') = 123);
 assert(valx('cat456dog') = 456);
 assert(valx('walrus-789-roses') = -789);

 s1 := #0;
 assert(valx(@s1[1]) = 0);
 s1 := '0';
 assert(valx(@s1[1]) = 0);
 s1 := '-0';
 assert(valx(@s1[1]) = 0);
 s1 := '255';
 assert(valx(@s1[1]) = 255);
 s1 := '-1';
 assert(valx(@s1[1]) = -1);
 s1 := '2147483639';
 assert(valx(@s1[1]) = $7FFFFFF7);
 s1 := '2147483647';
 assert(valx(@s1[1]) = $7FFFFFF7);
 s1 := '-2147483639';
 assert(valx(@s1[1]) = -2147483639);
 s1 := '-9999999999';
 assert(valx(@s1[1]) = -2147483639);
 s1 := 'melon';
 assert(valx(@s1[1]) = 0);
 s1 := '123cat';
 assert(valx(@s1[1]) = 123);
 s1 := 'cat456dog';
 assert(valx(@s1[1]) = 456);
 s1 := 'walrus-789-roses';
 assert(valx(@s1[1]) = -789);

 assert(valhex('0') = 0);
 assert(valhex('') = 0);
 assert(valhex('FFFFFFFF') = $FFFFFFFF);
 assert(valhex('abc') = $ABC);
 assert(valhex('-1') = 1);
 assert(valhex('pi') = 0);
 assert(valhex('Acetone') = $ACE);
 assert(valhex('wombat') = $BA);

 writeln(':: strcat ::');
 assert(strcat('', []) = '');
 assert(strcat('baabaa', []) = 'baabaa');
 assert(strcat('+%', [0]) = '+0');
 assert(strcat('$&', [0]) = '$00'); // strhex always yields at least two digits
 assert(strcat('+%%%', [0]) = '+0%%');
 assert(strcat('+%%%', [1, 2, 3, 4, 5, 6, 7, 8]) = '+123');
 assert(strcat('%&', [TRUE, FALSE]) = 'truefalse');
 assert(strcat('%&', [chr(64), chr(65)]) = '@A');
 assert(strcat('% &', [-1, -1]) = '-1 FFFFFFFF');
 assert(strcat('% &', [int64(-1), int64(-1)]) = '-1 FFFFFFFFFFFFFFFF');
 assert(strcat('% &', [pointer(255), pointer($FF)]) = '255 FF');
 assert(strcat('% &', [qword($FFFFFFFFFFFFFFFF), qword($FFFFFFFFFFFFFFFF)]) = '18446744073709551615 FFFFFFFFFFFFFFFF');
 assert(strcat('a%d&g', ['bc', 'ef']) = 'abcdefg');
 assert(strcat('_%_&_', [UTF8string('äö'), UTF8string('ëïü')]) = UTF8string('_äö_ëïü_'));

 writeln(':: Path combine ::');
 assert(PathCombine([]) = '');
 assert(PathCombine(['']) = '');
 assert(PathCombine(['', '', '']) = '');
 assert(PathCombine(['moo']) = 'moo');
 assert(PathCombine(['\moo']) = '\moo');
 assert(PathCombine(['/moo']) = '/moo');
 assert(PathCombine(['\moo\']) = '\moo');
 assert(PathCombine(['/moo/']) = '/moo');
 assert(PathCombine(['\moo\\']) = '\moo');
 assert(PathCombine(['/moo//']) = '/moo');
 assert(PathCombine(['moo', 'baa']) = 'moo/baa');
 assert(PathCombine(['moo/', 'baa']) = 'moo/baa');
 assert(PathCombine(['moo', '/baa']) = 'moo/baa');
 assert(PathCombine(['moo/', '/baa']) = 'moo/baa');
 assert(PathCombine(['moo//', '//baa']) = 'moo/baa');
 assert(PathCombine(['moo\\', '\\baa']) = 'moo/baa');
 assert(PathCombine(['/', '', '\']) = '');
 assert(PathCombine(['\\badpath\\', '/\', '/banana\', 'kitten//']) = '\\badpath/banana/kitten');
 assert(PathCombine([], 'x') = '');
 assert(PathCombine([''], 'x') = '.x');
 assert(PathCombine(['s'], 'x') = 's.x');
 assert(PathCombine([' ', ' '], ' ') = ' / . ');
 assert(PathCombine(['moo', 'baa'], 'gronk') = 'moo/baa.gronk');

 writeln(':: ExtractFileNameWithoutExt ::');
 assert(ExtractFileNameWithoutExt('') = '');
 assert(ExtractFileNameWithoutExt('.') = '');
 assert(ExtractFileNameWithoutExt('moo') = 'moo');
 assert(ExtractFileNameWithoutExt('moo.') = 'moo');
 assert(ExtractFileNameWithoutExt('moo.cow') = 'moo');
 assert(ExtractFileNameWithoutExt('moo.cow.baa') = 'moo.cow');
 assert(ExtractFileNameWithoutExt('moo...') = 'moo..');
 assert(ExtractFileNameWithoutExt('.moo') = '');
 assert(ExtractFileNameWithoutExt('/') = '');
 assert(ExtractFileNameWithoutExt('\') = '');
 assert(ExtractFileNameWithoutExt('///') = '');
 assert(ExtractFileNameWithoutExt('\\\') = '');
 assert(ExtractFileNameWithoutExt('moo/') = '');
 assert(ExtractFileNameWithoutExt('moo/baa') = 'baa');
 assert(ExtractFileNameWithoutExt('moo\\baa') = 'baa');
 assert(ExtractFileNameWithoutExt('moo.cow/baa') = 'baa');
 assert(ExtractFileNameWithoutExt('moo.cow/baa.ewe') = 'baa');
 assert(ExtractFileNameWithoutExt('/mnt/cow/baa.ewe.eep') = 'baa.ewe');
 {$ifdef WINDOWS}
 assert(ExtractFileNameWithoutExt('C:meep.eep') = 'meep');
 {$endif}

 writeln(':: String compare ::');
 s1 := ''; s2 := ''; assert(CompStr(NIL, NIL, 0, 0) = 0);
 s1 := 'a'; s2 := 'b'; assert(CompStr(@s1[1], @s2[1], 1, 1) < 0);
 s1 := 'fff'; s2 := 'ffe'; assert(CompStr(@s1[1], @s2[1], 3, 3) > 0);
 s1 := 'fff'; s2 := 'ffe'; assert(CompStr(@s1[1], @s2[1], 2, 2) = 0);
 s1 := 'ff'; s2 := 'ffe'; assert(CompStr(@s1[1], @s2[1], 2, 3) < 0);
 s1 := 'ff'; s2 := 'fee'; assert(CompStr(@s1[1], @s2[1], 2, 3) > 0);
 s1 := 'A'; s2 := 'C'; assert(CompStr(@s1[1], @s2[1], 1, 1) = -2);
 s1 := 'AAAC'; s2 := 'AAAA'; assert(CompStr(@s1[1], @s2[1], 4, 4) = 2);

 writeln(':: String cut ::');
 i := 0; assert(CutNumberFromString('', i) = 0);
 i := 1; assert(CutNumberFromString('468', i) = 468);
 i := 1; assert(CutNumberFromString('-20', i) = -20);
 i := 3; assert(CutNumberFromString('468', i) = 8);
 i := 1; assert(CutNumberFromString('2147483639', i) = $7FFFFFF7);
 i := 1; assert(CutNumberFromString('12X45', i) = 12); assert(i = 3);
 i := 1; assert(CutNumberFromString('bonk88', i) = 88);
 i := 1; assert(CutNumberFromString('banana', i) = 0);

 s1 := #0; p := @s1[1];
 assert(CutHex(p) = 0);
 assert(p = @s1[1]);
 s1 := 'HotC0ffee'; p := @s1[1];
 assert(CutHex(p) = $C0FFEE);
 assert(p = @s1[1] + 9);

 writeln(':: String match ::');
 i := 0; assert(MatchString('', '', i) = FALSE);
 i := 0; assert(MatchString('bunnies', '', i) = FALSE); assert(i = 1);
 i := 0; assert(MatchString('bunnies', 'bunnies', i) = TRUE); assert(i = 8);
 i := 1; assert(MatchString('bunnies', 'bun', i) = TRUE); assert(i = 4);
 i := 3; assert(MatchString('bunnies', 'nni', i) = TRUE); assert(i = 6);
 i := 3; assert(MatchString('bunnies', 'xxx', i) = FALSE); assert(i = 3);
 i := 3; assert(MatchString('bunnies', 'nniesxxx', i) = FALSE); assert(i = 3);
 i := 99; assert(MatchString('bunnies', 'bunnies', i) = FALSE); assert(i = 99);

 writeln(':: String sort ::');
 setlength(bunch, 0);
 bunch.Sort;
 assert(bunch.Length = 0);

 setlength(bunch, 1);
 bunch[0] := '@';
 bunch.Sort;
 assert((bunch.Length = 1) and (bunch[0] = '@'));

 setlength(bunch, 3);
 bunch[0] := 'a'; bunch[1] := 'B'; bunch[2] := 'aa';
 bunch.Sort;
 assert(bunch[0] = 'B');
 assert(bunch[1] = 'a');
 assert(bunch[2] = 'aa');
 bunch.Sort;
 assert(bunch[0] = 'B');
 assert(bunch[1] = 'a');
 assert(bunch[2] = 'aa');

 bunch[2] := '';
 bunch.Sort;
 assert(bunch[0] = '');
 assert(bunch[1] = 'B');
 assert(bunch[2] = 'a');
 bunch[2] := '';
 bunch.Sort;
 assert(bunch[0] = '');
 assert(bunch[1] = '');
 assert(bunch[2] = 'B');

 writeln(':: strenum ::');
 enumx := tyrian;
 assert(strenum(typeinfo(enumx), @enumx) = 'Tyrian');
 longint(enumx) := 99;
 assert(strenum(typeinfo(enumx), @enumx) = '99');

 {$ifdef perftest}
 writeln(':: String sort performance ::');
 bunch := NIL;
 setlength(bunch, 999);
 total := 0;
 for l := 7 downto 0 do begin
  for i := high(bunch) downto 0 do begin
   setlength(bunch[i], random(35));
   for j := 1 to length(bunch[i]) do bunch[i][j] := chr(random(95) + 32);
  end;
  q := GetTickCount64;
  bunch.Sort;
  s1 := bunch[100]; bunch[100] := bunch[200]; bunch[200] := bunch[501];
  bunch[501] := bunch[300]; bunch[300] := bunch[250]; bunch[250] := s1;
  bunch.Sort;
  inc(total, GetTickCount64 - q);
 end;
 writeln(total, ' msec elapsed');
 for i := bunch.Length - 2 downto 0 do assert(bunch[i + 1] >= bunch[i]);
 {$endif}

 writeln('Tests passed.');
end.
