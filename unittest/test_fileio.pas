program test_fileio;

{$mode objfpc}
{$ifdef WINDOWS}{$apptype console}{$endif}
{$codepage UTF8}
{$ASSERTIONS ON}

uses {$ifdef unix}cthreads,{$endif} sysutils, mcfileio;

const testfile1 = 'deleteme.bin';
      testdir = 'subdir';
      testfile2 = testdir + DirectorySeparator + testfile1;
var   testfile3 : UTF8string;

var a, a2 : array[0..3] of dword;
    p : pointer;
    i, j : dword;
    f1, f2, f3 : file;
    loader : TFileLoader;
    writer : TBufferedFileWriter;
    txt : UTF8string;
    list : array of UTF8string;

begin
 testfile3 := upcase(testfile1);
 // Delete the test files, if they exist.
 assign(f1, testfile1);
 assign(f2, testfile2);
 assign(f3, testfile3);

 {$I-}
 erase(f1); while IOresult <> 0 do ;
 erase(f2); while IOresult <> 0 do ;
 erase(f3); while IOresult <> 0 do ;
 rmdir(testdir); while IOresult <> 0 do ;
 {$I+}

 // Set up a small binary file for testing.
 try
  a[0] := $76543210;
  a[1] := $CDEF89AB;
  a[2] := $44332211;
  a[3] := $45006655;
  a2[0] := 0; // silence a compiler warning

  writeln(':: SaveFile ::');
  // It should be possible to save a file in the current directory.
  SaveFile(testfile1, @a[0], 16);

  // The created file should be openable.
  filemode := 0; reset(f1, 1);
  // The created file should contain the saved content.
  blockread(f1, a2[0], 16);
  close(f1);
  for i := 0 to 3 do assert(a2[i] = a[i]);

  // A file can be saved in a subdirectory, and the subdirectory is created
  // if it doesn't exist.
  SaveFile(testfile2, @a[0], 16);

  // Check the created file.
  filemode := 0; reset(f2, 1);
  blockread(f2, a2[0], 16);
  close(f2);
  for i := 0 to 3 do assert(a2[i] = a[i]);

  // Case shenanigans should be handled. We modify the content of a[], and
  // resave with a capitalised filename. SaveFile deletes the original to
  // ensure the newly-saved file has the correct casing.
  a[2] := $FEEDBACC;
  SaveFile(testfile3, @a[0], 16);

  writeln(':: LoadFile ::');
  // Fileloader should be able to read a file.
  loader := TFileLoader.Open(testfile3);

  assert(loader.ofs = 0);
  assert(loader.buffySize = 16);
  assert(loader.fullFileSize = 16);
  assert(loader.RemainingBytes = 16);
  assert(loader.filename = testfile3);
  // Verify data reading functions.
  assert(loader.ReadDword = a[0]);
  assert(loader.ofs = 4);
  assert(loader.RemainingBytes = 12);
  assert(loader.ReadWord = word(a[1]));
  assert(loader.ReadByte = byte(a[1] shr 16));
  assert(loader.ofs = 7);
  assert(loader.RemainingBytes = 9);

  assert(loader.ReadByteFrom(4) = byte(a[1]));
  assert(loader.ReadWordFrom(4) = word(a[1]));
  assert(loader.ReadDwordFrom(0) = a[0]);
  assert(loader.ofs = 7);
  assert(loader.RemainingBytes = 9);

  // Verify bitreader.
  j := 0;
  loader.bitSelect := 0;
  for i := 7 downto 0 do begin
   j := j shl 1;
   if loader.ReadBit then j := j or 1;
  end;
  assert(j = byte(a[1] shr 24));
  assert(loader.bitSelect = 0);
  assert(loader.ofs = 8);

  // Wordwise bitreader.
  j := 0;
  for i := 15 downto 0 do begin
   j := j shl 1;
   if loader.ReadBitW then j := j or 1;
  end;
  assert(j = word(a[2]));
  assert(loader.bitSelect = 0);
  assert(loader.ofs = 10);

  // Multibit reader.
  i := loader.ReadBitsW(16);
  assert(i = a[2] shr 16);
  i := BEtoN(loader.ReadBits(32));
  assert(i = a[3]);
  assert(loader.bitSelect = 0);
  assert(loader.ofs = 16);

  // It should be possible to set ofs.
  loader.ofs := 4;
  assert(loader.ofs = 4);
  assert(loader.RemainingBytes = 12);
  assert(loader.ReadDword = a[1]);
  // Setting ofs out of bounds should stop at first invalid offset.
  loader.ofs := 9999;
  assert(loader.ofs = 16);
  assert(loader.RemainingBytes = 0);
  // Verify shenaniganned content.
  loader.ofs := 8;
  assert(loader.ReadDword = a[2]);

  // Verify zero-terminated stringreader.
  loader.ofs := $C;
  txt := loader.ReadString;
  assert(txt = chr(byte(a[3])) + chr(byte(a[3] shr 8)));
  assert(loader.ofs = $F);
  // Verify zero-terminated stringreader respects end of buffer.
  assert(loader.ReadString = chr(a[3] shr 24));
  assert(loader.ofs = $10);

  // Verify zero-terminated stringreader with custom offset.
  loader.ofs := 3;
  txt := loader.ReadStringFrom($C);
  assert(txt = chr(a[3] and $FF) + chr((a[3] shr 8) and $FF));
  assert(loader.ofs = 3);
  // Verify zero-terminated stringreader with custom offset respects end of
  // buffer.
  txt := loader.ReadStringFrom($F);
  assert(txt = chr(a[3] shr 24));
  // Verify buffy resize upward.
  loader.buffySize := 17;
  assert(loader.buffySize = 17);
  loader.ofs := 0;
  word((loader.readp + 15)^) := $ABCD;
  assert(loader.ReadWordFrom(15) = $ABCD);
  // Verify buffy resize downward.
  loader.buffySize := 6;
  assert(loader.buffySize = 6);
  assert(loader.ReadStringFrom(4) = chr(a[1] and $FF) + chr((a[1] shr 8) and $FF));
  // Verify write pointer acquiry.
  loader.ofs := 2;
  assert(loader.ReadDword = dword(loader.PtrAt(2)^));
  loader.Destroy;
  loader := NIL;

  // File loader should be able to restrict maximum read size.
  loader := TFileLoader.Open(testfile3, 0);
  assert(loader.ofs = 0);
  assert(loader.buffySize = 0);
  assert(loader.fullFileSize = 16);
  loader.ofs := 99;
  assert(loader.ofs = 0);
  loader.Destroy;
  loader := NIL;

  loader := TFileLoader.Open(testfile3, 4);
  assert(loader.ofs = 0);
  assert(loader.buffySize = 4);
  assert(loader.fullFileSize = 16);
  loader.ofs := 99;
  assert(loader.ofs = 4);
  assert(loader.RemainingBytes = 0);
  loader.Destroy;
  loader := NIL;

  writeln(':: LoadFile padded ::');

  // File loader should be able to add uninitialised padding to the buffer.
  loader := TFileLoader.Open(testfile3, -1, 3);
  assert(loader.ofs = 0);
  assert(loader.buffySize = 19);
  assert(loader.fullFileSize = 16);
  assert(loader.RemainingBytes = 19);
  loader.ofs := 14;
  assert(loader.ReadDword and $FFFF = a[3] shr 16);
  assert(loader.ofs = 18);
  loader.ofs := 99;
  assert(loader.ofs = 19);
  loader.Destroy;
  loader := NIL;

  loader := TFileLoader.Open(testfile3, 0, 2);
  assert(loader.ofs = 0);
  assert(loader.buffySize = 2);
  assert(loader.fullFileSize = 16);
  loader.ofs := 99;
  assert(loader.ofs = 2);
  loader.Destroy;
  loader := NIL;

  loader := TFileLoader.Open(testfile3, 4, 2);
  assert(loader.ofs = 0);
  assert(loader.buffySize = 6);
  assert(loader.fullFileSize = 16);
  loader.ofs := 99;
  assert(loader.ofs = 6);
  loader.Destroy;
  loader := NIL;

  loader := TFileLoader.Open(testfile3, -1, 0, 3);
  assert(loader.ofs = 3);
  assert(loader.ReadDword = a[0]);
  assert(loader.buffySize = 19);
  assert(loader.fullFileSize = 16);
  loader.Destroy;
  loader := NIL;

  writeln(':: FromMemory ::');
  getmem(p, 8);
  dword(p^) := $12345678;
  dword((p + 4)^) := $DABBAD00;
  loader := TFileLoader.FromMemory(p, 8, FALSE);
  assert(loader.ofs = 0);
  assert(loader.buffySize = 8);
  assert(loader.fullFileSize = 8);
  assert(loader.ReadDword = $12345678);
  assert(loader.ReadDword = $DABBAD00);
  assert(loader.RemainingBytes = 0);
  loader.Destroy;
  loader := NIL;

  loader := TFileLoader.FromMemory(p, 8, TRUE);
  assert(loader.ReadDword = $12345678);
  loader.Destroy;
  loader := NIL;
  p := NIL; // was freed on loader destroy

  loader := TFileLoader.FromMemory(NIL, 8, TRUE);
  qword(loader.readp^) := 1;
  loader.Destroy;
  loader := NIL;

  writeln(':: Crop ::');
  getmem(p, 8);
  qword(p^) := NtoLE(qword($123456789ABCDEF0));
  loader := TFileLoader.FromMemory(p, 8, TRUE);
  p := NIL;
  loader.Crop(0, 8);
  assert(loader.ofs = 0);
  assert(loader.buffySize = 8);

  loader.Crop(1, 7);
  assert(loader.ofs = 0);
  assert(loader.fullFileSize = 7);
  assert(loader.buffySize = 8);
  assert(byte(loader.readp^) = $DE);

  loader.Crop(0, 1);
  assert(loader.ofs = 0);
  assert(loader.fullFileSize = 1);
  assert(loader.buffySize = 8);
  assert(byte(loader.readp^) = $DE);

  loader.Crop(0, 0);
  assert(loader.ofs = 0);
  assert(loader.fullFileSize = 0);
  assert(loader.buffySize = 8);

  i := 0;
  try
    loader.Crop(69, 0); // cropping more than buffer size
  except
    inc(i);
  end;
  assert(i = 1);
  try
    loader.Crop(0, 69); // expanding beyond buffer size
  except
    inc(i);
  end;
  assert(i = 2);

  loader.Destroy;
  loader := NIL;

  writeln(':: Pad ::');
  getmem(p, 8);
  qword(p^) := $123456789ABCDEF0;
  loader := TFileLoader.FromMemory(p, 0, TRUE);
  p := NIL;
  loader.Pad(0, 0);
  assert(loader.ofs = 0);
  assert(loader.buffySize = 0);
  loader.Pad(1, 2);
  assert(loader.ofs = 1);
  assert(loader.buffySize = 3);

  byte(loader.PtrAt(0)^) := $12;
  word(loader.PtrAt(1)^) := $6988;
  loader.Pad(3, 0);
  assert(loader.ofs = 4);
  assert(loader.buffySize = 6);
  assert(loader.ReadWord = $6988);
  assert(loader.ReadByteFrom(3) = $12);
  loader.Destroy;
  loader := NIL;

  writeln(':: WildcardMatch ::');
  assert(WildcardMatch('', '') = TRUE);
  assert(WildcardMatch('a', '') = FALSE);
  assert(WildcardMatch('', 'a') = TRUE);
  assert(WildcardMatch('a','a') = TRUE);
  assert(WildcardMatch('a','abc') = FALSE);
  assert(WildcardMatch('abc','a') = FALSE);
  assert(WildcardMatch('abc.xyz','abc.xyz') = TRUE);
  assert(WildcardMatch('abc.xyz','abc.zyx') = FALSE);
  assert(WildcardMatch('abc.xyz','abc.') = FALSE);
  assert(WildcardMatch('abc.','abc.xyz') = FALSE);
  assert(WildcardMatch('abc.','abc.') = TRUE);
  assert(WildcardMatch('abc','abc.') = TRUE);
  assert(WildcardMatch('abc.','abc') = TRUE);
  assert(WildcardMatch('a?c','abc') = TRUE);
  assert(WildcardMatch('?','abc') = FALSE);
  assert(WildcardMatch('???','abc') = TRUE);
  assert(WildcardMatch('????','abc') = FALSE);
  assert(WildcardMatch('a*a','abc') = FALSE);
  assert(WildcardMatch('a*b','abc') = FALSE);
  assert(WildcardMatch('a*c','abc') = TRUE);
  assert(WildcardMatch('a**c','abc') = TRUE);
  assert(WildcardMatch('abc*','abc') = TRUE);
  assert(WildcardMatch('*abc','abc') = TRUE);
  assert(WildcardMatch('*','abc') = TRUE);
  assert(WildcardMatch('****','abc') = TRUE);
  assert(WildcardMatch('**b**','abc') = TRUE);
  assert(WildcardMatch('abc/xyz','abc/xyz') = TRUE);
  assert(WildcardMatch('abc/xyz','xyz') = FALSE);
  assert(WildcardMatch('abc/xyz','abc') = FALSE);
  assert(WildcardMatch('xyz','abc/xyz') = TRUE);
  assert(WildcardMatch('abc/def/xyz','abc/def/xyz') = TRUE);
  assert(WildcardMatch('*/xyz','abc/def/xyz') = TRUE);
  assert(WildcardMatch('a?c/*/xyz','abc/def/xyz.') = TRUE);
  assert(WildcardMatch('ABC/dEf/xyZ.gg','abC/def/XYZ.gG') = TRUE);
  assert(WildcardMatch('abc/*/xY?.gg','ABC/def/XYZ.gG') = TRUE);

  writeln(':: FindFiles_caseless ::');
  assert(FindFiles_caseless('') <> NIL);
  assert(Findfiles_caseless('bananas') = NIL);

  list := FindFiles_caseless(testfile1);
  assert(length(list) = 1);
  assert(list[0] = upcase(testfile1));
  assert(ExtractFilePath(list[0]) = ''); // searched for a pathless reference

  list := FindFiles_caseless(testfile1, TRUE);
  assert(list = NIL);
  list := FindFiles_caseless('test*');
  assert(length(list) > 1);
  list := FindFiles_caseless('test*', FALSE, TRUE); // firstonly
  assert(length(list) = 1);

  list := FindFiles_caseless('./' + testfile1, FALSE, FALSE, TRUE); // onlynames
  assert(length(list) = 1);
  assert(list[0] = upcase(testfile1));
  list := FindFiles_caseless('./' + testfile1, FALSE, FALSE, FALSE);
  assert(length(list) = 1);
  assert(list[0] = '.' + DirectorySeparator + upcase(testfile1));
  list := FindFiles_caseless('./test*', FALSE, FALSE, TRUE);
  assert(length(list) > 1);
  for i := high(list) downto 0 do
    assert(list[i][1] <> '.');
  list := FindFiles_caseless('./test*', FALSE, FALSE, FALSE);
  assert(length(list) > 1);
  for i := high(list) downto 0 do
    assert(list[i][1] = '.');

  list := FindFiles_caseless(testdir, TRUE);
  assert(length(list) = 1);
  assert(list[0] = testdir);
  list := FindFiles_caseless(testdir, FALSE);
  assert(list = NIL);
  list := FindFiles_caseless('*/' + testfile1);
  assert(length(list) = 1);
  assert(list[0] = testfile2);

  writeln(':: BufferedFileWriter ::');

  // Automatic default buffer.
  writer := TBufferedFileWriter.Create(testfile1, NIL, 0);
  assert(writer.buffySize = 1024 * 1024);
  writer.WriteByte(255);
  writer.WriteWord(65535);
  writer.WriteDword($FFFFFFFF);
  writer.WriteString('banana');
  writer.WriteLine('ananas');
  writer.Destroy;
  writer := NIL;

  // Automatic specific-size buffer.
  writer := TBufferedFileWriter.Create(testfile1, NIL, 1);
  assert(writer.buffySize = 1);
  writer.WriteByte(255);
  writer.WriteWord(65535);
  writer.WriteDword($FFFFFFFF);
  writer.WriteString('banana');
  writer.WriteLine('ananas');
  writer.Destroy;
  writer := NIL;

  // Caller-provided buffer.
  getmem(p, 1);
  writer := TBufferedFileWriter.Create(testfile1, p, 1);
  assert(writer.buffySize = 1);
  writer.WriteByte(255);
  writer.WriteWord(65535);
  writer.WriteDword($FFFFFFFF);
  writer.WriteString('banana');
  writer.WriteLine('ananas');
  writer.Destroy;
  writer := NIL;
  freemem(p);
  p := NIL;

 finally
  {$I-}
  erase(f1); while IOresult <> 0 do ;
  erase(f2); while IOresult <> 0 do ;
  erase(f3); while IOresult <> 0 do ;
  rmdir(testdir); while IOresult <> 0 do ;
  if loader <> NIL then begin loader.Destroy; loader := NIL; end;
  if writer <> NIL then begin writer.Destroy; writer := NIL; end;
 end;

 writeln('Tests passed.');
end.
