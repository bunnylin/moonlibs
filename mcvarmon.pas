unit mcvarmon;
{                                                                           }
{ Mooncore Varmon variable management system                                }
{ Copyright 2016-2023 :: Kirinn Bunnylin / MoonCore                         }
{                                                                           }
{ Available under the zlib license:                                         }
{                                                                           }
{ This software is provided 'as-is', without any express or implied         }
{ warranty.  In no case will the authors be held liable for any damages     }
{ arising from the use of this software.                                    }
{                                                                           }
{ Permission is granted to anyone to use this software for any purpose,     }
{ including commercial applications, and to alter it and redistribute it    }
{ freely, subject to the following restrictions:                            }
{                                                                           }
{ 1. The origin of this software must not be misrepresented; you must not   }
{    claim that you wrote the original software. If you use this software   }
{    in a product, an acknowledgment in the product documentation would be  }
{    appreciated but is not required.                                       }
{ 2. Altered source versions must be plainly marked as such, and must not   }
{    be misrepresented as being the original software.                      }
{ 3. This notice may not be removed or altered from any source              }
{    distribution.                                                          }
{                                                                           }

// Variables are referenced by shortstring ascii names. Existing variable names are kept in hash buckets so they're
// quick to access. In each bucket, variable names are in a linked list, where any accessed name is moved to the top of
// the list, so scanning from the top of the list finds recently used variables fast.
//
// Numeric values are directly attached to each variable item, but strings are kept in a separate direct array.
// All public access to the arrays is via getters and setters. Variable names are all case-insensitive.
//
// String variables are stored as a set, since when dealing with multiple simultaneous languages, it's necessary to
// keep a copy of every string in every language. When getting a string, if it is not available in the requested
// language, empty strings are returned.
//
// SaveVarState can be used to create a packed snapshot of the current set of variables, which is easy to include in
// a save file, perhaps with added compression. LoadVarState restores the saved state.
//
// To facilitate game access to engine variables, it's possible to define a callback get/set. Accessing a variable
// flagged with useCallbacks is forwarded to the callbacks instead of a value in the hash buckets. Such variable values
// are saved and restored in snapshots like normal values. Because the setter is a callback, you have full discretion
// over whether to apply a variable change or not, so you can have readonly variables that ignore sets. To declare
// a callbacked variable, call SetCallback with usecallback = TRUE. After this, any access to the variable uses
// redirected gets and sets, and it can't be deleted or its type changed.

// ------------------------------------------------------------------

{$mode objfpc}
{$codepage UTF8}

interface

uses
{$ifdef UNIX}cthreads,{$endif}
sysutils, mccommon;

type EVarType = (VT_NULL = 0, VT_INT = 1, VT_STR = 2);

// Todo: try to improve hash table efficiency. Ref: https://thenumb.at/Hashtables/
// Currently this uses separate chaining; an array of buckets, each containing a linked list of content.
// This entails an occasional cache miss on bucket access, and probable miss on content access. And another probable
// miss if it's a string, although that one's unavoidable.
// Using open addressing entails a probable cache miss on bucket access, but since content is flattened on the same
// array, the second cache miss is avoided. Robin Hood probing with backshift deletion sounds like the preferred
// implementation.
// Adding a last-used index probably won't help. FindVar could short-circuit straight to the known index if the
// previous accessed variable was the same one. But the bucket array is accessed either way, and the last-used index is
// likely to still be nearly cached, so this is unlikely to avoid cache misses.
// Limiting the max length for variable names would help. Keeping them in another table would solve that, but would
// cause an extra cache miss, defeating the point. Variable-length storage won't work since each content entry must
// have a fixed size or can't be random-accessed.
// Does FPC do an automatic UTF8string to ShortString conversion copy? If slow, would have to change every ShortString
// here to a pointer at string start plus byte length. However, minimal test prog shows only marginal speedup thus.

type TVarmon = class
	private
		bucket : array of record
			bucketSize : dword;
			topLink : dword;
			content : array of record
				strValue : TStringBunch;
				numValue : longint;
				prevLink, nextLink : dword; // prev = topward, next = bottomward
				varType : EVarType;
				useCallbacks : boolean;
				ignoreSave : boolean;
				name : ShortString;
			end;
		end;

		bucketCount, numLanguages : dword;
		addBucketsAutomatically : boolean;

		function HashString(const hashname : ShortString) : dword;
		procedure Promote(_bucket, index : dword);
		function FindVar(const findname : ShortString) : dword;
		function AddVar(const newvarname : ShortString; newvartype : EVarType) : dword;

	public
		callbackGetter : function (const varname : ShortString; const dest : TStringBunch) : longint;
		callbackSetter : procedure (const varname : ShortString; const src : TStringBunch; num : longint);

		procedure LoadVarState(inbuf : pointer; bufbytes : dword);
		function SaveVarState(out outbuf : pointer) : dword;

		function GetVarType(const varname : ShortString) : EVarType;
		procedure SetNumVar(const varname : ShortString; num : longint);
		function GetNumVar(const varname : ShortString) : longint;
		procedure SetStrVar(const varname : ShortString; const txt : TStringBunch);
		procedure SetStrVar(const varname : ShortString; const txt : UTF8string);
		function GetStrVar(const varname : ShortString) : TStringBunch;
		function GetStrVar(const varname : ShortString; language : byte) : UTF8string;
		procedure IgnoreVar(const varname : ShortString; ignore : boolean);
		procedure DeleteVar(const varname : ShortString);
		procedure SetCallback(const varname : ShortString; usecallback : boolean; _vartype : EVarType);
		procedure SetNumBuckets(const newbuckets : dword);
		function CountBuckets : dword;
		procedure Init(numlang, numbuckets : dword);
		constructor Create(numlang, numbuckets : dword);
		{$define !debugdump}
		{$ifdef debugdump}
		procedure DumpVarTable;
		destructor Destroy; override;
		{$endif}
	end;

const
VARMON_MAXBUCKETS = 65536; // hashing assumes this is the hard maximum
VARMON_MAXLANGUAGES = 255;

// ------------------------------------------------------------------

implementation

{$ifdef debugdump}
procedure TVarmon.DumpVarTable;
var b, cont : dword;
begin
	{$ifdef fullvartable}
	for b := 0 to bucketCount - 1 do with bucket[b] do begin
		if bucketSize <> 0 then
			for cont := 0 to bucketSize - 1 do
				with content[cont] do begin
					write('$', name, #9'bucket=', b, ':', cont);
					if cont = bucket[b].topLink then write('*');
					write(#9'prev=', prevLink, #9'next=', nextLink, #9'type=', byte(varType),
						#9'callback=', useCallbacks, #9'ignore=', ignoreSave, #9);
					if varType = VT_INT then
						write(numValue)
					else
						if (varType = VT_STR) and (strValue <> NIL) then write(strValue.Join(':'));
					writeln;
				end;
	end;
	{$else}
	cont := 0;
	for b := 0 to bucketCount - 1 do with bucket[b] do if bucketSize = 0 then inc(cont);
	writeln('Varmon bucket count: ', bucketCount,' (', cont, ' empty)');
	cont := 0;
	for b := 0 to bucketCount - 1 do inc(cont, bucket[b].bucketSize);
	writeln('Total content entries: ', cont,' (', (cont / bucketCount):0:2, ' per bucket)');
	{$endif}
end;
{$endif}

function TVarmon.HashString(const hashname : ShortString) : dword;
// Returns the bucket number that this variable name belongs to.
var nameptr : pointer;
	i : dword;
	strlen : byte;
begin
	result := 0;
	strlen := hashname.Length;
	nameptr := @hashname[0]; // must be [0] in case of empty string
	i := strlen shr 2;
	while i <> 0 do begin
		result := RORdword(result, 13) xor dword(nameptr^);
		inc(nameptr, 4);
		dec(i);
	end;
	// Leftovers.
	if strlen and 2 <> 0 then begin
		result := ROLdword(result, 3) xor word(nameptr^);
		inc(nameptr, 2);
	end;
	if strlen and 1 <> 0 then result := ROLdword(result, 3) xor byte(nameptr^);

	// Fold upper halves to avoid throwing away bits.
	result := result xor (result shr 16);
	if bucketCount <= 256 then result := result xor (result shr 8);

	result := result mod bucketCount;
end;

procedure TVarmon.Promote(_bucket, index : dword);
// Attaches the given index as the start of a bucket content chain. The index must not currently be attached.
begin
	with bucket[_bucket] do begin
		content[index].nextLink := topLink;
		content[index].prevLink := content[topLink].prevLink;
		content[content[topLink].prevLink].nextLink := index;
		content[topLink].prevLink := index;
		topLink := index;
	end;
end;

function TVarmon.FindVar(const findname : ShortString) : dword;
// Checks if the given variable name exists in the variable buckets. If it does, the variable is moved to the top of
// its bucket, and FindVar returns the bucket's number. If the variable doesn't exist, returns the bucket's number
// OR'd with $8000 0000 so the caller can add it to the bucket if desired.
var i, j : dword;
begin
	result := HashString(findname);
	i := bucket[result].bucketSize;

	if i = 0 then begin
		// Empty bucket! this variable name doesn't exist yet.
		result := result or $80000000;
		exit;
	end;

	with bucket[result] do begin
		j := topLink;
		while i <> 0 do begin
			if content[j].name = findname then begin
				// Found a match!
				// Move the matched variable name to the top, if not already at the top.
				if topLink <> j then begin
					// Detach from current link position.
					with content[j] do begin
						content[prevLink].nextLink := nextLink;
						content[nextLink].prevLink := prevLink;
					end;
					// Attach at top link position.
					Promote(result, j);
				end;
				exit; // return the bucket number
			end;
			j := content[j].nextLink;
			dec(i);
		end;
	end;

	// No match found in bucket, this variable name doesn't exist yet.
	result := result or $80000000;
end;

function TVarmon.AddVar(const newvarname : ShortString; newvartype : EVarType) : dword;
// Checks if the given variable name exists yet. If not, creates it. If it exists, overwrites it.
// Variables are not initialised with any value, so the added variable's content is undefined.
// Returns the bucket number where the variable can be found topmost.
var poku : pointer;
	i : dword;
begin
	if NOT (newvartype in [VT_INT, VT_STR]) then
		raise Exception.Create(strcat('AddVar: bad type % for %', [newvartype, newvarname]));
	result := FindVar(newvarname);
	if result < $80000000 then
		// This variable already exists...
		with bucket[result].content[bucket[result].topLink] do begin
			if varType <> newvartype then begin
				if useCallbacks then
					raise Exception.Create(strcat('AddVar: can''t change % type to %', [newvarname, newvartype]));
				varType := newvartype;
			end;
			exit;
		end;

	// This variable doesn't exist... clear the inexistence bit.
	result := result and $7FFFFFFF;

	// If the content array is getting quite big, and automatic bucket management is enabled, add more buckets. This
	// causes a slight delay as all existing variables are redistributed.
	if (addBucketsAutomatically) and (bucketCount < VARMON_MAXBUCKETS)
	and (bucket[result].bucketSize * bucket[result].bucketSize > bucketCount) then begin
		i := SaveVarState(poku);
		inc(dword(poku^), 11); // poke a bigger bucket count directly in; increase by a prime to maximise distribution
		LoadVarState(poku, i);
		freemem(poku); poku := NIL;
		// Find the new bucket this variable will go in.
		result := FindVar(newvarname) and $7FFFFFFF;
	end;

	with bucket[result] do begin
		// Make sure the bucket's content array is big enough for a new slot.
		if bucketSize >= dword(length(content)) then setlength(content, length(content) + 16);

		with content[bucketSize] do begin
			name := newvarname;
			varType := newvartype;
			useCallbacks := FALSE;
			ignoreSave := FALSE;
		end;
		// Make the new item the topmost in the bucket's chain.
		Promote(result, bucketsize);
		inc(bucketSize);
	end;
end;

procedure TVarmon.LoadVarState(inbuf : pointer; bufbytes : dword);
// The input variable must be a pointer to a packed snapshot created by SaveVarState. The snapshot is extracted over
// existing variable data, including overwriting the bucket count.
// If the current number of languages is fewer than the languages in the snapshot, the extra language string variables
// are dropped. If the current languages is greater, then language 0 is copied to the extra languages.
// The caller is responsible for freeing the buffer.
// The savestate buffer is not robustly checked for correctness, beware.
var readp, endp, namep : pointer;
	i, j, k : dword;
	savedbuckets, savedlanguages : dword;
	_special : byte;
begin
	// Safeties, read the header.
	if inbuf = NIL then raise Exception.Create('LoadVarState: tried to load null');
	if bufbytes < 12 then raise Exception.Create('LoadVarState: too tiny, corrupted?');

	readp := inbuf;
	endp := inbuf + bufbytes;
	savedbuckets := LEtoN(dword(readp^)); inc(readp, 4);
	addBucketsAutomatically := (savedbuckets and $80000000) <> 0;
	savedbuckets := savedbuckets and $7FFFFFFF;
	if savedbuckets = 0 then savedbuckets := 1;
	if savedbuckets > VARMON_MAXBUCKETS then savedbuckets := VARMON_MAXBUCKETS;
	//numstrings := LEtoN(dword(readp^));
	inc(readp, 4);
	savedlanguages := LEtoN(dword(readp^)); inc(readp, 4);
	if savedlanguages = 0 then raise Exception.Create('LoadVarState: state savedlanguages = 0');
	if savedlanguages > VARMON_MAXLANGUAGES then
		raise Exception.Create('LoadVarState: state savedlanguages ' + strdec(savedlanguages) + ' > max');

	// Clear and re-init everything.
	// Note, that this will not change numLanguages, even if the savestate has a different language count! numLanguages
	// can only be equal to however many languages the caller currently has available. So while loading a state, any
	// extra saved languages are dropped; if the state is missing languages, then language 0 is substituted for the
	// missing strings.
	Init(numLanguages, savedbuckets);

	// Read the variables into memory.
	while readp + 4 <= endp do begin
		i := byte(readp^); inc(readp); // variable type
		_special := byte(readp^); inc(readp);
		namep := readp;
		k := AddVar(ShortString(namep^), EVarType(i));
		with bucket[k] do content[topLink].useCallbacks := (_special and 1 <> 0);
		inc(readp, byte(namep^) + 1);
		if _special and 2 <> 0 then begin
			IgnoreVar(ShortString(namep^), TRUE);
			continue;
		end;

		with bucket[k] do with content[topLink] do case EVarType(i) of
			VT_INT:
			begin
				numValue := LEtoN(longint(readp^));
				if useCallbacks then begin
					Assert(callbackSetter <> NIL, 'specify callbackSetter first');
					callbackSetter(ShortString(namep^), NIL, numValue);
				end;
				inc(readp, 4);
			end;

			VT_STR:
			begin
				setlength(strValue, numLanguages);
				i := 0;
				repeat
					// numLanguages and savedlanguages are hopefully equal, but if not, there are four options for each
					// language index...
					if i < numLanguages then begin
						if i < savedlanguages then begin
							// slot is within loaded languages and within languages in snapshot:
							// copy snapshot language string normally
							j := LEtoN(dword(readp^)); inc(readp, 4);
							setlength(strValue[i], j);
							if j <> 0 then begin
								move(readp^, strValue[i][1], j);
								inc(readp, j);
							end;
						end
						else begin
							// slot is within loaded languages but beyond languages in snapshot:
							// copy language 0 string
							strValue[i] := strValue[0];
						end;
					end
					else begin
						if i < savedlanguages then begin
							// slot is beyond loaded languages but within languages in snapshot:
							// this language string must be dropped, there's nowhere to put it
							inc(readp, LEtoN(dword(readp^)) + 4);
						end
						else begin
							// slot is beyond loaded languages and beyond languages in snapshot:
							// we're done with this string set!
							break;
						end;
					end;

					inc(i);
				until FALSE;

				if useCallbacks then begin
					Assert(callbackSetter <> NIL, 'specify callbackSetter first');
					callbackSetter(ShortString(namep^), strValue, 0);
				end;
			end;

			else raise Exception.Create('LoadVarState: ' + ShortString(namep^) + ' has bad type ' + strdec(i));
		end;
	end;
end;

function TVarmon.SaveVarState(out outbuf : pointer) : dword;
// The input variable must be a null pointer. A packed snapshot of the current variable state is placed there.
// Returns the number of bytes used for the snapshot.
// The caller is responsible for freeing the buffer, and may want to compress the buffer if saving in a file.
//
// Snapshot content (dwords in little-endian x86 order):
//   dwords : bucketCount, reserved, numLanguages (except bucketCount's top bit is addBucketsAutomatically)
//   array of...
//     byte : variable type
//     byte : specialness bitfield, where 1 = callbacked, 2 = ignorable
//     ministring : variable name
//     if ignored then nothing
//     if numeric then longint : variable value
//     if string then array[numLanguages] of...
//       dword : UTF-8 string byte length
//       array[length] of bytes : UTF-8 string content
var i, j, l : dword;
	writep : pointer;

	procedure _WriteVar;
	var special : byte;
	begin
		with bucket[i].content[j] do begin
			byte(writep^) := byte(varType); inc(writep);

			special := 0;
			if useCallbacks then special := 1;
			if ignoreSave then special := special or 2;
			byte(writep^) := special; inc(writep);

			move(name[0], writep^, name.Length + 1);
			inc(writep, name.Length + 1);
		end;
	end;

begin
	outbuf := NIL;
	result := 12; // header: 3 dwords
	Assert((bucketCount <> 0) and (numLanguages <> 0));

	// Calculate the memory needed. Fetch current protected variable values while at it.
	for i := bucketCount - 1 downto 0 do with bucket[i] do begin
		if bucketSize <> 0 then for j := bucketSize - 1 downto 0 do with content[j] do begin
			inc(result, 3 + name.Length);
			if ignoreSave then continue;
			case varType of
				VT_INT:
				begin
					if useCallbacks then numValue := callbackGetter(name, NIL);
					inc(result, 4);
				end;

				VT_STR:
				begin
					if strValue.Length <> numLanguages then begin
						strValue := NIL; setlength(strValue, numLanguages);
					end;
					if useCallbacks then callbackGetter(name, strValue);
					for l := numLanguages - 1 downto 0 do
						inc(result, 4 + strValue[l].Length);
				end;

				else raise Exception.Create(strcat('SaveVarState: % has bad type %', [name, varType]));
			end;
		end;
	end;

	// Reserve the memory... caller is responsible for freeing it.
	getmem(outbuf, result);
	writep := outbuf;

	// Write the header.
	i := bucketCount;
	if addBucketsAutomatically then i := i or $80000000;
	dword(writep^) := NtoLE(i); inc(writep, 4);
	//dword(writep^) := NtoLE(numstrings);
	inc(writep, 4);
	dword(writep^) := NtoLE(numLanguages); inc(writep, 4);

	// Iterate through buckets, saving all variables. Numeric first.
	for i := bucketCount - 1 downto 0 do with bucket[i] do begin
		if bucketSize <> 0 then for j := bucketSize - 1 downto 0 do with content[j] do begin
			if varType <> VT_INT then continue;
			_WriteVar;
			if ignoreSave then continue;
			longint(writep^) := NtoLE(numValue); // value
			inc(writep, 4);
		end;
	end;

	// Now save string variables. Keeping them grouped like this may improve compressibility.
	for i := bucketCount - 1 downto 0 do with bucket[i] do begin
		if bucketSize <> 0 then for j := bucketSize - 1 downto 0 do with content[j] do begin
			if varType <> VT_STR then continue;
			_WriteVar;
			if ignoreSave then continue;

			for l := 0 to numLanguages - 1 do begin
				dword(writep^) := NtoLE(strValue[l].Length);
				inc(writep, 4);
				if strValue[l].Length = 0 then continue;
				move(strValue[l][1], writep^, strValue[l].Length);
				inc(writep, strValue[l].Length);
			end;
		end;
	end;
end;

function TVarmon.GetVarType(const varname : ShortString) : EVarType;
// Returns the variable type, VT_*.
var i : dword;
begin
	result := VT_NULL;
	i := FindVar(upcase(varname));
	if i >= $80000000 then exit; // variable doesn't exist
	with bucket[i] do result := content[topLink].varType;
end;

procedure TVarmon.SetNumVar(const varname : ShortString; num : longint);
// Assigns the given number to the given number variable. The variable is created if it doesn't exist yet; it is
// overwritten if it does exist. If the variable is callbacked, forwards to callbackSetter.
var i : dword;
begin
	i := AddVar(upcase(varname), VT_INT);
	with bucket[i] do with content[topLink] do begin
		numValue := num;
		if useCallbacks then begin
			Assert(callbackSetter <> NIL, 'specify callbackSetter first');
			callbackSetter(varname, NIL, num);
		end;
	end;
end;

function TVarmon.GetNumVar(const varname : ShortString) : longint;
// Returns the value of the given variable, or 0 if that doesn't exist.
// If the requested variable is a string, also returns 0.
// If the variable has ever had callbacks enabled, forwards to callbackGetter.
var i : dword;
begin
	result := 0;
	i := FindVar(upcase(varname));
	if i >= $80000000 then exit; // variable doesn't exist
	with bucket[i] do with content[topLink] do begin
		if varType <> VT_INT then exit;
		if NOT useCallbacks then
			result := numValue
		else
			result := callbackGetter(varname, NIL);
	end;
end;

procedure TVarmon.SetStrVar(const varname : ShortString; const txt : TStringBunch);
// Assigns the contents of txt to the given string variable. The variable is created if it doesn't exist;
// it is overwritten if it does exist. If the variable is callbacked, forwards to callbackSetter.
var i, l : dword;
begin
	i := AddVar(upcase(varname), VT_STR);
	with bucket[i] do with content[topLink] do begin
		if strValue.Length <> numLanguages then begin strValue := NIL; setlength(strValue, numLanguages); end;
		if NOT useCallbacks then begin
			l := txt.Length;
			if l > numLanguages then l := numLanguages;
			while l <> 0 do begin
				dec(l);
				strValue[l] := txt[l];
			end;
		end
		else begin
			Assert(callbackSetter <> NIL, 'specify callbackSetter first');
			callbackSetter(varname, txt, 0);
		end;
	end;
end;

procedure TVarmon.SetStrVar(const varname : ShortString; const txt : UTF8string);
// Assigns txt to all languages of the given string variable. The variable is created if it doesn't exist;
// it is overwritten if it does exist. If the variable is callbacked, forwards to callbackSetter.
var i : dword;
begin
	i := AddVar(upcase(varname), VT_STR);
	with bucket[i] do with content[topLink] do begin
		if strValue.Length <> numLanguages then begin strValue := NIL; setlength(strValue, numLanguages); end;
		strValue.SetAll(txt);
		if useCallbacks then begin
			Assert(callbackSetter <> NIL, 'specify callbackSetter first');
			callbackSetter(varname, strValue, 0);
		end;
	end;
end;

function TVarmon.GetStrVar(const varname : ShortString) : TStringBunch;
// Returns a string variable value.
// If the given variable doesn't exist, returns a bunch of empty strings.
// If the variable has callbacks enabled, forwards to callbackGetter.
// If the requested variable is a number, converts it to a string.
var i, j : dword;
begin
	result := NIL;
	setlength(result, numLanguages);

	i := FindVar(upcase(varname));
	// Variable doesn't exist.
	if i >= $80000000 then exit;

	with bucket[i] do with content[topLink] do begin
		if varType = VT_INT then begin
			// Numeric variable, convert to string.
			if NOT useCallbacks then
				result.SetAll(strdec(numValue))
			else
				result.SetAll(strdec(callbackGetter(varname, NIL)));
			exit;
		end;

		if NOT useCallbacks then begin
			if strValue.Length <> numLanguages then begin strValue := NIL; setlength(strValue, numLanguages); end;
			for j := numLanguages - 1 downto 0 do
				result[j] := strValue[j];
		end
		else callbackGetter(varname, result);
	end;
end;

function TVarmon.GetStrVar(const varname : ShortString; language : byte) : UTF8string;
// Fetches and returns the requested language value of the string variable. The string stash is not affected.
// Use this if you only need one language specifically.
// If the variable has ever had callbacks enabled, forwards to callbackGetter.
// If the given variable doesn't exist, returns an empty string. If the language is out of bounds, substitutes with
// language 0. If the requested variable is a number, converts it to a string.
var i : dword;
begin
	i := FindVar(upcase(varname));
	if i >= $80000000 then begin
		// Variable doesn't exist.
		result := '';
		exit;
	end;

	with bucket[i] do with content[topLink] do begin
		if varType = VT_INT then begin
			// Numeric variable, convert to string.
			if NOT useCallbacks then
				result := strdec(numValue)
			else
				result := strdec(callbackGetter(varname, NIL));
			exit;
		end;

		if language >= numLanguages then language := 0;
		if NOT useCallbacks then
			result := strValue[language]
		else begin
			if strValue.Length <> numLanguages then begin strValue := NIL; setlength(strValue, numLanguages); end;
			callbackGetter(varname, strValue);
			result := strValue[language];
		end;
	end;
end;

procedure TVarmon.IgnoreVar(const varname : ShortString; ignore : boolean);
// Sets the ignore saved value flag for an existing variable. If variable doesn't exist, throws an exception.
var i : dword;
begin
	i := FindVar(upcase(varname));
	if i >= $80000000 then raise Exception.Create('Variable ' + varname + ' not created');
	with bucket[i] do content[topLink].ignoreSave := ignore;
end;

procedure TVarmon.DeleteVar(const varname : ShortString);
// Deletes the given variable, if it exists.
// If the variable is using callbacks, sets it to empty instead of deleting.
var i, l : dword;
begin
	i := FindVar(upcase(varname));
	if i >= $80000000 then exit; // already doesn't exist

	with bucket[i] do begin
		with content[topLink] do begin
			if useCallbacks then begin
				if varType = VT_STR then strValue.SetAll('');
				Assert(callbackSetter <> NIL, 'specify callbackSetter first');
				callbackSetter(varname, strValue, 0);
				exit;
			end;

			strValue := NIL;
			// Detach neighbors from the outgoing top link.
			content[nextLink].prevLink := prevLink;
			content[prevLink].nextLink := nextLink;
			l := topLink;
			topLink := nextLink;
		end;

		// There can't be any empty space in the bucket's content array. If we're deleting the last index item, it can
		// just be detached, but deleting anything else requires moving the last index item into the vacated slot.
		dec(bucketSize);
		if l <> bucketSize then begin
			content[l] := content[bucketSize];
			content[bucketSize].strValue := NIL;
			with content[l] do begin
				content[prevLink].nextLink := l;
				content[nextLink].prevLink := l;
			end;
			if topLink = bucketSize then topLink := l;
		end;
	end;
end;

procedure TVarmon.SetCallback(const varname : ShortString; usecallback : boolean; _vartype : EVarType);
// Sets whether a variable should forward to a custom callback function. The variable is created if it doesn't exist.
var i : dword;
begin
	i := AddVar(upcase(varname), _vartype);
	with bucket[i] do content[topLink].useCallbacks := usecallback;
end;

procedure TVarmon.SetNumBuckets(const newbuckets : dword);
// Saves the current variables, re-inits with the new bucket count, then loads the variables into the new buckets.
// Scripts can set the number of buckets to accommodate their expected variable use. If a script uses a lot of
// variables, a larger number of buckets will be faster. If a script uses few variables, a small number of buckets
// keeps the memory overhead low.
// By default 1 bucket is used; a reasonable number is 1 bucket per 2 actively used variables; seldom-used variables
// sink to the bottom and don't slow down common lookups.
var poku : pointer;
	i : dword;
begin
	if newbuckets = bucketCount then exit;
	if (newbuckets = 0) or (newbuckets > VARMON_MAXBUCKETS) then
		raise Exception.Create('Bad new bucketCount, must be 1..' + strdec(VARMON_MAXBUCKETS));
	i := SaveVarState(poku);
	dword(poku^) := newbuckets; // poke bucketCount in saved structure
	LoadVarState(poku, i);
	freemem(poku); poku := NIL;
	// Manually setting the number of buckets disables automatic bucket adding.
	addBucketsAutomatically := FALSE;
end;

function TVarmon.CountBuckets : dword;
begin
	result := bucketCount;
end;

procedure TVarmon.Init(numlang, numbuckets : dword);
// Initialises everything, needs the number of languages being used and the preferred number of variable buckets.
// The number of languages is known at game mod load and should not change. The number of buckets may be changed at
// runtime using SetNumBuckets.
var i : dword;
begin
	if numlang = 0 then numlang := 1;
	if numlang > VARMON_MAXLANGUAGES then
		raise Exception.Create(strcat('Tried to init with % languages, max %', [numlang, VARMON_MAXLANGUAGES]));
	if numbuckets = 0 then numbuckets := 1;
	if numbuckets > VARMON_MAXBUCKETS then numbuckets := VARMON_MAXBUCKETS;

	bucketCount := numbuckets;
	numLanguages := numlang;
	setlength(bucket, 0);
	setlength(bucket, numbuckets);
	for i := numbuckets - 1 downto 0 do with bucket[i] do begin
		bucketSize := 0;
		setlength(content, 0);
	end;

	addBucketsAutomatically := TRUE;
end;

constructor TVarmon.Create(numlang, numbuckets : dword);
begin
	Init(numlang, numbuckets);
	callbackGetter := NIL;
	callbackSetter := NIL;
end;

{$ifdef debugdump}
destructor TVarmon.Destroy;
begin
	DumpVarTable;
	inherited;
end;
{$endif}

// ------------------------------------------------------------------
initialization
finalization
end.

