unit minicrt;
{                                                                           }
{ MiniCrt, a small console-interaction unit that works like the Crt unit.   }
{ Compiles with FPC 3.2.2 on Windows/Linux.                                 }
{ CC0, 2024 :: Kirinn Bunnylin / MoonCore :: Use freely for anything ever!  }
{                                                                           }
{ Main differences:                                                         }
{ - Smaller code, more comments; more maintainable, less robust             }
{ - Console size querying and size change callback                          }
{ - No mouse or sound functions, no subwindows, no line editing functions   }
{ - Expects UTF-8 input and output everywhere                               }
{ - ReadKey returns a UTF-8 string                                          }
{                                                                           }
{ For a more robust solution, consider FPC's rtl-console/keyboard unit.     }
{                                                                           }

{$mode objfpc}
{$codepage UTF8}
{$inline on}

// You can use normal Write/WriteLn to output UTF-8 text. You may want to include "$codepage UTF8" in your program's
// defines.
// NOTE: the Write function in FPC 3.0.2 prints direct string literals and UTF-8 text from codepageless shortstrings
// correctly under Windows, but fails to print UTF8strings correctly. Use this unit's UTF8Write for such cases.
// NOTE: the plain linux tty does not yet display CJK without tweaking. (Terminal emulators, however, work fine.)
//
// ReadKey returns a shortstring that contains a UTF8 code of 1-4 bytes.
// If any modifier keys apply, the UTF8 code will be prefixed with a null byte and a modifier byte, a combination of:
//   4 = CTRL
//   2 = ALT
//   1 = SHIFT
// NOTE: The Ctrl+Alt+key combination is the same as AltGr+key, which is used in some keyboard layouts to produce
// common special characters. For simplicity, ReadKey does not see the Ctrl+Alt combination.
//
// Extended keys are saved as UTF-8 codes in the Unicode private use area:
//   EE 90 8C (E40C) = numpad center
//   EE 90 A1 (E421) = page up
//   EE 90 A2 (E422) = page down
//   EE 90 A3 (E423) = end
//   EE 90 A4 (E424) = home
//   EE 90 A5 (E425) = cursor left
//   EE 90 A6 (E426) = cursor up
//   EE 90 A7 (E427) = cursor right
//   EE 90 A8 (E428) = cursor down
//   EE 90 AD (E42D) = insert
//   EE 90 AE (E42E) = delete
//   EE 90 AF (E42F) = context menu
//   EE 91 B0 .. EE 91 BB (E470..E47B) = F1..F12

// Not all key combinations are equally available cross-platform. Don't even try to use the following:
//   Ctrl/Alt/Shift + Esc/Tab/Fn/Insert/Delete
//   PrintScr-SysRq key
//   Pause-Break key
//   Num lock, caps lock, scroll lock
//   Windows/Super/Menu key
//   Ctrl + 0..9 are partially unsupported on all terminals
//   Shift-F9..Shift-F12 are ignored in the plain linux tty
//   Shift-F1..Shift-F12 produce wrong, conflicting codes in urxvt
//   Ctrl+Alt+Fn and Alt+Fn tend to be bound to various OS functions
//   Ctrl-F1..Ctrl-F12 are ignored in linux tty
//   Ctrl + ,.- are not ok by lxterminal, qterminal, urxvt, linux tty
//   Numpad 5 is ignored by qterminal, urxvt; doesn't mix well with Ctrl/Alt
//   Page Down has a conflicting code on FreeBSD with numpad 5 in linux tty?
//   Shift-insert is used for pasting, so it won't produce a keypress
//   Ctrl/Shift + delete is incorrect in linux tty
//   Ctrl-insert is incorrect in linux tty and ignored by lxterminal
//   Ctrl/Alt/Shift + cursor keys are incorrect or OS functions in linux tty
//   Shift + PageUp/PageDown are used to scroll terminal buffers
//   Shift + Home/End are incorrect in linux tty, and scroll lx/qterminal
//   Ctrl/Alt + Ins/Del/Home/End/PgUp/PgDn are incorrect in linux tty
//   Ctrl+Alt + A..Z only work on Linux (except C, S, V in urxvt)
//   Ctrl+Shift + A..Z only work fully on Windows, ignores shift on linux

// What does appear to work universally?
//   Cursor keys and numpad directions
//   Ins/Del/Home/End/PgUp/PgDn
//   All normally typeable UTF-8 characters
//   space, enter, tab, shift-tab, backspace
//   ESC, F1..F12
//   Ctrl + A..Z except H..J, M
//   Alt + A..Z (unless a terminal window's own menus intercept it; on xterm, altSendsEscape must be true)
//   Alt + 0..9

// ------------------------------------------------------------------

interface

{$ifdef WINDOWS}

uses windows;

const crtPalette : array[0..15] of packed record
	r, g, b : byte;
end = (
(r: 0; g: 0; b: 0),
(r: 0; g: 0; b: 128),
(r: 0; g: 128; b: 0),
(r: 0; g: 128; b: 128),
(r: 128; g: 0; b: 0),
(r: 128; g: 0; b: 128),
(r: 128; g: 128; b: 0),
(r: 192; g: 192; b: 192),
(r: 128; g: 128; b: 128),
(r: 0; g: 0; b: 255),
(r: 0; g: 255; b: 0),
(r: 0; g: 255; b: 255),
(r: 255; g: 0; b: 0),
(r: 255; g: 0; b: 255),
(r: 255; g: 255; b: 0),
(r: 255; g: 255; b: 255));

// Windows-specific wrapper for WriteConsoleOutputW, the only way to do fast console output.
procedure CrtWriteConOut(const srcp : pointer; const sx, sy, x1, y1, x2, y2 : dword); inline;

{$else}

uses unix, baseunix, termio;

// Unix terminal colors have the red and blue components switched compared to legacy CGA. A translation table seems the
// fastest solution.
const termTextColor : array[0..15] of string[3] =
('30','34','32','36','31','35','33','37','90','94','92','96','91','95','93','97');
const termBkgColor : array[0..15] of string[3] =
('40','44','42','46','41','45','43','47','100','104','102','106','101','105','103','107');

// The XTerm palette is a good linux default. Call GetConsolePalette to ask the terminal for the precise values, but
// some terminals don't support that command (qterminal), or respond with incorrect RGB values (lxterminal).
const crtPalette : array[0..15] of packed record
	r, g, b : byte;
end
= (
(r: 0; g: 0; b: 0),
(r: 0; g: 0; b: $CD),
(r: 0; g: $CD; b: 0),
(r: 0; g: $CD; b: $CD),
(r: $EE; g: 0; b: 0),
(r: $CD; g: 0; b: $CD),
(r: $CD; g: $CD; b: 0),
(r: $E5; g: $E5; b: $E5),
(r: $7F; g: $7F; b: $7F),
(r: 0; g: 0; b: $FF),
(r: 0; g: $FF; b: 0),
(r: 0; g: $FF; b: $FF),
(r: $FF; g: $5C; b: $5C),
(r: $FF; g: 0; b: $FF),
(r: $FF; g: $FF; b: 0),
(r: $FF; g: $FF; b: $FF));

{$endif}

var crt_consoleSizeChanged : boolean = FALSE;
type string31 = string[31];

procedure Delay(msec : dword); inline;
function GetMsecTime : ptruint; inline;
procedure GotoXY(x, y : dword); inline;
procedure ClrScr;
procedure SetColor(color : word); inline;
function KeyPressed : boolean;
function ReadKey : string31;
procedure UTF8Write(const outstr : UTF8string); inline;
procedure CrtSetTitle(const newtitle : UTF8string); inline;
procedure CrtShowCursor(visible : boolean); inline;
procedure GetConsoleSize(out sizex, sizey : dword);
procedure GetConsolePalette;
procedure MiniCRT_RunTest;

// ------------------------------------------------------------------

implementation

// Keypresses through stdin are fed into this ring buffer. They can be fetched one at a time by calling ReadKey.
var inputBuf : array[0..31] of string31; // must be a ^2 - 1
	inputBufReadIndex, inputBufWriteIndex : longint;

{$ifdef WINDOWS}
var StdInH, StdOutH : HANDLE;
	oldCodePageOut : dword;
	conCursorInfo : CONSOLE_CURSOR_INFO;
{$else}

var stdInDescriptor : TFDSet;
	termSettings : Termios;
	oldSigAction : sigactionrec;
	fileControlFlags : ptrint;
	termSizeX, termSizeY : dword; // internal only, use GetConsoleSize
	crtPalResponse : dword;

// Keyboard input from unix terminals is fubar. The below table and procedures are used to get whatever input possible.
type TKeySeqType = record
	i : string[9];
	o : string[5];
end;

// This table must be sorted by i-sequence, so a binary search is possible.
// A possibly more comprehensive list of sequences is available as part of FPC's rtl-console/keyboard.pp.
const keySeqList : array[0..257] of TKeySeqType = (
(i:#27'OP'; o:#0#2 + chr($EE)+chr($91)+chr($B0)), // Alt-F1 (xterm)
(i:#27'OQ'; o:#0#2 + chr($EE)+chr($91)+chr($B1)), // Alt-F2 (xterm)
(i:#27'OR'; o:#0#2 + chr($EE)+chr($91)+chr($B2)), // Alt-F3 (xterm)
(i:#27'OS'; o:#0#2 + chr($EE)+chr($91)+chr($B3)), // Alt-F4 (xterm)
(i:#27'Oa'; o:#0#2 + chr($EE)+chr($90)+chr($A6)), // Alt-up (?)
(i:#27'Ob'; o:#0#2 + chr($EE)+chr($90)+chr($A8)), // Alt-down (?)
(i:#27'Oc'; o:#0#2 + chr($EE)+chr($90)+chr($A7)), // Alt-right (?)
(i:#27'Od'; o:#0#2 + chr($EE)+chr($90)+chr($A5)), // Alt-left (?)
(i:#27'Ol'; o:#0#2 + chr($EE)+chr($91)+chr($B7)), // Alt-F8 (xterm)
(i:#27'Ot'; o:#0#2 + chr($EE)+chr($91)+chr($B4)), // Alt-F5 (xterm)
(i:#27'Ou'; o:#0#2 + chr($EE)+chr($91)+chr($B5)), // Alt-F6 (xterm)
(i:#27'Ov'; o:#0#2 + chr($EE)+chr($91)+chr($B6)), // Alt-F7 (xterm)
(i:#27'Ow'; o:#0#2 + chr($EE)+chr($91)+chr($B8)), // Alt-F9 (xterm)
(i:#27'Ox'; o:#0#2 + chr($EE)+chr($91)+chr($B9)), // Alt-F10 (xterm)
(i:#27'Oy'; o:#0#2 + chr($EE)+chr($91)+chr($BA)), // Alt-F11 (xterm)
(i:#27'Oz'; o:#0#2 + chr($EE)+chr($91)+chr($BB)), // Alt-F12 (xterm)
(i:#27'[11~'; o:#0#2 + chr($EE)+chr($91)+chr($B0)), // Alt-F1 (rxvt)
(i:#27'[12~'; o:#0#2 + chr($EE)+chr($91)+chr($B1)), // Alt-F2 (rxvt)
(i:#27'[13~'; o:#0#2 + chr($EE)+chr($91)+chr($B2)), // Alt-F3 (rxvt)
(i:#27'[14~'; o:#0#2 + chr($EE)+chr($91)+chr($B3)), // Alt-F4 (rxvt)
(i:#27'[15~'; o:#0#2 + chr($EE)+chr($91)+chr($B4)), // Alt-F5 (rxvt)
(i:#27'[17~'; o:#0#2 + chr($EE)+chr($91)+chr($B5)), // Alt-F6 (rxvt)
(i:#27'[18~'; o:#0#2 + chr($EE)+chr($91)+chr($B6)), // Alt-F7 (rxvt)
(i:#27'[19~'; o:#0#2 + chr($EE)+chr($91)+chr($B7)), // Alt-F8 (rxvt)
(i:#27'[20~'; o:#0#2 + chr($EE)+chr($91)+chr($B8)), // Alt-F9 (rxvt)
(i:#27'[21~'; o:#0#2 + chr($EE)+chr($91)+chr($B9)), // Alt-F10 (rxvt)
(i:#27'[23~'; o:#0#2 + chr($EE)+chr($91)+chr($BA)), // Alt-F11 (rxvt)
(i:#27'[24~'; o:#0#2 + chr($EE)+chr($91)+chr($BB)), // Alt-F12 (rxvt)
(i:#27'[2~'; o:#0#2 + chr($EE)+chr($90)+chr($AD)), // Alt-insert (rxvt)
(i:#27'[3~'; o:#0#2 + chr($EE)+chr($90)+chr($AE)), // Alt-delete (rxvt)
(i:#27'[5~'; o:#0#2 + chr($EE)+chr($90)+chr($A1)), // Alt-pageup (rxvt)
(i:#27'[6~'; o:#0#2 + chr($EE)+chr($90)+chr($A2)), // Alt-pagedown (rxvt)
(i:#27'[7~'; o:#0#2 + chr($EE)+chr($90)+chr($A4)), // Alt-home (rxvt)
(i:#27'[8~'; o:#0#2 + chr($EE)+chr($90)+chr($A3)), // Alt-end (rxvt)
(i:#27'[A'; o:#0#2 + chr($EE)+chr($90)+chr($A6)), // Alt-up (rxvt)
(i:#27'[B'; o:#0#2 + chr($EE)+chr($90)+chr($A8)), // Alt-down (rxvt)
(i:#27'[C'; o:#0#2 + chr($EE)+chr($90)+chr($A7)), // Alt-right (rxvt)
(i:#27'[D'; o:#0#2 + chr($EE)+chr($90)+chr($A5)), // Alt-left (rxvt)
(i:#27'[[A'; o:#0#2 + chr($EE)+chr($91)+chr($B0)), // Alt-F1 (?)
(i:#27'[[B'; o:#0#2 + chr($EE)+chr($91)+chr($B1)), // Alt-F2 (?)
(i:#27'[[C'; o:#0#2 + chr($EE)+chr($91)+chr($B2)), // Alt-F3 (?)
(i:#27'[[D'; o:#0#2 + chr($EE)+chr($91)+chr($B3)), // Alt-F4 (?)
(i:#27'[[E'; o:#0#2 + chr($EE)+chr($91)+chr($B3)), // Alt-F4 (?)
(i:'O2M'; o:#0#1 + chr($D)), // Shift-numpad-enter (xterm)
(i:'O2P'; o:#0#1 + chr($EE)+chr($91)+chr($B0)), // Shift-F1 (konsole/xterm/qterm)
(i:'O2Q'; o:#0#1 + chr($EE)+chr($91)+chr($B1)), // Shift-F2 (konsole/xterm/qterm)
(i:'O2R'; o:#0#1 + chr($EE)+chr($91)+chr($B2)), // Shift-F3 (konsole/xterm/qterm)
(i:'O2S'; o:#0#1 + chr($EE)+chr($91)+chr($B3)), // Shift-F4 (konsole/xterm/qterm)
(i:'O2j'; o:#0#1 + chr($2A)), // Shift-numpad-mul (xterm)
(i:'O2l'; o:#0#1 + chr($EE)+chr($90)+chr($AE)), // Shift-numpad-delete (xterm)
(i:'O2o'; o:#0#1 + chr($2F)), // Shift-numpad-div (xterm)
(i:'O2p'; o:#0#1 + chr($EE)+chr($90)+chr($AD)), // Shift-numpad-insert (xterm)
(i:'O2q'; o:#0#1 + chr($EE)+chr($90)+chr($A3)), // Shift-numpad1 (xterm)
(i:'O2r'; o:#0#1 + chr($EE)+chr($90)+chr($A8)), // Shift-numpad2 (xterm)
(i:'O2s'; o:#0#1 + chr($EE)+chr($90)+chr($A2)), // Shift-numpad3 (xterm)
(i:'O2t'; o:#0#1 + chr($EE)+chr($90)+chr($A5)), // Shift-numpad4 (xterm)
(i:'O2u'; o:#0#1 + chr($EE)+chr($90)+chr($8C)), // Shift-numpad5 (xterm)
(i:'O2v'; o:#0#1 + chr($EE)+chr($90)+chr($A7)), // Shift-numpad6 (xterm)
(i:'O2w'; o:#0#1 + chr($EE)+chr($90)+chr($A4)), // Shift-numpad7 (xterm)
(i:'O2x'; o:#0#1 + chr($EE)+chr($90)+chr($A6)), // Shift-numpad8 (xterm)
(i:'O2y'; o:#0#1 + chr($EE)+chr($90)+chr($A1)), // Shift-numpad9 (xterm)
(i:'O3M'; o:#0#2 + chr($D)), // Alt-numpad-enter (xterm)
(i:'O3P'; o:#0#2 + chr($EE)+chr($91)+chr($B0)), // Alt-F1 (xterm)
(i:'O3Q'; o:#0#2 + chr($EE)+chr($91)+chr($B1)), // Alt-F2 (xterm)
(i:'O3R'; o:#0#2 + chr($EE)+chr($91)+chr($B2)), // Alt-F3 (xterm)
(i:'O3S'; o:#0#2 + chr($EE)+chr($91)+chr($B3)), // Alt-F4 (xterm)
(i:'O3j'; o:#0#2 + chr($2A)), // Alt-numpad-mul (xterm)
(i:'O3k'; o:#0#2 + chr($2B)), // Alt-numpad-plus (xterm)
(i:'O3m'; o:#0#2 + chr($2D)), // Alt-numpad-minus (xterm)
(i:'O3o'; o:#0#2 + chr($2F)), // Alt-numpad-div (xterm)
(i:'O5M'; o:#0#4 + chr($A)), // Ctrl-numpad-enter (xterm)
(i:'O5P'; o:#0#4 + chr($EE)+chr($91)+chr($B0)), // Ctrl-F1 (konsole/xterm)
(i:'O5Q'; o:#0#4 + chr($EE)+chr($91)+chr($B1)), // Ctrl-F2 (konsole/xterm)
(i:'O5R'; o:#0#4 + chr($EE)+chr($91)+chr($B2)), // Ctrl-F3 (konsole/xterm)
(i:'O5S'; o:#0#4 + chr($EE)+chr($91)+chr($B3)), // Ctrl-F4 (konsole/xterm)
(i:'O5j'; o:#0#4 + chr($2A)), // Ctrl-numpad-mul (xterm)
(i:'O5k'; o:#0#4 + chr($2B)), // Ctrl-numpad-plus (xterm)
(i:'O5m'; o:#0#4 + chr($2D)), // Ctrl-numpad-minus (xterm)
(i:'O5o'; o:#0#4 + chr($2F)), // Ctrl-numpad-div (xterm)
(i:'OA'; o:chr($EE)+chr($90)+chr($A6)), // up (xterm)
(i:'OB'; o:chr($EE)+chr($90)+chr($A8)), // down (xterm)
(i:'OC'; o:chr($EE)+chr($90)+chr($A7)), // right (xterm)
(i:'OD'; o:chr($EE)+chr($90)+chr($A5)), // left (xterm)
(i:'OE'; o:chr($EE)+chr($90)+chr($8C)), // numpad5 (xterm)
(i:'OF'; o:chr($EE)+chr($90)+chr($A3)), // end (xterm)
(i:'OH'; o:chr($EE)+chr($90)+chr($A4)), // home (xterm)
(i:'OM'; o:chr($D)), // numpad-enter (xterm)
(i:'OP'; o:chr($EE)+chr($91)+chr($B0)), // F1 (vt100/gnome/konsole/lx/qterm)
(i:'OQ'; o:chr($EE)+chr($91)+chr($B1)), // F2 (vt100/gnome/konsole/lx/qterm)
(i:'OR'; o:chr($EE)+chr($91)+chr($B2)), // F3 (vt100/gnome/konsole/lx/qterm)
(i:'OS'; o:chr($EE)+chr($91)+chr($B3)), // F4 (vt100/gnome/konsole/lx/qterm)
(i:'Oa'; o:#0#4 + chr($EE)+chr($90)+chr($A6)), // Ctrl-up (rxvt)
(i:'Ob'; o:#0#4 + chr($EE)+chr($90)+chr($A8)), // Ctrl-down (rxvt)
(i:'Oc'; o:#0#4 + chr($EE)+chr($90)+chr($A7)), // Ctrl-right (rxvt)
(i:'Od'; o:#0#4 + chr($EE)+chr($90)+chr($A5)), // Ctrl-left (rxvt)
(i:'Oj'; o:chr($2A)), // numpad-mul (xterm)
(i:'Ok'; o:chr($2B)), // numpad-plus (xterm)
(i:'Ol'; o:chr($EE)+chr($91)+chr($B7)), // F8 (vt100)
(i:'Om'; o:chr($2D)), // numpad-minus (xterm)
(i:'Oo'; o:chr($2F)), // numpad-div (xterm)
(i:'Op'; o:#0#1 + chr($EE)+chr($90)+chr($AD)), // shift-numpad-insert (rxvt)
(i:'Ot'; o:chr($EE)+chr($91)+chr($B4)), // F5 (vt100)
(i:'Ou'; o:chr($EE)+chr($91)+chr($B5)), // F6 (vt100)
(i:'Ov'; o:chr($EE)+chr($91)+chr($B6)), // F7 (vt100)
(i:'Ow'; o:chr($EE)+chr($91)+chr($B8)), // F9 (vt100)
(i:'Ox'; o:chr($EE)+chr($91)+chr($B9)), // F10 (vt100)
(i:'Oy'; o:chr($EE)+chr($91)+chr($BA)), // F11 (vt100)
(i:'Oz'; o:chr($EE)+chr($91)+chr($BB)), // F12 (vt100)
(i:'[11;2~'; o:#0#1 + chr($EE)+chr($91)+chr($B0)), // Shift-F1 (konsole as vt420pc)
(i:'[11;5~'; o:#0#4 + chr($EE)+chr($91)+chr($B0)), // Ctrl-F1
(i:'[11^'; o:#0#4 + chr($EE)+chr($91)+chr($B0)), // Ctrl-F1 (rxvt)
(i:'[11~'; o:chr($EE)+chr($91)+chr($B0)), // F1 (Eterm/rxvt)
(i:'[12;2~'; o:#0#1 + chr($EE)+chr($91)+chr($B1)), // Shift-F2 (konsole as vt420pc)
(i:'[12;5~'; o:#0#4 + chr($EE)+chr($91)+chr($B1)), // Ctrl-F2
(i:'[12^'; o:#0#4 + chr($EE)+chr($91)+chr($B1)), // Ctrl-F2 (rxvt)
(i:'[12~'; o:chr($EE)+chr($91)+chr($B1)), // F2 (Eterm/rxvt)
(i:'[13;2~'; o:#0#1 + chr($EE)+chr($91)+chr($B2)), // Shift-F3 (konsole as vt420pc)
(i:'[13;5~'; o:#0#4 + chr($EE)+chr($91)+chr($B2)), // Ctrl-F3
(i:'[13^'; o:#0#4 + chr($EE)+chr($91)+chr($B2)), // Ctrl-F3 (rxvt)
(i:'[13~'; o:chr($EE)+chr($91)+chr($B2)), // F3 (Eterm/rxvt)
(i:'[14;2~'; o:#0#1 + chr($EE)+chr($91)+chr($B3)), // Shift-F4 (konsole as vt420pc)
(i:'[14;5~'; o:#0#4 + chr($EE)+chr($91)+chr($B3)), // Ctrl-F4
(i:'[14^'; o:#0#4 + chr($EE)+chr($91)+chr($B3)), // Ctrl-F4 (rxvt)
(i:'[14~'; o:chr($EE)+chr($91)+chr($B3)), // F4 (Eterm/rxvt)
(i:'[15;2~'; o:#0#1 + chr($EE)+chr($91)+chr($B4)), // Shift-F5 (xterm/lx/qterm)
(i:'[15;3~'; o:#0#2 + chr($EE)+chr($91)+chr($B4)), // Alt-F5 (xterm/lx/qterm)
(i:'[15;5~'; o:#0#4 + chr($EE)+chr($91)+chr($B4)), // Ctrl-F5 (xterm/lx/qterm)
(i:'[15^'; o:#0#4 + chr($EE)+chr($91)+chr($B4)), // Ctrl-F5 (rxvt)
(i:'[15~'; o:chr($EE)+chr($91)+chr($B4)), // F5 (xterm/Eterm/gnome/rxvt/lx/qterm)
(i:'[17;2~'; o:#0#1 + chr($EE)+chr($91)+chr($B5)), // Shift-F6 (xterm/lx/qterm)
(i:'[17;3~'; o:#0#2 + chr($EE)+chr($91)+chr($B5)), // Alt-F6 (xterm/lx/qterm)
(i:'[17;5~'; o:#0#4 + chr($EE)+chr($91)+chr($B5)), // Ctrl-F6 (xterm/lx/qterm)
(i:'[17^'; o:#0#4 + chr($EE)+chr($91)+chr($B5)), // Ctrl-F6 (rxvt)
(i:'[17~'; o:chr($EE)+chr($91)+chr($B5)), // F6 (linux/xterm/Eterm/konsole/gnome/rxvt/lx/qterm)
(i:'[18;2~'; o:#0#1 + chr($EE)+chr($91)+chr($B6)), // Shift-F7 (xterm/lx/qterm)
(i:'[18;3~'; o:#0#2 + chr($EE)+chr($91)+chr($B6)), // Alt-F7 (xterm/lx/qterm)
(i:'[18;5~'; o:#0#4 + chr($EE)+chr($91)+chr($B6)), // Ctrl-F7 (xterm/lx/qterm)
(i:'[18^'; o:#0#4 + chr($EE)+chr($91)+chr($B6)), // Ctrl-F7 (rxvt)
(i:'[18~'; o:chr($EE)+chr($91)+chr($B6)), // F7 (linux/xterm/Eterm/konsole/gnome/rxvt/lx/qterm)
(i:'[19;2~'; o:#0#1 + chr($EE)+chr($91)+chr($B7)), // Shift-F8 (xterm/lx/qterm)
(i:'[19;3~'; o:#0#2 + chr($EE)+chr($91)+chr($B7)), // Alt-F8 (xterm/lx/qterm)
(i:'[19;5~'; o:#0#4 + chr($EE)+chr($91)+chr($B7)), // Ctrl-F8 (xterm/lx/qterm)
(i:'[19^'; o:#0#4 + chr($EE)+chr($91)+chr($B7)), // Ctrl-F8 (rxvt)
(i:'[19~'; o:chr($EE)+chr($91)+chr($B7)), // F8 (linux/xterm/Eterm/konsole/gnome/rxvt/lx/qterm)
(i:'[1;2A'; o:#0#1 + chr($EE)+chr($90)+chr($A6)), // Shift-up (xterm/lx)
(i:'[1;2B'; o:#0#1 + chr($EE)+chr($90)+chr($A8)), // Shift-down (xterm/lx)
(i:'[1;2C'; o:#0#1 + chr($EE)+chr($90)+chr($A7)), // Shift-right (xterm/lx)
(i:'[1;2D'; o:#0#1 + chr($EE)+chr($90)+chr($A5)), // Shift-left (xterm/lx)
(i:'[1;2E'; o:#0#1 + chr($EE)+chr($90)+chr($8C)), // Shift-numpad5 when numlock on (xterm/lx)
(i:'[1;2F'; o:#0#1 + chr($EE)+chr($90)+chr($A3)), // Shift-end (xterm)
(i:'[1;2H'; o:#0#1 + chr($EE)+chr($90)+chr($A4)), // Shift-home (xterm)
(i:'[1;2P'; o:#0#1 + chr($EE)+chr($91)+chr($B0)), // Shift-F1 (xterm/gnome3/lx)
(i:'[1;2Q'; o:#0#1 + chr($EE)+chr($91)+chr($B1)), // Shift-F2 (xterm/gnome3/lx)
(i:'[1;2R'; o:#0#1 + chr($EE)+chr($91)+chr($B2)), // Shift-F3 (xterm/gnome3/lx)
(i:'[1;2S'; o:#0#1 + chr($EE)+chr($91)+chr($B3)), // Shift-F4 (xterm/gnome3/lx)
(i:'[1;3A'; o:#0#2 + chr($EE)+chr($90)+chr($A6)), // Alt-up (xterm/lx/qterm)
(i:'[1;3B'; o:#0#2 + chr($EE)+chr($90)+chr($A8)), // Alt-down (xterm/lx/qterm)
(i:'[1;3C'; o:#0#2 + chr($EE)+chr($90)+chr($A7)), // Alt-right (xterm/lx/qterm)
(i:'[1;3D'; o:#0#2 + chr($EE)+chr($90)+chr($A5)), // Alt-left (xterm/lx/qterm)
(i:'[1;3E'; o:#0#2 + chr($EE)+chr($90)+chr($8C)), // Alt-numpad5 (xterm/lx)
(i:'[1;3F'; o:#0#2 + chr($EE)+chr($90)+chr($A3)), // Alt-end (xterm/lx/qterm)
(i:'[1;3H'; o:#0#2 + chr($EE)+chr($90)+chr($A4)), // Alt-home (xterm/lx/qterm)
(i:'[1;3P'; o:#0#2 + chr($EE)+chr($91)+chr($B0)), // Alt-F1 (xterm/gnome3/lx)
(i:'[1;3Q'; o:#0#2 + chr($EE)+chr($91)+chr($B1)), // Alt-F2 (xterm/gnome3/lx)
(i:'[1;3R'; o:#0#2 + chr($EE)+chr($91)+chr($B2)), // Alt-F3 (xterm/gnome3/lx)
(i:'[1;3S'; o:#0#2 + chr($EE)+chr($91)+chr($B3)), // Alt-F4 (xterm/gnome3/lx)
(i:'[1;5A'; o:#0#4 + chr($EE)+chr($90)+chr($A6)), // Ctrl-up (xterm/lx/qterm)
(i:'[1;5B'; o:#0#4 + chr($EE)+chr($90)+chr($A8)), // Ctrl-down (xterm/lx/qterm)
(i:'[1;5C'; o:#0#4 + chr($EE)+chr($90)+chr($A7)), // Ctrl-right (xterm/lx/qterm)
(i:'[1;5D'; o:#0#4 + chr($EE)+chr($90)+chr($A5)), // Ctrl-left (xterm/lx/qterm)
(i:'[1;5E'; o:#0#4 + chr($EE)+chr($90)+chr($8C)), // Ctrl-numpad5 (xterm/lx)
(i:'[1;5F'; o:#0#4 + chr($EE)+chr($90)+chr($A3)), // Ctrl-end (xterm/lx/qterm)
(i:'[1;5H'; o:#0#4 + chr($EE)+chr($90)+chr($A4)), // Ctrl-home (xterm/lx/qterm)
(i:'[1;5P'; o:#0#4 + chr($EE)+chr($91)+chr($B0)), // Ctrl-F1 (xterm/gnome3/lx)
(i:'[1;5Q'; o:#0#4 + chr($EE)+chr($91)+chr($B1)), // Ctrl-F2 (xterm/gnome3/lx)
(i:'[1;5R'; o:#0#4 + chr($EE)+chr($91)+chr($B2)), // Ctrl-F3 (xterm/gnome3/lx)
(i:'[1;5S'; o:#0#4 + chr($EE)+chr($91)+chr($B3)), // Ctrl-F4 (xterm/gnome3/lx)
(i:'[1~'; o:chr($EE)+chr($90)+chr($A4)), // home (linux)
(i:'[20;2~'; o:#0#1 + chr($EE)+chr($91)+chr($B8)), // Shift-F9 (xterm/lx/qterm)
(i:'[20;3~'; o:#0#2 + chr($EE)+chr($91)+chr($B8)), // Alt-F9 (xterm/qterm)
(i:'[20;5~'; o:#0#4 + chr($EE)+chr($91)+chr($B8)), // Ctrl-F9 (xterm/qterm)
(i:'[20^'; o:#0#4 + chr($EE)+chr($91)+chr($B8)), // Ctrl-F9 (rxvt)
(i:'[20~'; o:chr($EE)+chr($91)+chr($B8)), // F9 (linux/xterm/Eterm/konsole/gnome/rxvt/lx/qterm)
(i:'[21;2~'; o:#0#1 + chr($EE)+chr($91)+chr($B9)), // Shift-F10 (xterm/lx)
(i:'[21;3~'; o:#0#2 + chr($EE)+chr($91)+chr($B9)), // Alt-F10 (xterm/qterm)
(i:'[21;5~'; o:#0#4 + chr($EE)+chr($91)+chr($B9)), // Ctrl-F10 (xterm/qterm)
(i:'[21^'; o:#0#4 + chr($EE)+chr($91)+chr($B9)), // Ctrl-F10 (rxvt)
(i:'[21~'; o:chr($EE)+chr($91)+chr($B9)), // F10 (linux/xterm/Eterm/konsole/gnome/rxvt/lx/qterm)
(i:'[23$'; o:#0#1 + chr($EE)+chr($91)+chr($BA)), // Shift-F11 (rxvt)
(i:'[23;2~'; o:#0#1 + chr($EE)+chr($91)+chr($BA)), // Shift-F11 (xterm/lx/qterm)
(i:'[23;3~'; o:#0#2 + chr($EE)+chr($91)+chr($BA)), // Alt-F11 (xterm/qterm)
(i:'[23;5~'; o:#0#4 + chr($EE)+chr($91)+chr($BA)), // Ctrl-F11 (xterm/qterm)
(i:'[23^'; o:#0#4 + chr($EE)+chr($91)+chr($BA)), // Ctrl-F11 (rxvt)
(i:'[23~'; o:chr($EE)+chr($91)+chr($BA)), // F11 (linux/xterm/Eterm/konsole/gnome/rxvt/lx/qterm)
(i:'[24$'; o:#0#1 + chr($EE)+chr($91)+chr($BB)), // Shift-F12 (rxvt)
(i:'[24;2~'; o:#0#1 + chr($EE)+chr($91)+chr($BB)), // Shift-F12 (xterm/lx/qterm)
(i:'[24;3~'; o:#0#2 + chr($EE)+chr($91)+chr($BB)), // Alt-F12 (xterm/qterm)
(i:'[24;5~'; o:#0#4 + chr($EE)+chr($91)+chr($BB)), // Ctrl-F12 (xterm/qterm)
(i:'[24^'; o:#0#4 + chr($EE)+chr($91)+chr($BB)), // Ctrl-F12 (rxvt)
(i:'[24~'; o:chr($EE)+chr($91)+chr($BB)), // F12 (linux/xterm/Eterm/konsole/gnome/rxvt/lx/qterm)
(i:'[25~'; o:#0#1 + chr($EE)+chr($91)+chr($B0)), // Shift-F1 (linux)
(i:'[26~'; o:#0#1 + chr($EE)+chr($91)+chr($B1)), // Shift-F2 (linux)
(i:'[28~'; o:#0#1 + chr($EE)+chr($91)+chr($B2)), // Shift-F3 (linux)
(i:'[29~'; o:chr($EE)+chr($90)+chr($AF)), // Context menu (xterm/urxvt/kitty), conflicts with Shift-F4 (linux)
(i:'[2;3~'; o:#0#2 + chr($EE)+chr($90)+chr($AD)), // Alt-insert (xterm/qterm)
(i:'[2;5~'; o:#0#4 + chr($EE)+chr($90)+chr($AD)), // Ctrl-insert (xterm/qterm)
(i:'[2^'; o:#0#4 + chr($EE)+chr($90)+chr($AD)), // Ctrl-insert (rxvt)
(i:'[2~'; o:chr($EE)+chr($90)+chr($AD)), // insert (linux/xterm/Eterm/rxvt/qterm)
(i:'[3$'; o:#0#1 + chr($EE)+chr($90)+chr($AE)), // Shift-delete (rxvt)
(i:'[31~'; o:#0#1 + chr($EE)+chr($91)+chr($B4)), // Shift-F5 (linux)
(i:'[32~'; o:#0#1 + chr($EE)+chr($91)+chr($B5)), // Shift-F6 (linux)
(i:'[33~'; o:#0#1 + chr($EE)+chr($91)+chr($B6)), // Shift-F7 (linux)
(i:'[34~'; o:#0#1 + chr($EE)+chr($91)+chr($B7)), // Shift-F8 (linux)
(i:'[3;2~'; o:#0#1 + chr($EE)+chr($90)+chr($AE)), // Shift-delete (xterm/konsole/qterm)
(i:'[3;3~'; o:#0#2 + chr($EE)+chr($90)+chr($AE)), // Alt-delete (xterm/qterm)
(i:'[3;5~'; o:#0#4 + chr($EE)+chr($90)+chr($AE)), // Ctrl-delete (xterm/qterm)
(i:'[3^'; o:#0#4 + chr($EE)+chr($90)+chr($AE)), // Ctrl-delete (rxvt)
(i:'[3~'; o:chr($EE)+chr($90)+chr($AE)), // delete (linux/xterm/Eterm/rxvt/qterm)
(i:'[4~'; o:chr($EE)+chr($90)+chr($A3)), // end (linux/Eterm)
(i:'[5;2~'; o:#0#1 + chr($EE)+chr($90)+chr($A1)), // Shift-pageup (kitty)
(i:'[5;3~'; o:#0#2 + chr($EE)+chr($90)+chr($A1)), // Alt-pageup (xterm/lx/qterm)
(i:'[5;5~'; o:#0#4 + chr($EE)+chr($90)+chr($A1)), // Ctrl-pageup (xterm/lx/qterm)
(i:'[5^'; o:#0#4 + chr($EE)+chr($90)+chr($A1)), // Ctrl-pageup (rxvt)
(i:'[5~'; o:chr($EE)+chr($90)+chr($A1)), // page up (linux/xterm/Eterm/rxvt/lx/qterm)
(i:'[6;2~'; o:#0#1 + chr($EE)+chr($90)+chr($A2)), // Shift-pagedown (kitty)
(i:'[6;3~'; o:#0#2 + chr($EE)+chr($90)+chr($A2)), // Alt-pagedown (xterm/lx/qterm)
(i:'[6;5~'; o:#0#4 + chr($EE)+chr($90)+chr($A2)), // Ctrl-pagedown (xterm/lx/qterm)
(i:'[6^'; o:#0#4 + chr($EE)+chr($90)+chr($A2)), // Ctrl-pagedown (rxvt)
(i:'[6~'; o:chr($EE)+chr($90)+chr($A2)), // page down (linux/xterm/Eterm/rxvt/lx/qterm)
(i:'[7$'; o:#0#1 + chr($EE)+chr($90)+chr($A4)), // Shift-home (rxvt)
(i:'[7^'; o:#0#4 + chr($EE)+chr($90)+chr($A4)), // Ctrl-home (rxvt)
(i:'[7~'; o:chr($EE)+chr($90)+chr($A4)), // home (Eterm/rxvt)
(i:'[8$'; o:#0#1 + chr($EE)+chr($90)+chr($A3)), // Shift-end (rxvt)
(i:'[8^'; o:#0#4 + chr($EE)+chr($90)+chr($A3)), // Ctrl-end (rxvt)
(i:'[8~'; o:chr($EE)+chr($90)+chr($A3)), // end (rxvt)
(i:'[A'; o:chr($EE)+chr($90)+chr($A6)), // up (linux/xterm/FreeBSD/rxvt/lx/qterm)
(i:'[B'; o:chr($EE)+chr($90)+chr($A8)), // down (linux/xterm/FreeBSD/rxvt/lx/qterm)
(i:'[C'; o:chr($EE)+chr($90)+chr($A7)), // right (linux/xterm/FreeBSD/rxvt/lx/qterm)
(i:'[D'; o:chr($EE)+chr($90)+chr($A5)), // left (linux/xterm/FreeBSD/rxvt/lx/qterm)
(i:'[E'; o:chr($EE)+chr($90)+chr($8C)), // numpad 5 (xterm/lx)
(i:'[F'; o:chr($EE)+chr($90)+chr($A3)), // end (xterm/FreeBSD/lx/qterm)
(i:'[G'; o:chr($EE)+chr($90)+chr($8C)), // numpad 5 (linux)
(i:'[H'; o:chr($EE)+chr($90)+chr($A4)), // home (xterm/FreeBSD/lx/qterm)
(i:'[I'; o:chr($EE)+chr($90)+chr($A1)), // page up (FreeBSD)
(i:'[Oa'; o:#0#4 + chr($EE)+chr($90)+chr($A6)), // Ctrl-up (rxvt)
(i:'[Ob'; o:#0#4 + chr($EE)+chr($90)+chr($A8)), // Ctrl-down (rxvt)
(i:'[Oc'; o:#0#4 + chr($EE)+chr($90)+chr($A7)), // Ctrl-right (rxvt)
(i:'[Od'; o:#0#4 + chr($EE)+chr($90)+chr($A5)), // Ctrl-left (rxvt)
(i:'[Z'; o:#0#1 + chr(9)), // Shift-tab (xterm/lx/qterm)
(i:'[[A'; o:chr($EE)+chr($91)+chr($B0)), // F1 (linux/konsole/xterm/qterminal)
(i:'[[B'; o:chr($EE)+chr($91)+chr($B1)), // F2 (linux/konsole/xterm/qterminal)
(i:'[[C'; o:chr($EE)+chr($91)+chr($B2)), // F3 (linux/konsole/xterm/qterminal)
(i:'[[D'; o:chr($EE)+chr($91)+chr($B3)), // F4 (linux/konsole/xterm/qterminal)
(i:'[[E'; o:chr($EE)+chr($91)+chr($B4)), // F5 (linux/konsole)
(i:'[a'; o:#0#1 + chr($EE)+chr($90)+chr($A6)), // Shift-up (rxvt)
(i:'[b'; o:#0#1 + chr($EE)+chr($90)+chr($A8)), // Shift-down (rxvt)
(i:'[c'; o:#0#1 + chr($EE)+chr($90)+chr($A7)), // Shift-right (rxvt)
(i:'[d'; o:#0#1 + chr($EE)+chr($90)+chr($A5)) // Shift-left (rxvt)
);

const lastMatch : longint = high(keySeqList) shr 1;

function FindSeq(seqp : pointer; seqlen : byte) : longint;
// Attempts to find the longest keySeqList[] string that matches the pointed-to byte sequence.
// Returns the matched index if a match found, otherwise -1.
var min, max : longint;
	res : longint;
	minlen, complen : byte;
begin
	// Binary search, starting from last successful match.
	min := 0; max := high(keySeqList);
	result := lastMatch;
	repeat

		{for minlen := 0 to seqlen  - 1 do
			if byte((seqp + minlen)^) in [32..127] then
				write(char((seqp + minlen)^))
			else
				write('#',byte((seqp + minlen)^));
			write(' = ');
		for minlen := 1 to length(keySeqList[result].i) do
			if byte(keySeqList[result].i[minlen]) in [32..127] then
				write(keySeqList[result].i[minlen])
			else
				write('.');
			write(' ? ');}

		complen := length(keySeqList[result].i);
		minlen := seqlen;
		if complen < seqlen then minlen := complen;
		res := CompareByte(seqp^, keySeqList[result].i[1], minlen);
		//writeln(res,' @ ',result);
		if res = 0 then begin
			if complen = seqlen then begin
				lastMatch := result;
				exit;
			end;
			res := -1;
			if complen < seqlen then res := 1;
		end;
		if res > 0 then
			min := result + 1
		else
			max := result - 1;
		result := (min + max) shr 1;
	until min > max;

	//writeln('closest match: ESC',keySeqList[result].i,'_');
	complen := length(keySeqList[result].i);
	if (complen > seqlen) or (CompareByte(seqp^, keySeqList[result].i[1], complen) <> 0) then
		result := -1;
end;

function TranslateEsc(readp : pointer; readlen : byte) : longint;
// This tries to translate the byte stream in readp^ into a non-escaped custom version saved in inputBuf.
// Readp must point to the initial esc, readlen must be the number of input bytes available including the esc.
// Returns the byte length of the recognised section in the source string, including the esc.
// If failed to recognise anything, converts the whole input string into readable output to signal translation failure.
var i, myseq : longint;
begin
	result := 2;
	Assert(readlen >= 2);
	case char((readp + 1)^) of
		// Alt-tab in linux tty should be treated as shift-tab.
		#9: inputBuf[inputBufWriteIndex] := #0#1#9;
		// Backspace masquerading as delete.
		#$7F: inputBuf[inputBufWriteIndex] := #0#2#8;

		'[','O':
		begin
			// Structured xterm modified keypresses, for example Ctrl+Tab = ESC[27;5;9~
			if (readlen >= 8)
			and (char((readp + 1)^) = '[') and (char((readp + 2)^) = '2') and (char((readp + 3)^) = '7')
			and (char((readp + 4)^) = ';') and (char((readp + 5)^) in ['3'..'6']) and (char((readp + 6)^) = ';')
			then begin
				// Modifiers: 3=alt, 4=alt+shift, 5=ctrl, 6=ctrl+shift
				i := byte((readp + 5)^) - 49;
				myseq := 0;
				result := 7;
				while (char((readp + result)^) in ['0'..'9']) do begin
					myseq := myseq * 10 + byte((readp + result)^) - 48;
					inc(result);
				end;
				inc(result);
				// Save the translated keypress.
				inputBuf[inputBufWriteIndex] := #0 + char(i) + char(myseq);
			end

			else begin
				// Complex combinations.
				result := 0;
				myseq := FindSeq(readp + 1, readlen - 1);
				if myseq >= 0 then begin
					inputBuf[inputBufWriteIndex] := keySeqList[myseq].o;
					result := length(keySeqList[myseq].i) + 1;
				end
				else begin
					inputBuf[inputBufWriteIndex] := 'Unknown escape code: ';
					for i := 0 to readlen - 1 do
						if byte((readp + i)^) in [32..127] then
							inputBuf[inputBufWriteIndex] := inputBuf[inputBufWriteIndex] + char((readp + i)^)
						else if byte((readp + i)^) = 27 then
							inputBuf[inputBufWriteIndex] := inputBuf[inputBufWriteIndex] + 'esc'
						else
							inputBuf[inputBufWriteIndex] := inputBuf[inputBufWriteIndex] + '?';
					result := readlen;
				end;
			end;
		end;

		// Any other alphanumeric or basic symbol key.
		else inputBuf[inputBufWriteIndex] := #0#2 + char((readp + 1)^);
	end;
end;
{$endif}

{$ifdef WINDOWS}

procedure CrtWriteConOut(const srcp : pointer; const sx, sy, x1, y1, x2, y2 : dword); inline;
var bufsize, bufcoord : COORD;
	writereg : SMALL_RECT;
begin
	bufsize.x := sx; bufsize.y := sy;
	bufcoord.x := 0; bufcoord.y := 0;
	writereg.left := x1; writereg.top := y1; writereg.right := x2; writereg.bottom := y2;
	WriteConsoleOutputW(StdOutH, srcp, bufsize, bufcoord, writereg);
end;

procedure Delay(msec : dword); inline;
begin
	Sleep(msec);
end;

function GetMsecTime : ptruint; inline;
begin
	result := GetTickCount;
end;

procedure GotoXY(x, y : dword); inline;
// Attempts to place the cursor to the given character cell in the console.
// If the coordinates are outside the console buffer, the move is cancelled.
// The coordinates are 0-based.
var gotopos : COORD;
begin
	gotopos.X := x; gotopos.Y := y;
	SetConsoleCursorPosition(StdOutH, gotopos);
end;

procedure ClrScr;
// Fills the console buffer with whitespace, and places the cursor in the top left.
var numcharswritten : dword;
const writecoord : COORD = (x : 0; y : 0);
begin
	numcharswritten := 0; // silence a compiler warning
	FillConsoleOutputCharacter(StdOutH, ' ', 32767 * 32767, writecoord, numcharswritten);
	FillConsoleOutputAttribute(StdOutH, 7, 32767 * 32767, writecoord, numcharswritten);
	GotoXY(0, 0);
end;

procedure SetColor(color : word); inline;
// Sets the attribute to be used when printing any text in the console after this call.
// The low nibble is the text color, and the next nibble is the background color, using the standard 16-color palette.
// The high byte may contain some other odd flags; see "Console Screen Buffers" on MSDN.
begin
	SetConsoleTextAttribute(StdOutH, color);
end;

function KeyPressed : boolean;
// Returns TRUE if the user has pressed a key, that hasn't yet been fetched through the ReadKey function. Otherwise
// returns FALSE without waiting. Any new keypresses are placed in inputBuf[].
var numevents, mychar, mymod : dword;
	eventrecord : INPUT_RECORD;
begin
	numevents := 0;
	repeat
		// Check if new input events exist.
		if NOT GetNumberOfConsoleInputEvents(StdInH, numevents) then break;
		if numevents = 0 then break;
		// Fetch the next console input event.
		if NOT ReadConsoleInputW(StdInH, @eventrecord, 1, @numevents) then break;

		// If the event is a keypress, we may be interested...
		if eventrecord.EventType = KEY_EVENT then
			with KEY_EVENT_RECORD(eventrecord.Event) do begin
				// The key was pressed, rather than released?
				if bKeyDown then begin
					// Ignore unaccompanied special keys (shift, alt, numlock, etc).
					if wVirtualKeyCode in [16,17,18,20,91,92,144,145] then continue;
					mymod := 0;

					// Note the key value...
					if UnicodeChar = #0 then begin
						// "Enhanced" key.
						case wVirtualKeyCode of
							// Ctrl-number
							$30..$39:
							begin
								mychar := wVirtualKeyCode;
								mymod := 4;
							end;
							// Ctrl-numpadstar/numpadplus/numpadminus/numpadslash
							$6A, $6B, $6D, $6F:
							begin
								mychar := wVirtualKeyCode - $40;
								mymod := 4;
							end;
							// Ctrl-plus/comma/dash/period/slash
							$BB, $BC, $BD, $BE, $BF:
							begin
								mychar := wVirtualKeyCode - $90;
								mymod := 4;
							end;
							// Recognised enhanced keys.
							$C,$21..$28,$2D,$2E,$70..$7B: mychar := $E400 + wVirtualKeyCode;
							// Context menu.
							$5D: mychar := $E42F;
							// Ignore unrecognised enhanced keys.
							else continue;
							//else writeln('unk ', wVirtualKeyCode);
						end;
						if dwControlKeyState and SHIFT_PRESSED <> 0 then mymod := 1;
					end
					else begin
						// Normal keypress. (If the user input a value outside the basic multilingual plane, this only
						// returns half of the code point.)
						mychar := dword(UnicodeChar);
						// Note shift if the key was a control key or space etc.
						if (dword(UnicodeChar) <= 32) and (dwControlKeyState and SHIFT_PRESSED <> 0) then mymod := 1;
					end;

					// Note control and alt modifiers.
					if dwControlKeyState and (LEFT_ALT_PRESSED + RIGHT_ALT_PRESSED) <> 0 then mymod := mymod or 2;
					if dwControlKeyState and (LEFT_CTRL_PRESSED + RIGHT_CTRL_PRESSED) <> 0 then mymod := mymod or 4;

					// AltGr or Ctrl+Alt can be confusing, since it is present in various normal keypresses on certain
					// keyboard layouts. For simplicity, let's just ignore that modifier.
					if mymod = 6 then mymod := 0;

					// Convert the UCS-2 to UTF-8, and stash it with the control modifier.
					// (see unicode.org's Corrigendum #1, "UTF-8 Bit Distribution")
					// UCS 0..7F    : 0000-0000-0xxx-xxxx --> 0xxxxxxx
					// UCS 80..7FF  : 0000-0yyy-yyxx-xxxx --> 110yyyyy 10xxxxxx
					// UCS 800..FFFF: zzzz-yyyy-yyxx-xxxx --> 1110zzzz 10yyyyyy 10xxxxxx
					if mychar <= $7F then begin
						if mymod = 0 then
							inputBuf[inputBufWriteIndex] := char(mychar)
						else
							inputBuf[inputBufWriteIndex] := #0 + char(mymod) + char(mychar);
					end

					else if mychar <= $7FF then begin
						if mymod = 0 then
							inputBuf[inputBufWriteIndex] := char($C0 or (mychar shr 6)) + char($80 or (mychar and $3F))
						else
							inputBuf[inputBufWriteIndex] := #0 + char(mymod) + char($C0 or (mychar shr 6))
								+ char($80 or (mychar and $3F));
					end
					else begin
						if mymod = 0 then
							inputBuf[inputBufWriteIndex] := char($E0 or (mychar shr 12))
								+ char($80 or ((mychar shr 6) and $3F)) + char($80 or (mychar and $3F))
						else
							inputBuf[inputBufWriteIndex] := #0 + char(mymod) + char($E0 or (mychar shr 12))
								+ char($80 or ((mychar shr 6) and $3F)) + char($80 or (mychar and $3F));
					end;

					// Advance the input buffer write index.
					inputBufWriteIndex := (inputBufWriteIndex + 1) and high(inputBuf);
				end;
				continue;
			end;

		// Handle window size change event. (Only signals buffer size changes, not window size change...)
		if eventrecord.EventType = WINDOW_BUFFER_SIZE_EVENT then crt_consoleSizeChanged := TRUE;

		// Continue checking input events, until no more are available.
	until FALSE;

	// Return TRUE, if a keypress was buffered earlier but hasn't yet been read with ReadKey.
	result := inputBufReadIndex <> inputBufWriteIndex;
end;

function ReadKey : string31;
// Blocks execution until the user presses a key. Returns the pressed key value as UTF-8 in a shortstring.
// Extended keys are returned as values in the Unicode private use area; see the top of the file.
// If any modifiers are present, the returned string will begin with a null byte, followed by a modifier bitfield, and
// then the UTF-8 code value.
begin
	// Repeat this until a wild keypress appears.
	while NOT KeyPressed do begin
		// No keypress yet, enter an efficient wait state.
		if WaitForSingleObject(StdInH, INFINITE) <> WAIT_OBJECT_0 then begin
			// The Wait call may return WAIT_FAILED or WAIT_ABANDONED, in which case something has gone horribly wrong.
			result := '';
			exit;
		end;
	end;
	// Key pressed! Return its value and advance the input buffer read index.
	result := inputBuf[inputBufReadIndex];
	inputBuf[inputBufReadIndex] := '';
	inputBufReadIndex := (inputBufReadIndex + 1) and high(inputBuf);
end;

procedure UTF8Write(const outstr : UTF8string); inline;
// Workaround for FPC's Write failing to print UTF8strings in a Windows console.
begin
	if length(outstr) <> 0 then WriteConsoleA(stdouth, @outstr[1], length(outstr), NIL, NIL);
end;

procedure CrtSetTitle(const newtitle : UTF8string); inline;
// Attempts to set the console or term window's title to the given string.
var widetitle : unicodestring;
begin
	widetitle := unicodestring(newtitle) + WideChar(0);
	SetConsoleTitleW(@widetitle[1]);
end;

procedure CrtShowCursor(visible : boolean); inline;
// Shows or hides the blinking console cursor.
begin
	conCursorInfo.bVisible := visible;
	SetConsoleCursorInfo(StdOutH, conCursorInfo);
end;

procedure GetConsoleSize(out sizex, sizey : dword);
// Returns the currently visible console size (cols and rows) in sizex and sizey.
var bufinfo : CONSOLE_SCREEN_BUFFER_INFO;
begin
	if NOT GetConsoleScreenBufferInfo(StdOutH, @bufinfo) then begin
		sizex := 0;
		sizey := 0;
	end
	else begin
		sizex := bufinfo.srWindow.right - bufinfo.srWindow.left + 1;
		sizey := bufinfo.srWindow.bottom - bufinfo.srWindow.top + 1;
	end;
end;

procedure GetConsolePalette;
// Updates crtPalette with the console's actual palette, if possible. May fail silently, in which case the fallback
// palette remains in place. This uses a function only available on WinVista and later.
type CONSOLE_SCREEN_BUFFER_INFOEX = record
		cbSize : ULONG;
		dwSize, dwCursorPosition : COORD;
		wAttributes : word;
		srWindow : SMALL_RECT;
		dwMaximumWindowSize : COORD;
		wPopupAttributes : word;
		bFullscreenSupported: BOOL;
		ColorTable : array[0..15] of COLORREF;
	end;
	PCONSOLE_SCREEN_BUFFER_INFOEX = ^CONSOLE_SCREEN_BUFFER_INFOEX;

var GetConsoleScreenBufferInfoEx : function(
		hConsoleOutput : HANDLE; lpConsoleScreenBufferInfoEx : PCONSOLE_SCREEN_BUFFER_INFOEX) : BOOL; stdcall;
	conbufinfo : CONSOLE_SCREEN_BUFFER_INFOEX;
	libhandle : HANDLE;
	txt : widestring = '';
	procname : ShortString;
	i : UINT;

begin
	libhandle := 0;
	setlength(txt, MAX_PATH);
	// To load a system dll, we first need the system directory.
	i := GetSystemDirectoryW(@txt[1], MAX_PATH);
	if i <> 0 then begin
		setlength(txt, i);
		txt := txt + '\kernel32.dll'#0;
		// Try to load the library.
		libhandle := LoadLibraryW(@txt[1]);
		if libhandle <> 0 then begin
			// Success! Now fetch the function from the library.
			procname := 'GetConsoleScreenBufferInfoEx'#0;
			pointer(GetConsoleScreenBufferInfoEx) := GetProcAddress(libhandle, @procname[1]);
			if pointer(GetConsoleScreenBufferInfoEx) <> NIL then begin
				// So far so good. The info structure's size element must be filled in.
				conbufinfo.cbSize := sizeof(CONSOLE_SCREEN_BUFFER_INFOEX);
				// Try to fetch the extended console information using the new function.
				if GetConsoleScreenBufferInfoEx(StdOutH, @conbufinfo) then begin
					// Got it! Distribute the palette colors into crtPalette[].
					for i := 0 to $F do begin
						crtPalette[i].r := conbufinfo.ColorTable[i] and $FF;
						crtPalette[i].g := (conbufinfo.ColorTable[i] shr 8) and $FF;
						crtPalette[i].b := (conbufinfo.ColorTable[i] shr 16) and $FF;
					end;
				end;
			end;
		end;
	end;

	setlength(txt, 0);
	pointer(GetConsoleScreenBufferInfoEx) := NIL;
	if libhandle <> 0 then FreeLibrary(libhandle);
end;

{$else}

procedure Delay(msec : dword); inline;
begin
	// Although FpNanoSleep exists specifically for delays, it may unexpectedly return before the requested timeout if
	// some other interesting signal appeared. Also, it uses nanosecs rather than millisecs.
	// FpSelect will reliably wait the requested time. Normally it's used to wait for a file event, but by inputting
	// all null files, the function only returns upon timeout.
	FpSelect(0, NIL, NIL, NIL, msec);
end;

function GetMsecTime : ptruint; inline;
var tp : TTimeVal;
begin
	FpGetTimeOfDay(@tp, NIL);
	result := tp.tv_sec * 1000 + tp.tv_usec shr 10;
end;

procedure GotoXY(x, y : dword); inline;
// Attempts to place the cursor to the given character cell in the console.
// If the coordinates are outside the console buffer, the move is cancelled.
// The coordinates are 0-based.
var xx, yy : string[15];
begin
	str(x + 1, xx);
	str(y + 1, yy);
	write(#27'[' + yy + ';' + xx + 'H');
end;

procedure ClrScr;
// Fills the console buffer with whitespace, and places the cursor in the top left.
begin
	write(#27'[2J'#27'[H');
end;

procedure SetColor(color : word); inline;
// Sets the attribute to be used when printing any text in the console after this call.
// The low nibble is the text color, and the next nibble is the background color, using the standard 16-color palette.
// The high byte doesn't do anything.
begin
	write(#27'[' + termTextColor[color and $F] + ';' + termBkgColor[color shr 4] + 'm');
end;

function KeyPressed : boolean;
// Returns TRUE if the user has pressed a key that hasn't yet been fetched through the ReadKey function. Otherwise
// returns FALSE immediately. Any new keypresses are placed in inputBuf[].
var seq : array[0..63] of byte;
	bytesavailable : ptrint = 0;
	i, bytesread : ptrint;
begin
	seq[0] := 0;
	inputBuf[inputBufWriteIndex] := '';
	repeat
		// Check if a new input sequence exists. It returns -1 and EsysEAgain if there's no new input.
		i := FpRead(1, seq[bytesavailable], length(seq) - bytesavailable);
		if i < 0 then begin
			i := FpGetErrno;
			if i = ESysEAGAIN then
				i := 0
			else begin
				str(i, inputBuf[inputBufWriteIndex]);
				inputBuf[inputBufWriteIndex] := 'fpRead error ' + inputBuf[inputBufWriteIndex];
				inputBufWriteIndex := (inputBufWriteIndex + 1) and high(inputBuf);
				break;
			end;
		end;
		inc(bytesavailable, i);
		if bytesavailable = 0 then break;

		{SetColor($F7); write('in: ');
		for i := 0 to bytesavailable - 1 do if seq[i] >= 32 then write(char(seq[i])) else write('#',seq[i]);
		writeln;}

		// Translate and save the input sequence in our input buffy.
		bytesread := 1;
		case seq[0] of
			// special case: ctrl-space
			0: inputBuf[inputBufWriteIndex] := #0#4#32;
			// non-conflicting ctrl-letters: a..g, k, l, n..z
			1..7, $B, $C, $E..$1A:
			inputBuf[inputBufWriteIndex] := #0#4 + char(seq[0]);
			// non-conflicting ctrl-numbers: 4..6
			$1C..$1E:
			inputBuf[inputBufWriteIndex] := #0#4 + char(seq[0] + $18);
			// ctrl-minus (conflicts with ctrl-7)
			$1F: inputBuf[inputBufWriteIndex] := #0#4#31;
			// esc
			$1B:
			begin
				if bytesavailable = 1 then begin
					// Just an esc.
					inputBuf[inputBufWriteIndex] := #27
				end
				else begin
					// Terminal window size notification: ESC[8;<rows>;<cols>t
					if (bytesavailable >= 8)
					and (char(seq[2]) = '8') and (char(seq[3]) = ';') and (char(seq[1]) = '[')
					and (char(seq[bytesavailable - 1]) = 't')
					then begin
						termSizeY := 0; i := 4; bytesread := 6;
						while (i < bytesavailable) and (char(seq[i]) in ['0'..'9']) do begin
							termSizeY := termSizeY * 10 + seq[i] - 48;
							inc(i);
							inc(bytesread);
						end;
						termSizeX := 0; inc(i);
						while (i < bytesavailable) and (char(seq[i]) in ['0'..'9']) do begin
							termSizeX := termSizeX * 10 + seq[i] - 48;
							inc(i);
							inc(bytesread);
						end;
						dec(inputBufWriteIndex);
					end
					// Palette response: ESC]4;<index>;rgb:<bbbb>/<gggg>/<rrrr>
					else if (bytesavailable >= 22)
					and (char(seq[2]) = '4') and (char(seq[3]) = ';') and (char(seq[1]) = ']')
					then begin
						// Ignore the index, the requester knows which index it asked for.
						for i := 8 to 10 do if seq[i] = ord(':') then break;
						bytesread := i + 16;
						// Process the RGB numbers, doubled for some reason, so only two digits each are needed.
						seq[1] := seq[i + 3];
						seq[2] := seq[i + 4];
						seq[3] := seq[i + 8];
						seq[4] := seq[i + 9];
						seq[5] := seq[i + 13];
						seq[6] := seq[i + 14];
						crtPalResponse := 0;
						for i := 1 to 6 do begin
							crtPalResponse := crtPalResponse shl 4;
							case char(seq[i]) of
								'1'..'9': inc(crtPalResponse, seq[i] - 48);
								'a'..'f': inc(crtPalResponse, seq[i] - 87);
							end;
						end;
						dec(inputBufWriteIndex);
					end
					else begin
						// Scary escape sequence.
						bytesread := TranslateEsc(@seq[0], bytesavailable);
					end;
				end;
			end;
			// Backspace masquerading as delete.
			$7F: inputBuf[inputBufWriteIndex] := #8;

			$80..$BF: inputBuf[inputBufWriteIndex] := 'fpRead malformed UTF8';
			// Double-byte UTF-8.
			$C0..$DF:
			if bytesavailable < 2 then
				inputBuf[inputBufWriteIndex] := 'fpRead malformed UTF8'
			else begin
				bytesread := 2;
				setlength(inputBuf[inputBufWriteIndex], bytesread);
				move(seq[0], inputBuf[inputBufWriteIndex][1], bytesread);
			end;
			// Triple-byte UTF-8.
			$E0..$EF:
			if bytesavailable < 3 then
				inputBuf[inputBufWriteIndex] := 'fpRead malformed UTF8'
			else begin
				bytesread := 3;
				setlength(inputBuf[inputBufWriteIndex], bytesread);
				move(seq[0], inputBuf[inputBufWriteIndex][1], bytesread);
			end;
			// Quadruple-byte UTF-8.
			$F0..$FF:
			if bytesavailable < 4 then
				inputBuf[inputBufWriteIndex] := 'fpRead malformed UTF8'
			else begin
				bytesread := 4;
				setlength(inputBuf[inputBufWriteIndex], bytesread);
				move(seq[0], inputBuf[inputBufWriteIndex][1], bytesread);
			end;
			// Single-byte UTF-8.
			else inputBuf[inputBufWriteIndex] := char(seq[0]);
		end;

		// Remove the processed bytes.
		//writeln('Removing ',bytesread,'/',bytesavailable,' bytes');
		if bytesread = bytesavailable then
			bytesavailable := 0
		else begin
			for i := 0 to bytesavailable - bytesread - 1 do seq[i] := seq[i + bytesread];
			dec(bytesavailable, bytesread);
		end;

		// Advance the input buffer write index.
		inputBufWriteIndex := (inputBufWriteIndex + 1) and high(inputBuf);

		// Continue checking input events, until no more are available.
	until FALSE;

	// Return TRUE, if a keypress was buffered earlier but hasn't yet been read with ReadKey.
	result := inputBufReadIndex <> inputBufWriteIndex;
end;

function ReadKey : string31;
// Blocks execution until the user presses a key. Returns the pressed key value as UTF-8 in a shortstring.
// Extended keys are returned as values in the Unicode private use area; see the top of the file.
// If any modifiers are present, the returned string will begin with a null byte, followed by a modifier bitfield, and
// then the UTF-8 code value.
begin
	// Repeat this until a wild keypress appears.
	while NOT KeyPressed do begin
		// No keypress yet, enter an efficient wait state...
		// Use a file descriptor pointing at stdin (channel 0).
		fpFD_ZERO(stdInDescriptor);
		fpFD_SET(0, stdInDescriptor);
		FpSelect(1, @stdInDescriptor, NIL, NIL, -1);
	end;
	// Key pressed! Return its value and advance the input buffer read index.
	result := inputBuf[inputBufReadIndex];
	inputBuf[inputBufReadIndex] := '';
	inputBufReadIndex := (inputBufReadIndex + 1) and high(inputBuf);
end;

procedure UTF8Write(const outstr : UTF8string); inline;
begin
	write(outstr);
end;

procedure CrtSetTitle(const newtitle : UTF8string); inline;
// Attempts to set the console or term window's title to the given string.
begin
	write(#27']2;', newtitle, #7);
end;

procedure CrtShowCursor(visible : boolean); inline;
// Attempts to show or hide the cursor in the terminal window.
begin
	if visible then
		write(#27'[?25h') // show
	else
		write(#27'[?25l'); // hide
end;

procedure GetConsoleSize(out sizex, sizey : dword);
// Returns the currently visible terminal size (cols and rows) in sizex and sizey.
begin
	// Flush stdin.
	KeyPressed;
	// Request the window size.
	write(#27'[18t');
	// Enter a wait state until something appears in stdin, or 768ms elapsed.
	fpFD_ZERO(stdInDescriptor);
	fpFD_SET(0, stdInDescriptor);
	FpSelect(1, @stdInDescriptor, NIL, NIL, 768);
	// Check stdin; the window size should be processed into termSizeXy.
	KeyPressed;
	sizex := termSizeX; sizey := termSizeY;
end;

procedure GetConsolePalette;
// Updates crtPalette with the terminal's actual palette, if possible. May fail silently, in which case the fallback
// palette remains in place.

	function _AnsiGetColor(colnum : byte) : boolean;
	begin
		crtPalResponse := $FFFFFFFF;
		write(#27']4;', colnum, ';?'#7);
		// Enter a wait state until something appears in stdin, or 768ms elapsed.
		fpFD_ZERO(stdInDescriptor);
		fpFD_SET(0, stdInDescriptor);
		FpSelect(1, @stdInDescriptor, NIL, NIL, 768);
		// Check stdin; the color should be processed by KeyPressed.
		KeyPressed;
		result := crtPalResponse <> $FFFFFFFF;
		if result then with crtPalette[colnum] do begin
			r := crtPalResponse and $FF;
			g := (crtPalResponse shr 8) and $FF;
			b := (crtPalResponse shr 16) and $FF;
		end;
	end;

var i : dword;
begin
	// Flush stdin.
	KeyPressed;
	// Request palette 0. If that worked, request the other colors.
	if _AnsiGetColor(0) then
		for i := 1 to 15 do _AnsiGetColor(i);
end;

procedure sigwinchHandler(sig : longint); cdecl;
begin
	if sig = SIGWINCH then crt_consoleSizeChanged := TRUE;
end;

procedure DoNewSets;
// This is called during initialisation, to tweak the terminal settings.
// Current settings must have been fetched into termSettings before calling.
var newsets : termios;
	sigact : SigActionRec;
begin
	newsets := termSettings;
	// See man pages on cfmakeraw and termios for details.
	// This tweaks the settings to a standard raw IO mode.
	cfmakeraw(newsets);
	// Enable UTF-8 handling. Should already be on by default everywhere?
	//newsets.c_iflag := newsets.c_iflag or IUTF8;
	// Output processing must be kept on, or writeln stops working as expected, since linebreaks no longer get
	// automatically expanded from a newline to a newline-carriage return pair.
	newsets.c_oflag := OPOST or ONLCR or ONOCR;
	// Send in the new settings.
	tcsetattr(1, TCSANOW, newsets);
	// Set up a signal handler for window size changes.
	sigact.sa_Handler := NIL;
	fillbyte(sigact, sizeof(sigact), 0);
	sigact.sa_Handler := sigactionhandler(@sigwinchHandler);
	FPSigaction(SIGWINCH, @sigact, @oldSigAction);
end;

{$endif}

// ------------------------------------------------------------------

procedure MiniCRT_RunTest;
// Call this from your program to enter a debug testing mode. This calls all functions in the unit, then enters a loop
// waiting for keypresses and console resizes. Press ESC to finish the test.
const hextable : array[0..$F] of char = (
	'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
var sux, suy, color, i : dword;
	kii : UTF8string;
begin
	sux := 0; suy := 0;
	CrtSetTitle('yeehehee');
	ClrScr;
	GetConsoleSize(sux, suy);
	writeln('detected console size: ', sux, 'x', suy);
	if sux = 0 then sux := 80;
	if suy = 0 then suy := 25;

	// Japanese UTF-8 text output.
	write(#$E3#$81#$BE + #$E3#$81#$A0);
	kii := #$E8#$A9#$B1 + #$E3#$81#$99;
	UTF8write(kii);
	writeln;

	// Fill the bottom row with purple, scribble in the lower right corner.
	// The window should not scroll.
	GotoXY(0, suy - 1);
	SetColor($50);
	write(space(sux - 4));
	write('...]');
	Delay(1300);

	// Draw a palette at the top.
	GetConsolePalette;
	GotoXY(0, 3);
	for sux := 0 to $F do begin
		if sux = 0 then
			SetColor(7)
		else
			SetColor(sux);
		suy := sux * 4;
		if suy > 0 then dec(suy);
		if sux and 1 = 0 then
			GotoXY(suy, 3)
		else
			GotoXY(suy, 6);
		write(hextable[crtPalette[sux].r shr 4]);
		write(hextable[crtPalette[sux].r and $F]);
		write(hextable[crtPalette[sux].g shr 4]);
		write(hextable[crtPalette[sux].g and $F]);
		write(hextable[crtPalette[sux].b shr 4]);
		write(hextable[crtPalette[sux].b and $F]);
	end;
	GotoXY(0, 4);
	for sux := 0 to $F do begin
		SetColor($11 * sux);
		write('@@@@');
	end;
	GotoXY(0, 5);
	for sux := 0 to $F do begin
		SetColor($11 * sux);
		write('@@@@');
	end;

	// Start the key-catcher.
	GotoXY(0,7);
	color := $003B;
	repeat
		kii := ReadKey;
		SetColor(color);
		color := color xor 4;
		write('key: ');
		for i := 1 to length(kii) do
			write(hextable[(byte(kii[i]) shr 4)], hextable[(byte(kii[i]) and $F)]);
		write(': ');
		for i := 1 to length(kii) do
			if byte(kii[i]) in [32..127] then write(kii[i]) else write('.');
		writeln;
		if crt_consoleSizeChanged then begin
			crt_consoleSizeChanged := FALSE;
			GetConsoleSize(sux, suy);
			writeln('detected console size: ', sux, 'x', suy);
		end;
		if NOT KeyPressed then Delay(500); // for testing how key repetition joins into a single string
	until (kii = #27) or (kii = ' ');
end;

// ------------------------------------------------------------------
initialization

	{$ifdef WINDOWS}

	// Get the OS handles for the console's input and output.
	StdInH := GetStdHandle(STD_INPUT_HANDLE);
	StdOutH := GetStdHandle(STD_OUTPUT_HANDLE);
	// Disable most special handling, but enable window resize notifications.
	SetConsoleMode(StdInH, ENABLE_WINDOW_INPUT);
	SetConsoleMode(StdOutH, ENABLE_PROCESSED_OUTPUT{ or ENABLE_WRAP_AT_EOL_OUTPUT});
	// Fetch the cursor size and visibility.
	conCursorInfo.dwSize := 0;
	GetConsoleCursorInfo(StdOutH, conCursorInfo);
	// The console must be switched to UTF-8 mode, or multibyte characters are displayed as single-byte mojibake.
	// The console or OS must also have a suitable font with the needed glyphs, such as Lucida Sans Unicode.
	// At least on WinXP the console is buggy, and will display kanji as generic blocks, until you move the window
	// around to force a redraw, which makes the correct characters turn up at a wider spacing.
	oldCodePageOut := GetConsoleOutputCP;
	SetConsoleOutputCP(CP_UTF8);

	{$else}

	// Get the current terminal settings.
	termSettings.c_iflag := 0;
	TCGetAttr(1, termSettings);
	// Punch in new settings.
	DoNewSets;
	// Stop FpRead from blocking execution, for benefit of KeyPressed.
	fileControlFlags := FpFcntl(0, F_GetFl);
	FpFcntl(0, F_SetFl, fileControlFlags or O_NONBLOCK);
	// Reset text color attributes?
	write(#27'[0m');
	// Select the UTF-8 character set. Not always necessary, but at least on my LXterminal this is required or UTF-8
	// output isn't recognised.
	write(#27'%G');

	{$endif}

	// Initialise the internal keypress ring buffer.
	inputBufWriteIndex := 0;
	inputBufReadIndex := 0;

finalization

	{$ifdef WINDOWS}

	// Restore the original console code page.
	SetConsoleOutputCP(oldCodePageOut);
	// Restore the standard console mode and color.
	SetConsoleMode(StdInH, ENABLE_LINE_INPUT + ENABLE_ECHO_INPUT + ENABLE_PROCESSED_INPUT + ENABLE_MOUSE_INPUT);
	SetConsoleMode(StdOutH, ENABLE_PROCESSED_OUTPUT + ENABLE_WRAP_AT_EOL_OUTPUT);
	SetColor($0007);

	{$else}

	// Restore the standard text color.
	write(#27'[0m');
	// Restore the original terminal settings.
	TCSetAttr(1, TCSANOW, termSettings);
	// Restore the old signal handling.
	FPSigaction(SIGWINCH, @oldSigAction, NIL);
	// Restore normal fpRead operation.
	FpFcntl(0, F_SetFl, fileControlFlags);

	{$endif}

	// Restore the cursor.
	CrtShowCursor(true);
end.

