{                                                                           }
{ Copyright 2009 :: Kirinn Bunnylin / Mooncore                              }
{                                                                           }
{ This file is part of SuperSakura.                                         }
{                                                                           }
{ SuperSakura is free software: you can redistribute it and/or modify       }
{ it under the terms of the GNU General Public License as published by      }
{ the Free Software Foundation, either version 3 of the License, or         }
{ (at your option) any later version.                                       }
{                                                                           }
{ SuperSakura is distributed in the hope that it will be useful,            }
{ but WITHOUT ANY WARRANTY; without even the implied warranty of            }
{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             }
{ GNU General Public License for more details.                              }
{                                                                           }
{ You should have received a copy of the GNU General Public License         }
{ along with SuperSakura.  If not, see <https://www.gnu.org/licenses/>.     }
{                                                                           }

function XorSpan(startp : pointer; len : dword) : dword;
// Returns a CRC from a memory span, simply XORing every byte. Returns a byte, the dword is used for calculations.
begin
	result := $45;
	if len > 20 then begin
		while len >= 4 do begin result := result xor dword(startp^); inc(startp, 4); dec(len, 4); end;
		result := (result and $FFFF) xor (result shr 16);
		result := (result and $FF) xor (result shr 8);
	end;
	while len <> 0 do begin result := result xor byte(startp^); inc(startp); dec(len); end;
end;

function FindLanguageIndex(langdesc : UTF8string) : dword;
// Returns an existing language index for the given language. Case-insensitive match. If not found, returns -1.
begin
	assert(languageList.Length <> 0);
	langdesc := lowercase(langdesc);
	for result := high(languageList) downto 0 do
		if lowercase(languageList[result]) = langdesc then exit;
	result := high(dword);
end;

function FindOrAddLanguage(const langdesc : UTF8string) : dword;
// Returns an existing language index for the input descriptor. If not found, it is added as a new language.
begin
	assert(languageList.Length <> 0);
	// If the first language is unspecified (default empty value on engine startup), overwrite it.
	if languageList[0] = '' then
		result := 0
	else begin
		// If the language is already listed, return its index.
		result := FindLanguageIndex(langdesc);
		if result < languageList.Length then exit;

		// No matching language, create a new slot for it.
		result := languageList.Length;
		setlength(languageList, result + 1);
	end;
	languageList[result] := langdesc;
end;

function AddGlobalString(const s : UTF8string) : dword;
// Inserts the given string into the sorted global string table. Returns the index.
begin
	Assert(length(scriptObjects) <> 0);
	Assert(s <> '');

	with scriptObjects[0] do begin
		result := globalStringCount;
		inc(globalStringCount);
		if globalStringCount >= dword(length(stringTable)) then
			setlength(stringTable, globalStringCount + globalStringCount shr 3 + 8);
		inc(totalStringCount);

		while (result <> 0) and (stringTable[result - 1].txt[0] > s) do begin
			stringTable[result] := stringTable[result - 1];
			dec(result);
		end;

		with stringTable[result] do begin
			txt := NIL;
			setlength(txt, languageList.Length + 1);
			txt[0] := s;
		end;
	end;
end;

function FindGlobalString(const canonical : UTF8string) : dword;
// Returns the index of the global string equal to the given string. If not found, returns -1.
// The search is case-sensitive. The global string table must be sorted before calling this.
var comp, min, max : longint;
begin
	if (canonical = '') or (globalStringCount = 0) then begin
		result := high(dword); exit;
	end;
	// binary search
	min := 0; max := globalStringCount - 1;
	with scriptObjects[0] do repeat
		result := (min + max) shr 1;
		comp := CompStr(
			@canonical[1], @stringTable[result].txt[0][1],
			canonical.Length, stringTable[result].txt[0].Length);
		if comp = 0 then exit;
		if comp > 0 then
			min := result + 1
		else
			max := result - 1;
	until min > max;
	result := high(dword);
end;

procedure ImportStringTable(const importbuffy : pointer; bytelen : dword; importcanonicals : boolean);
// Attempts to read the given file, which should contain a plain tab-separated table in UTF8, and imports it as
// a string table. The first row should have an empty cell, followed by a language name for each column. The following
// rows must contain a unique string ID each, and string text in the other columns.
// The given buffer must end with a newline. Throws an exception in case of errors.
var readp, endp : ^byte;
	columnlanguage : array of byte = NIL;
	labelcrcfail : array of boolean = NIL;
	cells : TStringBunch = NIL;
	rowcount, colcount, i : dword;
	b : boolean;

	function _ReadRow : boolean;
	// Copies next TSV row into cells[]. Returns TRUE if unique string, FALSE if global.
	var cellstart : ^byte;
		ci, cellsize : dword;
	begin
		result := FALSE;
		cellstart := readp;
		ci := 0;
		while readp < endp do begin
			// Control character!
			if readp^ < $20 then begin
				// Tab or linebreak.
				if readp^ in [9, $A..$D] then begin
					if ci = 0 then begin
						if (cellstart^ = byte('@')) then begin
							result := TRUE;
							inc(cellstart);
						end
						else if ((cellstart^ = byte('\')) and ((cellstart + 1)^ = byte('@'))) then
							inc(cellstart);
					end;
					cellsize := readp - cellstart;
					if ci >= cells.Length then setlength(cells, ci + 1);
					if cells[ci].Length < cellsize then cells[ci] := '';
					setlength(cells[ci], cellsize);
					if cellsize <> 0 then move(cellstart^, cells[ci][1], cellsize);
					inc(ci);

					if readp^ in [$A..$D] then begin
						// CR+LF or other such pair: count as single row end.
						if (readp + 1 < endp) and ((readp + 1)^ in [$A..$D]) and (readp^ <> (readp + 1)^) then
							inc(readp);
						inc(readp);
						inc(rowcount);
						// Row had some kind of content: exit.
						if (ci > 1) or (cells[0] <> '') then break;
						// Row was empty: try the next row.
						ci := 0;
					end;

					cellstart := readp + 1;
				end
				else raise Exception.Create(strcat('invalid char $& in string table row %', [readp^, rowcount + 1]));
			end;
			// Any non-control character is valid cell content.
			inc(readp);
		end;
		// Shrink cells list to exact size.
		if ci <> cells.Length then setlength(cells, ci);
		//if cells.Length <> 0 then asman_logger(strcat('row %: got "%" and "%"', [rowcount, cells[0], cells[1]]));
	end;

	procedure _ApplyGlobal;
	var index, l : dword;
	begin
		index := FindGlobalString(cells[0]);
		if index >= globalStringCount then index := AddGlobalString(cells[0]);

		// Overwrite all language strings for this global with non-empty cells from this new row.
		if cells.Length > 1 then with scriptObjects[0].stringTable[index] do begin
			if txt.Length < languageList.Length + 1 then setlength(txt, languageList.Length + 1);
			for l := 1 to high(cells) do if cells[l] <> '' then
				txt[columnlanguage[l] + 1] := cells[l];
		end;
	end;

	procedure _ApplyUnique;
	var labelindex, j, l, index : dword;
		_crc : string[2];
	begin
		// The label that's part of the string ID must be in uppercase.
		cells[0] := upcase(cells[0]);
		j := cells[0].Length;

		// Extract the CRC from end of string.
		if (j < 6) or (NOT (cells[0][j] in ['0'..'9','A'..'F']))
		or (NOT (cells[0][j - 1] in ['0'..'9','A'..'F']))
		or (cells[0][j - 2] <> '.')
		then raise Exception.Create(strcat('bad crc in "%" on string table row %', [cells[0], rowcount]));
		_crc[1] := cells[0][j - 1];
		_crc[2] := cells[0][j];
		dec(j, 3);

		// Extract the index.
		while cells[0][j] <> '.' do begin
			if (j <= 2) or (NOT (cells[0][j] in ['0'..'9'])) then
				raise Exception.Create(strcat('bad index in "%" on string table row %', [cells[0], rowcount]));
			dec(j);
		end;
		index := dword(valx(copy(cells[0], j + 1)));

		// The rest of the cell content is the script.label identifier.
		setlength(cells[0], j - 1);
		// Find the script.label being added to.
		labelindex := GetScript(cells[0]);
		if labelindex = 0 then
			raise Exception.Create(strcat('label "%" not found (string table row %)', [cells[0], rowcount]));

		with scriptObjects[labelindex] do begin
			if index >= dword(length(stringTable)) then setlength(stringTable, index + 1);
			if stringTable[index].txt.Length < languageList.Length then
				setlength(stringTable[index].txt, languageList.Length);

			with stringTable[index] do begin
				// If there's a canonical string on this row, add the whole row, overwrite all existing.
				for j := high(columnlanguage) downto 1 do if columnlanguage[j] = labelLanguage then begin
					if NOT importcanonicals then cells[j] := '';
					if cells[j] = '' then break; // no canonical!
					if txt[labelLanguage] = '' then inc(totalStringCount);
					txt := NIL;
					setlength(txt, languageList.Length);
					for l := 1 to high(cells) do if cells[l] <> '' then
						txt[columnlanguage[l]] := cells[l];
					exit;
				end;
				if labelcrcfail[labelindex] then exit;

				// No canonical on row. In this case the canonical string must already have been defined.
				if txt[labelLanguage] = '' then raise Exception.Create(strcat(
					'[!] can''t add translation for "%" without original in string table row %', [cells[0], rowcount]));

				// Canonical has been defined, only non-canonicals being added on this row. Verify CRC.
				byte(_crc[0]) := 2;
				j := labelLanguage;
				labelcrcfail[labelindex] := (XorSpan(@txt[j][1], txt[j].Length) <> valhex(_crc));
				if labelcrcfail[labelindex] then exit;

				for l := 1 to high(cells) do
					if cells[l] <> '' then txt[columnlanguage[l]] := cells[l];
			end;
		end;
	end;

begin
	readp := importbuffy;
	endp := importbuffy + bytelen;
	Assert((endp - 1)^ in [$A,$D], 'buf must end in newline');

	// Get the language descriptors from the header.
	rowcount := 0;
	_ReadRow;
	colcount := cells.Length;
	if colcount = 0 then begin
		asman_logger('empty string table');
		exit;
	end;
	if colcount = 1 then raise Exception.Create('string table must have 2+ columns');

	setlength(columnlanguage, colcount);
	for i := 1 to colcount - 1 do begin
		if cells[i] = '' then raise Exception.Create('empty header in string table column ' + strdec(i + 1));
		columnlanguage[i] := FindOrAddLanguage(cells[i]);
	end;

	setlength(labelcrcfail, scriptObjectCount); // inits to all false

	// Read and apply each row.
	while readp < endp do begin
		b := _ReadRow;
		if cells.Length <> 0 then begin
			if cells[0] = '' then raise Exception.Create('empty id on row ' + strdec(rowcount));
			if b then
				_ApplyUnique
			else
				_ApplyGlobal;
		end;
	end;

	// Resize the global strings array to a precise size.
	setlength(scriptObjects[0].stringTable, globalStringCount);

	// If CRC failed for any string in any label, wipe all non-canonical strings from that label.
	for i := scriptObjectCount - 1 downto 0 do if labelcrcfail[i] then with scriptObjects[i] do begin
		asman_logger('[!] CRC fail in ' + labelName + ', clear all translated strings');
		assert(length(stringTable) <> 0);
		for rowcount := high(stringTable) downto 0 do with stringTable[rowcount] do begin
			assert(txt.Length <> 0);
			for colcount := high(txt) downto 0 do if colcount <> labelLanguage then txt[colcount] := '';
		end;
	end;
	asman_logger('imported table, total strings ' + strdec(totalStringCount));
end;

function ExportStringTable(var exportbuffy : pointer) : dword;
// Prints the current string tables in plain UTF8 text from memory into the given buffer, in TSV format. The strings
// are presented as a table, separated with tabs and linebreaks. The script labels are hopefully sorted bytewise,
// as they should be at all times; if not, it'll slow down importing later.
// Exportbuffy should be NIL when calling.
// Returns the number of bytes saved in exportbuffy^. The caller is responsible for freeing the buffer.
// Throws an exception in case of errors, in which case exportbuffy will not be valid.
var writep : ^char;
	i, j, l, totalsize : dword;

	procedure _WriteBytes(const s : UTF8string);
	begin
		if s.Length <> 0 then begin
			move(s[1], writep^, s.Length);
			inc(writep, s.Length);
		end;
	end;

begin
	if scriptObjectCount < 2 then raise Exception.Create('Can''t export string table, no scripts exist');
	Assert(languageList.Length <> 0);

	totalsize := 1;
	for i := high(languageList) downto 0 do inc(totalsize, languageList[i].Length + 1);

	for i := scriptObjectCount - 1 downto 0 do with scriptObjects[i] do begin
		if length(stringTable) = 0 then continue; // script has no strings
		// Count length of all string IDs in this script.
		// "<script.label>.<index>.<crc>" where index may be up to 10 chars and crc is always 2 chars.
		inc(totalsize, (dword(length(labelName)) + 1 + 10 + 1 + 2) * dword(length(stringTable)));
		// Count terminating tab or newline after each string in this script. (One char in every data cell)
		inc(totalsize, (languageList.Length + 1) * dword(length(stringTable)));
		// Count the length of all strings to be saved for this script.
		for j := high(stringTable) downto 0 do with stringTable[j] do begin
			for l := high(txt) downto 0 do
				inc(totalsize, txt[l].Length);
		end;
	end;

	getmem(exportbuffy, totalsize);
	writep := exportbuffy;

	try
		// Print the header, with language descriptors.
		for i := 0 to high(languageList) do begin
			writep^ := #9; inc(writep);
			_WriteBytes(languageList[i]);
		end;
		writep^ := #$A; inc(writep);

		// Print the global string table.
		with scriptObjects[0] do if length(stringTable) <> 0 then
			for j := 0 to high(stringTable) do with stringTable[j] do begin
				// Edge case: global string beginning with @ must be prefixed with a backslash.
				// Extra edge case: global string beginning with \@ gets an extra backslash. Don't use that...
				if (txt[0][1] = '@') or (txt[0].StartsWith('\@')) then begin
					writep^ := '\'; inc(writep);
				end;
				_WriteBytes(txt[0]);
				if txt.Length > 1 then for l := 1 to txt.Length - 1 do begin
					writep^ := #9; inc(writep);
					if txt[l] <> txt[0] then _WriteBytes(txt[l]);
				end;
				writep^ := #$A; inc(writep);
			end;

		// Print unique string tables.
		for i := 1 to scriptObjectCount - 1 do with scriptObjects[i] do begin
			if length(stringTable) = 0 then continue; // script has no strings

			for j := high(stringTable) downto 0 do with stringTable[j] do begin
				// Print the string ID.
				writep^ := '@'; inc(writep);
				_WriteBytes(labelName);
				writep^ := '.'; inc(writep);
				_WriteBytes(strdec(j));
				writep^ := '.'; inc(writep);
				_WriteBytes(strhex(XorSpan(@txt[labelLanguage][1], txt[labelLanguage].Length)));

				// Print the string in each language.
				for l := 0 to high(txt) do begin
					writep^ := #9; inc(writep);
					_WriteBytes(txt[l]);
				end;
				writep^ := #$A; inc(writep);
			end;
		end;

		result := writep - exportbuffy;
		//SaveFile('table.tsv', exportbuffy, result); // immediate dump for debugging
	except
		freemem(exportbuffy); exportbuffy := NIL;
		raise;
	end;
end;

